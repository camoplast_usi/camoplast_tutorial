/*********************************************************************************
Class Name      : CS_orderProductRedirectExtension
Description     : This is class is the extension controller for page CS_orderProductRedirect 
Created By      : Reju Palathingal
Created Date    : 2014-08-6
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            

*********************************************************************************/

public with sharing class CS_orderProductRedirectExtension {

    Id ordId;

    // we are extending the OpportunityLineItem controller, so we query to get the parent OpportunityId
    public CS_orderProductRedirectExtension (ApexPages.StandardController controller) {
        ordId = [select Id, OrderId from OrderItem where Id = :controller.getRecord().Id limit 1].OrderId ;
    }
    
    // then we redirect to our desired page with the Opportunity Id in the URL
    public pageReference redirect(){
        return new PageReference('/apex/CS_OrderProductEntry2?id=' + ordId);
    }

}