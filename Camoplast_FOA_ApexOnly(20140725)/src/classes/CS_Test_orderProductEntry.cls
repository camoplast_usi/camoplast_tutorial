/*********************************************************************************
Class Name      : CS_Test_orderProductEntry
Description     : This class is used for Test Classes for Functionalities on CS_OrderProductEntry
Created By      : Divya A N
Created Date    : 30-Jul-14
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Divya A N                 30-Jul-14               Initial Version
*********************************************************************************/
@istest (seeAllData= true)
private class CS_Test_orderProductEntry {

    static testMethod void theTests(){
           list<Pricebook2> oPb2 = [Select Id from PriceBook2 where isStandard = true];
           //system.assert(false, oPb2.size() + 'Records found'); 
           Pricebook2 oPb=new Pricebook2();
                 oPb.Name= 'Test';
                 oPb.Construction_Discount_Class__c= 'C';
                 oPb.MH_Discount_Class__c= 'B';
                 oPb.IsActive= true;
             insert oPb;
           
           datetime myDateTime = datetime.now();
             test.starttest();     
             Account oAcc= new Account();
                 oAcc.Name = 'Test Account';
                 oAcc.CurrencyIsoCode = 'EUR';            
                 oAcc.Con_Discount_Class__c = 'C';
                 oAcc.MH_Discount_Class__c =  'B';                 
             insert oAcc;
             Product2 oPr= new Product2();
                    oPr.Name = 'Test Product';
                    oPr.CurrencyIsoCode = 'USD';
                    //oPr.DefaultPrice = 1200;
                    oPr.Family = 'Service';
                    oPr.IsActive = true;
                    oPr.ProductCode = 'Test123';
                    
            insert oPr;
            
            PricebookEntry oPbe0 = new PricebookEntry();
                oPbe0.Pricebook2id = oPb2[0].Id;
                oPbe0.Product2id = oPr.Id;
                //oPbe0.UseStandardPrice = true;
                //StandardPrice = 1200;
                oPbe0.UnitPrice = 1100;
            insert oPbe0;

            
            PricebookEntry oPbe = new PricebookEntry();
                oPbe.Pricebook2id = oPb.Id;
                oPbe.Product2id = oPr.Id;
                oPbe.UseStandardPrice = false;
                //StandardPrice = 1200;
                oPbe.UnitPrice = 1000;
                oPbe.IsActive = true;
            insert oPbe;  
                   
             
             RecordType rec = [SELECT Id FROM RecordType where Name = 'North America' limit 1];
             
             Account accnt = [SELECT Assigned_Price_Book__c FROM Account WHERE ID = :oAcc.ID];    
             accnt.Assigned_Price_Book__c= oPb.ID;  
             Order oOrd = new Order();
                 oOrd.PoNumber= '123';
                 oOrd.EffectiveDate = myDateTime.date() ;            
                 oOrd.RecordType= rec;
                 oOrd.AccountId =  oAcc.ID;   
                 oOrd.Status = 'Draft';
                 oOrd.CurrencyIsoCode = 'USD';       
             insert oOrd;  


             OrderItem oI = new OrderItem();
                oI.OrderID = oOrd.ID;
                oI.PricebookEntryId = oPbe.Id;
                oI.Quantity = 5;
                oI.UnitPrice = 1050;
             insert oI;
             
        // Get OrderItem's details from DB:
        oI = [SELECT Id, PriceBookEntry.Product2.Name, OrderId FROM OrderItem WHERE Id=: oI.Id LIMIT 1];   
        
        ////////////////////////////////////////
        //  test OrderProductEntry
        ////////////////////////////////////////
        
        // load the page       
        PageReference pageRef = Page.CS_OrderProductEntry;
        pageRef.getParameters().put('Id',oI.OrderId);
        Test.setCurrentPageReference(pageRef);
        
        // load the extension
        CS_OrderProductsEntryExtn oPEE = new CS_OrderProductsEntryExtn(new ApexPages.StandardController(oOrd));
        
        // test 'getChosenCurrency' method
        if(UserInfo.isMultiCurrencyOrganization())
            System.assert(oPEE.getChosenCurrency()!='');
        else
            System.assertEquals(oPEE.getChosenCurrency(),'');

        // we know that there is at least one line item, so we confirm
        Integer startCount = oPEE.ShoppingCart.size();
        system.assert(startCount>0);

        //test search functionality without finding anything
        oPEE.searchString = 'ABCD';
        oPEE.updateAvailableList();
        system.assert(oPEE.AvailableProducts.size()==0);
        
        //test remove from shopping cart
        oPEE.toUnselect = oI.PricebookEntryId;
        oPEE.removeFromShoppingCart();
        system.assert(oPEE.shoppingCart.size()==startCount-1);
        
        //test save and reload extension
        oPEE.onSave();
        
        pageRef = Page.CS_OrderProductEntry;
        pageRef.getParameters().put('Id',oI.OrderId);
        Test.setCurrentPageReference(pageRef);
        
        CS_OrderProductsEntryExtn oPEE2 = new CS_OrderProductsEntryExtn(new ApexPages.StandardController(oOrd));
        system.assert(oPEE2.shoppingCart.size()==startCount-1);
        

        
        // test search again, this time we will find something
        oPEE2.searchString = 'Test';//oI.PriceBookEntry.Product2.Name;
        oPEE2.updateAvailableList();
        system.assert(oPEE2.AvailableProducts.size()>0 , 'Query Used: '+ oPEE2.qString);       

        // test add to Shopping Cart function
        oPEE2.toSelect = oPEE2.AvailableProducts[0].Id;
        oPEE2.addToShoppingCart();
        system.assert(oPEE2.shoppingCart.size()==startCount);
                
        // test save method - WITHOUT quanitities and amounts entered and confirm that error message is displayed
        oPEE2.onSave();
        system.assert(ApexPages.getMessages().size()>0);
        
        // add required info and try save again
        for(OrderItem o : oPEE2.ShoppingCart){
            o.quantity = 5;
            o.unitprice = 300;
        }
        oPEE2.onSave();
        
        //To Test the Apply Discount button
        for(OrderItem o : oPEE2.ShoppingCart){
            o.quantity = 5;
            o.unitprice = 300;
            o.Discount__c = 10;
        }
        oPEE2.applyDiscount();
        //list<OrderItem> ordItem = [Select TotalPrice from OrderItem where ID=: o.ID];
        
        
        // query line items to confirm that the save worked
        OrderItem[] oI2 = [select Id from OrderItem where OrderId = :oI.OrderId];
        system.assert(oI2.size()==startCount);
        
        // test on new Order (no pricebook selected) to make sure redirect is happening
        RecordType rec1 = [SELECT Id FROM RecordType where Name = 'North America'];
        Order newOrd = new Order(RecordType=rec1,CurrencyIsoCode='USD',PoNumber='12345',Status='Draft',EffectiveDate=myDateTime.date(),AccountId = oAcc.Id);
        insert newOrd;
        CS_OrderProductsEntryExtn oPEE3 = new CS_OrderProductsEntryExtn(new ApexPages.StandardController(newOrd));
       
        
        // final quick check of cancel button
        System.assert(oPEE3.onCancel()!=null);
        
        

     
    }
}