/*************************************************************************************
Classe Name : CS_Extension_GenerateQuote
Description : Class to generate quote and PDF on mobile.
Created By  : Jayadev Rath
Created Date: 14-July-14
Modification Log:
---------------------------------------------------------------------------------- 
Developer             Date             Description
---------------------------------------------------------------------------------- 
Jayadev Rath         14-Jul-2014       Initial Version
Jayadev Rath         28-Aug-2014       Updated createQuote() method to add record type logic when a new Quote is created
************************************************************************************/
Public class CS_Extension_GenerateQuote {
    Public Opportunity oOpp{get;set;}
    Public String sErrorFound {get;set;}
    Public String sOpportunityId {get;set;}
    Public String sQuoteId {get;set;}
    Public ApexPages.StandardController con{Get;set;}
    // Stores Opportunity to Quote record type names mapping
    Private Map<String,String> mapOppToQuoteRecordType = new Map<String,String>{
                'EMEA_Sales' => 'EMEA_Sales',
                'EMEA_Service' => 'EMEA_Service',
                'North_America' => 'North_America'
    };

    public CS_Extension_GenerateQuote(){}



    public CS_Extension_GenerateQuote(ApexPages.StandardController controller) {
        con = controller;
        sErrorFound = 'No';
        oOpp = [SELECT Id,Name,AccountId, RecordType.DeveloperName, SyncedQuoteId, Pricebook2Id,Approval_Status__c,Quote_Status__c,Expiration_Date__c,Reason_for_Rejection__c,Description,
                Quote_Prepared_For__c,Quote_Prepared_For__r.Phone, Quote_Prepared_For__r.Fax,Quote_Prepared_For__r.Email,Ship_To_Contact__c,
                (SELECT Id,Description,Discount,PricebookEntryId,Quantity,UnitPrice FROM OpportunityLineItems)
                FROM Opportunity WHERE Id = :controller.getId() ];
                
        if(oOpp.Approval_Status__c == 'Required') {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Opportunity should be approved to generate a Quote'));
            sErrorFound = 'Yes';    // Used for making navigation decision in Salesforce 1
            return;
        }
    }
        
    Public PageReference CreateQuoteFromProductEtnryPage(String sOppId){
        // Set the OpportunityId which is passed from OpportunityLineItem page
        sOpportunityId = sOppId;
        // Invoke the createQuote method to create the Quote and get the quote Id
        createQuote();
        
        return new PageReference('/'+sQuoteId);
    }
    
    // Create a Quote record and redirect to Opportunity Page
    Public PageReference createQuote(){
        // If the sOpportunityId has any values, when called from CreateQuoteFromProductEtnryPage() method, use that value.Else use Controller values.
        if(sOpportunityId == null) sOpportunityId = con.getId();
        
        oOpp = [SELECT Id,Name,AccountId, RecordType.DeveloperName, Account.Name, SyncedQuoteId, Pricebook2Id,Approval_Status__c,Quote_Status__c,Expiration_Date__c,Reason_for_Rejection__c,Description,
                Quote_Prepared_For__c,Quote_Prepared_For__r.Phone, Quote_Prepared_For__r.Account.Name, Quote_Prepared_For__r.Account.BillingStreet, 
                Quote_Prepared_For__r.Account.BillingState, Quote_Prepared_For__r.Account.BillingCity, Quote_Prepared_For__r.Account.BillingCountry, 
                Quote_Prepared_For__r.Account.BillingPostalCode,Quote_Prepared_For__r.Fax,Quote_Prepared_For__r.Email,Ship_To_Contact__c,
                Ship_To_Contact__r.Account.Name, Ship_To_Contact__r.Account.ShippingStreet, Ship_To_Contact__r.Account.ShippingState, Ship_To_Contact__r.Account.ShippingCity,
                Ship_To_Contact__r.Account.ShippingCountry, Ship_To_Contact__r.Account.ShippingPostalCode, 
                (SELECT Id,Description,Discount,Discount_Reason__c,PricebookEntryId,Quantity,UnitPrice FROM OpportunityLineItems)
                FROM Opportunity WHERE Id = :sOpportunityId];

        
        // Get the Quote record type name from Opportunity record type name
        String sQuoteRecordType;
        if(oOpp.RecordType.DeveloperName != null) {
            List<RecordType> lstRecordType = [SELECT Id FROM RecordType WHERE SObjectType = 'Quote' AND DeveloperName = :mapOppToQuoteRecordType.get(oOpp.RecordType.DeveloperName) ];
            sQuoteRecordType = (lstRecordType !=null && lstRecordType.size()>0  ? lstRecordType[0].Id : null);
        }
    
        Quote oQuoteRecord = new Quote(Name= oOpp.Name + '- Quote',OpportunityId=oOpp.Id, RecordTypeId = sQuoteRecordType, Pricebook2Id=oOpp.Pricebook2Id, Status=oOpp.Quote_Status__c,
                                        ExpirationDate=oOpp.Expiration_Date__c, Reason_for_Rejection__c=oOpp.Reason_for_Rejection__c, Description=oOpp.Description,
                                        Ship_To_Contact__c = oOpp.Ship_To_Contact__c, ContactId=oOpp.Quote_Prepared_For__c, Phone=oOpp.Quote_Prepared_For__r.Phone, 
                                        Email=oOpp.Quote_Prepared_For__r.Email, Fax=oOpp.Quote_Prepared_For__r.Fax, BillingName = oOpp.Account.Name, ShippingName = oOpp.Ship_To_Contact__r.Account.Name,
                                        ShippingStreet = oOpp.Ship_To_Contact__r.Account.ShippingStreet, ShippingCity = oOpp.Ship_To_Contact__r.Account.ShippingCity,  
                                        ShippingState = oOpp.Ship_To_Contact__r.Account.ShippingState, shippingCountry = oOpp.Ship_To_Contact__r.Account.ShippingCountry, ShippingPostalCode = oOpp.Ship_To_Contact__r.Account.ShippingPostalCode, 
                                        BillingStreet = oOpp.Quote_Prepared_For__r.Account.BillingStreet,  BillingCity = oOpp.Quote_Prepared_For__r.Account.BillingCity,  
                                        BillingState = oOpp.Quote_Prepared_For__r.Account.BillingState, billingCountry = oOpp.Quote_Prepared_For__r.Account.BillingCountry, BillingPostalCode = oOpp.Quote_Prepared_For__r.Account.BillingPostalCode
                                        );
        insert oQuoteRecord;
        
        // Set the Quote Id (Will be returned when invoked from OpportunityLineItem page)
        sQuoteId = oQuoteRecord.Id;
        
        // Get Fresh Values from DB:
        oQuoteRecord = [SELECT Id,Name,ContactId FROM Quote WHERE Id =: oQuoteRecord.Id];
        
        // Create new QuoteLineItems:
        system.debug('%%'+'after quote creation: '+oOpp.OpportunityLineItems); 
        if(oOpp.OpportunityLineItems!=null && oOpp.OpportunityLineItems.size()>0){
            List<QuoteLineItem> lstQLI = new List<QuoteLineItem>();
            for(OpportunityLineItem oli : oOpp.OpportunityLineItems){
                QuoteLineItem qli = new QuoteLineItem();
                qli.QuoteId = oQuoteRecord.Id;
                qli.Description = oli.Description;
                qli.Discount = oli.Discount;
                qli.Discount_Reason__c = oli.Discount_Reason__c;
                qli.PricebookEntryId = oli.PricebookEntryId;
                qli.Quantity = oli.Quantity;
                qli.UnitPrice = oli.UnitPrice;
                lstQLI.add(qli);
            }
            insert lstQLI;
        }
            
        return null;
    }
    
    /*********************************************************************************
    Method Name    : returnToOpportunity
    Description    : Redirects to the parent opportunity page
    Return Type    : PageReference 
    Parameter      : None
    *********************************************************************************/
    Public PageReference returnToOpportunity(){
        system.debug('%%%'+oOpp.Id);
        PageReference oRedirectPage = new PageReference('/'+oOpp.Id); 
        oRedirectPage.setRedirect(true);
        return oRedirectPage;
    }
}