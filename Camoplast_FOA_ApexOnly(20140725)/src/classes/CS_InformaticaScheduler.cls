Global class CS_InformaticaScheduler implements Schedulable {
    Private static Integer RequestTimeout = 60000;
    Public Static String sEndPointUrl {get{ return 'http://test2145.apiary-mock.com/Products';} set;}
    Public Static String sEndPointUrl2 {get{ return 'http://requestb.in/11tk6wa1';} set;}

    Public Static String sOutputMessage {get;set;}
    Public Static String sFinalEndPointURL {get;set;}
    Public CS_InformaticaScheduler()  {sFinalEndPointURL = sEndPointUrl; }

    // Execute method: Method to run when the scheduler Runs on specified time. Used to make a callout to Informatica
    Global void execute(SchedulableContext SC) {
        // execute the DummyCallout Method
        
        //CS_InformaticaScheduler scheduler = new CS_InformaticaScheduler();
        //scheduler.DummyCallout();
        
        CS_InformaticaScheduler.DummyCallout();
    }

    // Method to run the job on demand.
    Global Static void TestDummyCallout() {
        //CS_InformaticaScheduler scheduler = new CS_InformaticaScheduler();
        //scheduler.DummyCallout();
        CS_InformaticaScheduler.DummyCallout();
    }

    // Make a callout to Informatica (now using a dummy url)
    Global Static void DummyCallout(){
        // Callout to given Informatica end point:
        Http httpGetRequest = new Http();
        HttpRequest httpReq = new HttpRequest();
        httpReq.setHeader('Content-type', 'application/x-www-form-urlencoded');
        httpReq.setHeader('Content-length', '0');
        httpReq.setEndpoint( sFinalEndPointURL!=null ? sFinalEndPointURL :sEndPointUrl2);
        httpReq.setMethod('POST');
        httpReq.setTimeout(RequestTimeout);
        HttpResponse httpResponse = httpGetRequest.send(httpReq);
        system.debug(' %%%' +httpResponse);
        sOutputMessage = String.valueOf(httpResponse);
    }
    
    
    
    
    // Creates a schedule for that class each day at 1AM.
    Global Static void ScheduleJob(){
        CS_InformaticaScheduler is = new CS_InformaticaScheduler();
        // Schedule the job to run at 1AM at every day.
        String sScheduleFor1AM = '0 0 1 * * ?';
        String jobID = system.schedule('Informatica Scheduled Job', sScheduleFor1AM, is);
    }
}