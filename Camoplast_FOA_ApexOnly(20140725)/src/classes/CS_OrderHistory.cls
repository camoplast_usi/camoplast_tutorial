public with sharing class CS_OrderHistory {
public Order[] orderHistory{get;set;}
    public CS_OrderHistory(ApexPages.StandardController controller) {
        string s =ApexPages.currentPage().getParameters().get('AccountId');
        Id AccountId=Id.valueOf(s);
         orderHistory= [select Id,Name,Total_Price__c from Order where AccountId=:AccountId];
        
    }



}