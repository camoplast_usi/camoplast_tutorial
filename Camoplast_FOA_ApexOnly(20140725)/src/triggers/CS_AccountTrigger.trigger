/*************************************************************************************
Trigger Name    : CS_AcountTrigger
Description     : Trigger for ‘Account' object. 
Created By      : Divya A N
Created Date    : 14-Jul-14
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Divya A N               14-Jul-14                  Initial Version
************************************************************************************/
trigger CS_AccountTrigger on Account (before insert,before update,after insert,after update) {
    
    if(Trigger.isBefore && Trigger.isInsert){
        CS_Trigger_Account.updateAssignedPriceBook(trigger.new);
    }
    if(Trigger.isAfter && Trigger.isInsert){
        CS_Trigger_Account.createJunctionObjectRecord(trigger.new);
    }
    else if(Trigger.isBefore && Trigger.isUpdate){
        CS_Trigger_Account.updateAssignedPriceBook(trigger.new);
    }

}