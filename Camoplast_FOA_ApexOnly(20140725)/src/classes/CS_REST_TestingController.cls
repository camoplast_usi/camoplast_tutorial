/*********************************************************************************
Class Name      : CS_REST_TestingController 
Description     : Class to test inventory call out (to Informatica)
Created By      : Jayadev Rath
Created Date    : 01-Aug-2014
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
Jayadev Rath                01-Aug-2014            Initial Version
----------------------------------------------------------------------------------
**********************************************************************************/   
Public class CS_REST_TestingController {
    Private static Integer RequestTimeout = 60000;
    Public Static Boolean bGetDataFromExtSystem {get;set;}
    //Public Static String sEndPointUrl {get{ return 'http://demo9115761.mockable.io/InventoryPerLocation/Location1';} set;}

    Public Static String sEndPointUrl {get{ return 'https://ps1w2.rt.informaticacloud.com/active-bpel/public/rt/000V28/SumitRunIntegrationLocation';} set;}
    
    Public Static String sOutputMessage {get;set;}
    Public Static String sFinalEndPointURL {get;set;}
    Public CS_REST_TestingController ()  {sFinalEndPointURL = sEndPointUrl; }
    
    Public Static DateTime dtStartTime {get;set;}
    Public Static DateTime dtEndTime {get;set;}
    Public Static String sStartTime {get;set;}
    Public Static String sEndTime {get;set;}    
    Public Static String SelectedLocation {get;set;}
    //Public Static String sInformaticaResult {get;set;}
    
    // Make a callout to Informatica (now using a dummy url)
    Public Static void DummyCallout2()
    {
        ICRT_Log__c newLog = new ICRT_Log__c();
        newLog.Message__c = 'This is a test!';
        newLog.Source_Id__c = '123123';
        newLog.External_Id__c = '456456';
        newLog.Status__c = 'Success';
        newLog.Operation_Id__c = 'DummyCallout';

        //Prepare http req
        HttpRequest req = new HttpRequest();
        req.setEndpoint('');
        req.setMethod('Post');
        
        String JSONString = JSON.serialize(newLog);    
        JSONString += string.format('\n"Session_Id__c" : "{0}"', new string[]{UserInfo.getSessionId()});
        req.setBody(JSONString);
        
        Http http = new Http();
        HTTPResponse res = http.send(req);
        System.debug(res.getBody());
    }

    Public Static void DummyCallout(){
        // Callout to given Informatica end point:
        Http httpGetRequest = new Http();
        HttpRequest httpReq = new HttpRequest();
        //httpReq.setHeader('Content-type', 'application/x-www-form-urlencoded');
        httpReq.setHeader('Content-length', '0');
        httpReq.setEndpoint( sEndPointUrl );
        httpReq.setMethod('GET');
        httpReq.setTimeout(RequestTimeout);
        
        dtStartTime = DateTime.Now();
        sStartTime = ''+ dtStartTime.Hour() +':'+ dtStartTime.Minute() + ':'+ dtStartTime.Second()+ ' '+ dtStartTime.MilliSecond();
        
        HttpResponse httpResponse = httpGetRequest.send(httpReq);
        system.debug(' %%%' +httpResponse);
        system.debug(' %%%' +httpResponse.getBody());
        sOutputMessage = String.valueOf(httpResponse.getBody());
        
        dtEndTime = DateTime.Now();
        sEndTime = ''+ dtEndTime.Hour() +':'+ dtEndTime.Minute() + ':'+ dtEndTime.Second()+ ' '+ dtEndTime.MilliSecond();

    }
    
    Public Static list<DTT_RestResponse_NA.ProductWrapper> UpdateInventoryDetails(List<PriceBookEntry> lstSourcePriceBookEntries,String Location){

        
        List<String> lstProductCodes = new List<String>();
        
        // Get the Product Codes and respective location details
        For(PriceBookEntry pbe: lstSourcePriceBookEntries) {
            lstProductCodes.add(pbe.Product2.ProductCode);
        }    

        DTT_RestPayload_NA.ProductInformation oPI = new  DTT_RestPayload_NA.ProductInformation(Location,lstProductCodes, 'solidealUSA', UserInfo.getSessionId(), DTT_ICRT_Helper.ServerURL);
        DTT_RestClient.DTT_Rest_Inventory restCall = new DTT_RestClient.DTT_Rest_Inventory(opi);
        return  restCall.ParseBody();
    }

        
    Public Static List<PriceBookEntry> FillInventoryDetails(List<PriceBookEntry> lstSourcePriceBookEntries,String Location, list<DTT_RestResponse_NA.ProductWrapper> productList)
    {    
        
        List<PriceBookEntry> lstFinalPriceBookEntries = new List<PriceBookEntry>();
        
        map<string,  List<DTT_RestResponse_NA.ProductWrapper>> mapInventoryDetails = new map<string, List<DTT_RestResponse_NA.ProductWrapper>>();


            //from the List create a map for easy access:
            For(DTT_RestResponse_NA.ProductWrapper p: productList) {
                
                if(mapInventoryDetails.get(p.part_no) == null)
                    mapInventoryDetails.put(p.part_no, new List<DTT_RestResponse_NA.ProductWrapper>()) ;
                
                List<DTT_RestResponse_NA.ProductWrapper> lstTempList = mapInventoryDetails.get(p.part_no);
                lstTempList.add(p);

                // Pierre Dufour - 2014-08-28 - Not required.  This is a pointer.
                //mapInventoryDetails.put(p.part_no, lstTempList);
            }

            system.debug('CS_REST_TestingController.cls.FillInventoryDetails ' + mapInventoryDetails);
            
            For(PriceBookEntry p: lstSourcePriceBookEntries) {
                List<DTT_RestResponse_NA.ProductWrapper> InventoryDetail = mapInventoryDetails.get(p.Product2.ProductCode);

                system.debug('CS_REST_TestingController.cls.InventoryDetail ' + InventoryDetail);

                if(InventoryDetail!=null ) { 
                    for(DTT_RestResponse_NA.ProductWrapper inventoryWrapper: InventoryDetail ) {                        
                        // Pierre Dufour - 2014-08-28
                        //if(Location == null || Location == 'All Locations' ) {
                            
                            PriceBookEntry oPbe2 = p.clone(true,true);
                        
                            // Populate values from the recently inventory web_saleable_flagrvice
                            oPbe2.Product2.On_hand__c  = Decimal.valueOf(inventoryWrapper.in_stock);
                            oPbe2.Product2.Reserved__c = Decimal.valueOf(inventoryWrapper.qty_alloc);
                            oPbe2.Product2.Ordered__c  = Decimal.valueOf(inventoryWrapper.commit_ed);
                            oPbe2.Product2.Transit__c  = Decimal.valueOf(inventoryWrapper.transit);
                            oPbe2.Product2.AvailableInventory__c = Decimal.valueOf(inventoryWrapper.qty_scheduled);
                            oPbe2.Product2.Location__c = inventoryWrapper.location;
                            
                            // Get the related object values from the old variable:
                            oPbe2.Product2.Product_Line__c = p.Product2.Product_Line__c;
                            oPbe2.Product2.Brand__c = p.Product2.Brand__c;
                            oPbe2.Product2.ProductCode = p.Product2.ProductCode;
                            oPbe2.Product2.Name = p.Product2.Name; 
                            //oPbe2.Product2.Public_PID__c = p.Product2.Public_PID__c;
                            oPbe2.Product2.Family = p.Product2.Family;
                            oPbe2.Product2.IsActive = p.Product2.IsActive;
                            oPbe2.Product2.Weight__c = p.Product2.Weight__c;
                            oPbe2.Product2.Description = p.Product2.Description;
                            
                            //oPbe2.Product2.Commodity__c = p.Product2.Commodity__c;
                            // Commodity renamed to ABC Classification
                            oPbe2.Product2.ABC_Classification__c = p.Product2.ABC_Classification__c;
                            oPbe2.Product2.Unit_of_Measure__c = p.Product2.Unit_of_Measure__c;
                            oPbe2.Product2.Volume__c = p.Product2.Volume__c;

                            lstFinalPriceBookEntries.add(oPbe2);
                    }
                }
            }
        return lstFinalPriceBookEntries;
    }    
}