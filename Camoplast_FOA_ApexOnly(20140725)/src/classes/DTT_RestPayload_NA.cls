public  class DTT_RestPayload_NA {
	
	Public Class ProductInformation{
        Public String Location {get;set;}
        Public String SalesOrganization {get;set;}
        Public String SessionId {get;set;}
        Public String ServerUrl {get;set;}
        Public set<String> ProductCodes {get;set;}
        
        ProductInformation(){}
        
        public ProductInformation(String sLocation, set<String> lstProductCodes, string sSalesOrganization, string sSessionId, string sServerURL){
            this.Location = sLocation;
            this.ProductCodes = lstProductCodes;
            this.SalesOrganization = sSalesOrganization;
            this.SessionId = sSessionId;
            this.ServerUrl = sServerURL;
        }
    }
}