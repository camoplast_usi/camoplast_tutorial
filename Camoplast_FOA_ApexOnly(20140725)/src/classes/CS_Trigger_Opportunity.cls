/*********************************************************************************
Class Name : CS_Trigger_Opportunity
Description : This class will be invoked by the triggers on Opportunity
Created By : Maryam 
Created Date : 14-Jul-14
Modification Log:
---------------------------------------------------------------------------------- 
Developer Date Description
---------------------------------------------------------------------------------- 
Maryam Abbas  14-Jul-14 Initial Version
Divya         24-Jul-14 Show_on_Print__c carry forward
Maryam Abbas  28-Jul-14 Bulkified code
Maryam        18-Aug- 14 Added update updateOptyLocationFields, updateAccountInfo, updatebilltoshiptofields
*********************************************************************************/

public class CS_Trigger_Opportunity{

    /*********************************************************************************
    Method Name    : updateOptyLocationFields
    Description    : Method to set order field values as per business logic
                        a. Set the location either from Ship To Account or Sold To Account or as 'ZSNA'
                        b. Set the 'Tax' field to 0 if Tax exemption is checked at Ship to Account level
    Return Type    : void 
    Author         : Maryam
    Date           : 19/1/2014
    Parameter      : 1. lstOrders: List of order records to upate
    *********************************************************************************/
    public static void updateOptyLocationFields(List<Opportunity> lstOpportunity){
        
        // Collect all Account information:
        Set<Id> setAccountIds = new Set<Id>();
        Set<Id> setOptyIds = new Set<Id>();
        for(Opportunity oOpp: lstOpportunity){
            setOptyIds.add(oOpp.Id);           
            setAccountIds.add(oOpp.AccountId);
            if(oOpp.Ship_To_Account__c !=null) setAccountIds.add(oOpp.Ship_To_Account__c);
        }

        // Get the account details from database
        Map<Id,Account> mapAccountDetails = new Map<Id,Account>([SELECT Id, Locations__c ,Tax_Exemption__c FROM Account where Id in : setAccountIds]);
        Map<Id,Opportunity> mapOpty = new Map<Id,Opportunity>([Select Id, Ship_To_Account__c, AccountId FROM Opportunity where Id=:setOptyIds]);                    
        String recordType;
        list<Location__C>LOC=[Select Id from Location__c where Location_code__c='ZSNA' limit 1];
        for(Opportunity oOpp: lstOpportunity){
        // Update the Location field: Consider ZSNA implying there is no CSC available (no default value specified at the Sold To & Ship To level).
        // oOrder.Location__c = 'ZSNA';
           
           
        if((mapOpty != null && mapOpty.IsEmpty() == false) && ((mapOpty.get(oOpp.Id).Ship_To_Account__c != oOpp.Ship_To_Account__c)||(mapOpty.get(oOpp.Id).AccountId != oOpp.AccountId))||(mapOpty == null || mapOpty.IsEmpty() == true))            
        // If any Ship To Account information is present, it should be prioritized
        {      
                   Map<ID, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Opportunity.getRecordTypeInfosById();
                   if(rtMap.get(oOpp.RecordTypeId) !=null){
                   
                        recordType = rtMap.get(oOpp.RecordTypeId).getName();
                  
                   } 
                   if(recordType == 'North America'){
                       if(oOpp.Ship_To_Account__c != null && mapAccountDetails.get(oOpp.Ship_To_Account__c).Locations__c != null)
                        {    oOpp.Location__c = mapAccountDetails.get(oOpp.Ship_To_Account__c).Locations__c;
                             oOpp.Selling_Location__c = mapAccountDetails.get(oOpp.Ship_To_Account__c).Locations__c;
                      
                        }
                        // If any Location is specified at Sold To Account level consider that
                        else if(mapAccountDetails.get(oOpp.AccountId).Locations__c != null)
                        {    oOpp.Location__c = mapAccountDetails.get(oOpp.AccountId).Locations__c;
                             oOpp.Selling_Location__c = mapAccountDetails.get(oOpp.AccountId).Locations__c;
                      
                        }
                        else
                        {
                            if(LOC!=null && LOC.isEmpty()==false){                               
                                   ID LOCID=LOC.get(0).Id;
                                   System.Debug('Location Id'+LOCID);
                                   oOpp.Location__c = LOC.get(0).Id;
                                   oOpp.Selling_Location__c = LOC.get(0).Id;
                             }
                        }
                   }
                   else
                   {    if(mapAccountDetails.get(oOpp.AccountId).Locations__c != null){
                             oOpp.Location__c = mapAccountDetails.get(oOpp.AccountId).Locations__c;
                             oOpp.Selling_Location__c = mapAccountDetails.get(oOpp.AccountId).Locations__c;                     
                        }
                        else{
                            oOpp.Location__c = null;
                            oOpp.Selling_Location__c = null;
                 
                        }
                   
                   }
         
            
            // Update the Location field:  Set this to Zero if Tax exemption is checked at Ship to Account level
            if(oOpp.Ship_To_Account__c != null  && mapAccountDetails.get(oOpp.Ship_To_Account__c).Tax_Exemption__c == true)
                oOpp.Tax__c = '0';
        }
    }
}
/*********************************************************************************
Method Name : updateOpportunityPriceBook
Description : To update the PriceBookId field when Opportunity is created/updated
Return Type : void
Parameter : 1. lstOpportunity
*********************************************************************************/
    public static void updateOpportunityPriceBook(List<Opportunity> lstOpportunity)
    {      
            Set<Id> CS_setAccountIds = new Set<Id>();
            for(Opportunity oOpty: lstOpportunity){
                CS_setAccountIds.add(oOpty.AccountId);
            }
            List<Pricebook2> CS_lstDefaultPrice = [Select Id FROM Pricebook2 WHERE Pricebook2.IsStandard = true];
                 
            Map<Id,Account> mapAccountDetails = new Map<Id,Account>([SELECT Assigned_Price_Book__c FROM Account where Id in : CS_setAccountIds]);
            for (Opportunity CS_oOpportunity : lstOpportunity){
               
                if(mapAccountDetails.get(CS_oOpportunity.AccountId).Assigned_Price_Book__c != null){ 
                    CS_oOpportunity.PriceBook2Id = mapAccountDetails.get(CS_oOpportunity.AccountId).Assigned_Price_Book__c;
                } 
                else{                   
                    CS_oOpportunity.PriceBook2Id = CS_lstDefaultPrice.get(0).Id; 
                }
            }            
        
    }
/*********************************************************************************
Method Name : updateTermsandConditions
Description : To add terms and conditions when Opportunity is created/updated
Return Type : void
Parameter : 1. lstOpportunity
*********************************************************************************/
    public static void updateTermsandConditions(List<Opportunity> lstOpportunity)
    {            
            Set<Id> CS_setAccountIds = new Set<Id>();
            for(Opportunity oOpty: lstOpportunity){
                CS_setAccountIds.add(oOpty.AccountId);
            }
                           
            Set<Id> setShippingAccountIds = new Set<Id>();
            for(Opportunity oOpty: lstOpportunity){
                setShippingAccountIds.add(oOpty.Ship_to_Account__c);
            }
            
            List<Terms_and_Conditions__c> CS_lst_OptyTerms= new List<Terms_and_Conditions__c>();
            
            Map<Id, List<Terms_and_Conditions__c>> mapTnC = new Map<Id, List<Terms_and_Conditions__c>>();
            Map<Id, List<Terms_and_Conditions__c>> mapTnCShipping = new Map<Id, List<Terms_and_Conditions__c>>();
    
            List<Account> Accounts = [Select Id, (Select Terms_and_Conditions__c, Show_on_Print__c From Terms_and_Conditions__r) From Account where Id in :CS_setAccountIds];
            List<Account> ShippingAccounts = [Select Id, (Select Terms_and_Conditions__c, Show_on_Print__c From Terms_and_Conditions__r) From Account where Id in :setShippingAccountIds];
          
            // loop through all retrieved Accounts 
            for(Account oAcc : Accounts)
            {
               List<Terms_and_Conditions__c> TnC = oAcc.getSObjects('Terms_and_Conditions__r'); // get all Terms and Conditions for the Account
               mapTnC.put(oAcc.Id, TnC); // put the Account Terms and Conditions in a map by Account Id
               
            }
            for(Account oSAcc : ShippingAccounts)
            {
               List<Terms_and_Conditions__c> TnCShip = oSAcc.getSObjects('Terms_and_Conditions__r'); // get all Terms and Conditions for the Account
               mapTnCShipping.put(oSAcc.Id, TnCShip); // put the Account Terms and Conditions in a map by Account Id
               
            }
            List<Terms_and_Conditions__c> lstTnC = new List<Terms_and_Conditions__c>();
            List<Terms_and_Conditions__c> lstTnCShip = new List<Terms_and_Conditions__c>();
            
          
            Terms_and_Conditions__c CS_oOptyTerm;
            for (Opportunity CS_Opty: lstOpportunity){          
                 if( CS_Opty.AccountId !=null)
                 {     lstTnC = mapTnC.get(CS_Opty.AccountId); 
                       if(lstTnC != null){
                           for (Terms_and_Conditions__c CS_oTerm: lstTnC){
                                
                                CS_oOptyTerm = new Terms_and_Conditions__c();
                                CS_oOptyTerm.OpportunityId__c = CS_Opty.Id;
                                CS_oOptyTerm.Terms_and_Conditions__c=CS_oTerm.Terms_and_Conditions__c;
                                CS_oOptyTerm.Show_on_Print__c = CS_oTerm.Show_on_Print__c;
                                CS_lst_OptyTerms.add(CS_oOptyTerm);
                            }   
                        }
                 }
                 if( CS_Opty.Ship_to_Account__c !=null )
                 {    lstTnCShip = mapTnCShipping.get(CS_Opty.Ship_to_Account__c);     
                      if(lstTnCShip != null){      
                          for (Terms_and_Conditions__c CS_oTerm: lstTnCShip){
                                
                                CS_oOptyTerm = new Terms_and_Conditions__c();
                                CS_oOptyTerm.OpportunityId__c = CS_Opty.Id;
                                CS_oOptyTerm.Terms_and_Conditions__c=CS_oTerm.Terms_and_Conditions__c;
                                CS_oOptyTerm.Show_on_Print__c = CS_oTerm.Show_on_Print__c;
                                CS_lst_OptyTerms.add(CS_oOptyTerm);
                            } 
                       }                
                 }          
                
            }
            insert CS_lst_OptyTerms;           
    }
    
/*********************************************************************************
Method Name : updateAccountInfo
Description : To update the Account Info when Opportunity is created
Author      : Maryam
Date        : Aug 18 2014
Return Type : void
Parameter : 1. lstOpportunity
*********************************************************************************/
    public static void updateAccountInfo(List<Opportunity> lstOpportunity)
    {      
            String recordType='';
           
            Set<Id> CS_setAccountIds = new Set<Id>();
            Set<Id> setOptyIds = new Set<Id>();
            for(Opportunity oOpty: lstOpportunity){
                CS_setAccountIds.add(oOpty.AccountId);
                setOptyIds.add(oOpty.Id);
            }
            Map<Id,Opportunity> mapOpty = new Map<Id,Opportunity>([Select Id, AccountId FROM Opportunity where Id=:setOptyIds]);                         
            Map<Id,Account> mapAccountDetails = new Map<Id,Account>([SELECT Locations__c, CurrencyIsoCode, Price_List__c, Terms__c, Price_Group__c, Incoterms__c, Incoterms_Location__c, Complete_Delivery__c, Payment_Method__c FROM Account where Id in : CS_setAccountIds]);
            if(mapOpty == null || mapOpty.IsEmpty() == true) {                             
             
                for (Opportunity CS_oOpportunity : lstOpportunity){
                   
                    Map<ID, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Opportunity.getRecordTypeInfosById();
                    if(rtMap.get(CS_oOpportunity.RecordTypeId) !=null)
                        recordType = rtMap.get(CS_oOpportunity.RecordTypeId).getName();
                    if(recordType =='EMEA Sales Opportunity'||recordType=='EMEA Service Opportunity'){
                        CS_oOpportunity.Location__c = CS_oOpportunity.Location__c==null?mapAccountDetails.get(CS_oOpportunity.AccountId).Locations__c:CS_oOpportunity.Location__c;
                        CS_oOpportunity.Price_List__c =CS_oOpportunity.Price_List__c==null?mapAccountDetails.get(CS_oOpportunity.AccountId).Price_List__c:CS_oOpportunity.Price_List__c;
                        CS_oOpportunity.Price_Group__c =CS_oOpportunity.Price_Group__c==null?mapAccountDetails.get(CS_oOpportunity.AccountId).Price_Group__c:CS_oOpportunity.Price_Group__c;
                        CS_oOpportunity.Incoterms__c = CS_oOpportunity.Incoterms__c==null?mapAccountDetails.get(CS_oOpportunity.AccountId).Incoterms__c:CS_oOpportunity.Incoterms__c;
                        CS_oOpportunity.Incoterms_Location__c = CS_oOpportunity.Incoterms_Location__c==null?mapAccountDetails.get(CS_oOpportunity.AccountId).Incoterms_Location__c:CS_oOpportunity.Incoterms_Location__c;
                        CS_oOpportunity.Payment_Terms__c = CS_oOpportunity.Payment_Terms__c==null?mapAccountDetails.get(CS_oOpportunity.AccountId).Terms__c:CS_oOpportunity.Payment_Terms__c;
                        //CS_oOpportunity.Complete_Delivery__c= mapAccountDetails.get(CS_oOpportunity.AccountId).Complete_Delivery__c;
                        CS_oOpportunity.Payment_Method__c = CS_oOpportunity.Payment_Method__c==null?mapAccountDetails.get(CS_oOpportunity.AccountId).Payment_Method__c:CS_oOpportunity.Payment_Method__c;
                        CS_oOpportunity.CurrencyIsoCode = mapAccountDetails.get(CS_oOpportunity.AccountId).CurrencyIsoCode;
                    }               
                }  
           } 
           else {
               for (Opportunity CS_oOpportunity : lstOpportunity){              
           
                   Map<ID, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Opportunity.getRecordTypeInfosById();
                        if(rtMap.get(CS_oOpportunity.RecordTypeId) !=null)
                            recordType = rtMap.get(CS_oOpportunity.RecordTypeId).getName();
                        if(recordType =='EMEA Sales Opportunity'||recordType=='EMEA Service Opportunity'){
                        if(mapOpty.get(CS_oOpportunity.Id).AccountId != CS_oOpportunity.AccountId){
                            CS_oOpportunity.Location__c = mapAccountDetails.get(CS_oOpportunity.AccountId).Locations__c;
                            CS_oOpportunity.Price_List__c =mapAccountDetails.get(CS_oOpportunity.AccountId).Price_List__c;
                            CS_oOpportunity.Price_Group__c =mapAccountDetails.get(CS_oOpportunity.AccountId).Price_Group__c;
                            CS_oOpportunity.Incoterms__c = mapAccountDetails.get(CS_oOpportunity.AccountId).Incoterms__c;
                            CS_oOpportunity.Incoterms_Location__c = mapAccountDetails.get(CS_oOpportunity.AccountId).Incoterms_Location__c;
                            CS_oOpportunity.Payment_Terms__c = mapAccountDetails.get(CS_oOpportunity.AccountId).Terms__c;
                            //CS_oOpportunity.Complete_Delivery__c= mapAccountDetails.get(CS_oOpportunity.AccountId).Complete_Delivery__c;
                            CS_oOpportunity.Payment_Method__c = mapAccountDetails.get(CS_oOpportunity.AccountId).Payment_Method__c;
                            CS_oOpportunity.CurrencyIsoCode = mapAccountDetails.get(CS_oOpportunity.AccountId).CurrencyIsoCode;
                   
                        }
                   }
               }
        }
                    
        
    }
        
    /*********************************************************************************
    Method Name    : updateBillToShipToFields
    Description    :
    Return Type    : void 
    Parameter      : 1. lstOpportunity: List of opportunity records to upate
    *********************************************************************************/
    public static void updateBillToShipToFields(List<Opportunity> lstOpportunity){
            
            Set<Id> setOptyIds = new Set<Id>();
            Set<Id> setBillToAccountIds = new Set<Id>();
            Set<Id> setShipToAccountIds = new Set<Id>();
            Set<Id> setBillToContactIds = new Set<Id>();
            Set<Id> setShipToContactIds = new Set<Id>();
            
            for(Opportunity oOpty: lstOpportunity){             
                
                setOptyIds.add(oOpty.Id);
                setBillToAccountIds.add(oOpty.AccountId);
                setShipToAccountIds.add(oOpty.Ship_To_Account__c);
                setBillToContactIds.add(oOpty.Quote_Prepared_For__c);
                setShipToContactIds.add(oOpty.Ship_To_Contact__c);
            }
           
             //Get the Account Contact Roles 
             list<AccountContactRole> BillContactId = [Select ContactId FROM AccountContactRole WHERE AccountId = :setBillToAccountIds AND Role='Orders Main Contact' /*AND IsPrimary=True*/ limit 1 ];
             list<AccountContactRole> ShipContactId = [Select ContactId FROM AccountContactRole WHERE AccountId = :setShipToAccountIds AND Role='Service' /*AND IsPrimary=True*/ limit 1 ];
           
             Map<Id,Opportunity> mapOpty = new Map<Id,Opportunity>([Select Id, Ship_To_Account__c, Quote_Prepared_For__c, AccountId, Ship_To_Contact__c FROM Opportunity where Id=:setOptyIds]);                    
             Map<Id,Contact> mapShipToContact = new Map<Id,Contact>([Select Id, AccountId FROM Contact where Id=:setShipToContactIds]); 
             Map<Id,Contact> mapBillToContact = new Map<Id,Contact>([Select Id, AccountId FROM Contact where Id=:setBillToContactIds]);  
             
             if(mapOpty != null && mapOpty.IsEmpty() == false)                              
             {
                 for (Opportunity oOpty: lstOpportunity )
                 {   if(mapOpty.get(oOpty.Id).Ship_To_Account__c != oOpty.Ship_To_Account__c)
                       if(ShipContactId!=null&&ShipContactId.IsEmpty()==false)
                         oOpty.Ship_To_Contact__c=ShipContactId.get(0).ContactId;
                       else
                         oOpty.Ship_To_Contact__c=null;
                     else if(mapOpty.get(oOpty.Id).Ship_To_Contact__c!= oOpty.Ship_To_Contact__c )
                         if(oOpty.Ship_To_Contact__c != null&&mapShipToContact!=null&&mapShipToContact.IsEmpty()==false )
                             oOpty.Ship_To_Account__c =mapShipToContact.get(oOpty.Ship_To_Contact__c).AccountId;
                         else 
                             oOpty.Ship_To_Account__c=null;
                     if(mapOpty.get(oOpty.Id).AccountId != oOpty.AccountId)
                       if(BillContactId!=null&&BillContactId.IsEmpty()==false)
                         oOpty.Quote_Prepared_For__c=BillContactId.get(0).ContactId;
                       else 
                         oOpty.Quote_Prepared_For__c=null;
                     else if(mapOpty.get(oOpty.Id).Quote_Prepared_For__c != oOpty.Quote_Prepared_For__c)
                        if(oOpty.Quote_Prepared_For__c!=null&&mapBillToContact!=null&&mapBillToContact.IsEmpty()==false)
                           oOpty.AccountId=mapBillToContact.get(oOpty.Quote_Prepared_For__c).AccountId; 
                        //else
                        //   oOpty.AccountId=null; 
                  }
              }
              else{
                 for (Opportunity oOpty: lstOpportunity)
                 {   if(oOpty.Ship_To_Account__c!=null&&ShipContactId!=null&&ShipContactId.IsEmpty()==false)
                         oOpty.Ship_To_Contact__c=ShipContactId.get(0).ContactId;                      
                     else if(oOpty.Ship_To_Contact__c!=null && mapShipToContact!=null && mapShipToContact.IsEmpty()==false)
                         oOpty.Ship_To_Account__c =mapShipToContact.get(oOpty.Ship_To_Contact__c).AccountId;
                     if(oOpty.AccountId != null&&BillContactId!=null&&BillContactId.IsEmpty()==false)
                         oOpty.Quote_Prepared_For__c=BillContactId.get(0).ContactId;
                     else if(oOpty.Quote_Prepared_For__c!=null)
                       oOpty.AccountId=mapBillToContact.get(oOpty.Quote_Prepared_For__c).AccountId; 
                  }
              
              
              }
             
                  
                 
    
    }
}