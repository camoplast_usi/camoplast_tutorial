trigger DTT_Event_Short on Event (before insert, before update) {

	for (Event e : trigger.new)
	{
		if (e.Description != null)
			e.Short_Description__c = e.Description.length() > 125 ? e.Description.substring(0, 125) + '...' : e.Description;
	}

}