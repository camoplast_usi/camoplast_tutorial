public class CS_Opportunity_Update{
/*********************************************************************************
Method Name : updateQuotepricebook
Description : To update the Quote Price Book if it is blank.
Return Type : void
Parameter : 
*********************************************************************************/

 public static void updateQuotepricebook(List<Opportunity> lstOppo)
{
for (Opportunity Oppnew: lstOppo)
         {

        ID RId=Oppnew.Id;
         
        
        ID oldPriceid=Oppnew.Pricebook2Id;
        ID mainid=Oppnew.CS_Main_Pricebook__c;
        ID quoteid=Oppnew.CS_Quote_Price_Book__c;
        
             
         
         if(mainid==null)
         {
         Oppnew.CS_Main_Pricebook__c=oldPriceid;
         mainid=Oppnew.CS_Main_Pricebook__c;
         }

        if (quoteid==null && oldPriceid==null)
            {
               Oppnew.CS_Quote_Price_Book__c=mainid;
               
             }
        if (quoteid==null && oldPriceid!=null)
        {
            Oppnew.CS_Quote_Price_Book__c=oldPriceid;
        }
        
        if(quoteid!=oldPriceid)
        {
        Oppnew.Pricebook2Id=Oppnew.CS_Quote_Price_Book__c;
        }
        
        }
        
}
/*********************************************************************************
Method Name : updateOpportunityBilltoContact
Description : To update the Bill to Contact with the Account's primary Contact with Role as Orders Main Contact and
update Ship to Account based on Ship to Contact.
Return Type : void
Parameter : lstOrder
Author: Laxmy M Krishnan
Date : 8/4/2014
*********************************************************************************/

 public static void updateOpportunityBilltoContact(List<Opportunity>lstOppo)
{

// Collect all Account information from the Opportunity.
        Set<Id> setAccountIds = new Set<Id>();
        for(Opportunity oOppo: lstOppo){
            setAccountIds.add(oOppo.AccountId);
                }
                           
  if(setAccountIds!=null)
 { 
 
 //Get the Account Contact Roles 
 list<AccountContactRole> ContactId = [Select ContactId FROM AccountContactRole WHERE AccountId = :setAccountIds AND Role='Orders Main Contact' AND IsPrimary=True limit 1 ];
 
 
         for (Opportunity Opponew: lstOppo)
             {

                if (ContactId!=null && ContactId.isEmpty()==false)
                    {
                    
                        ID accId=Opponew.AccountId;
                        
                        ID BillContact = Opponew.Quote_Prepared_For__c;
                        
                        ID Contact = ContactId.get(0).ContactId;
                       
                             if (BillContact==null )
                                    {    
                                     
                                        Opponew.Quote_Prepared_For__c=Contact;
                                    }
            
                    }
 
 
             }  

     
    }        
    }
  /*********************************************************************************
Method Name : updateOpportunityShiptoAcc to Account
Description : To Ship to Account based on Ship to Contact.
Return Type : void
Parameter : lstOppo
Author: Laxmy M Krishnan
Date : 8/5/2014
*********************************************************************************/
public static void updateOrderShiptoAcc(List<Opportunity>lstOppo)
{

// Collect all Contactinformation from the Orders.
        Set<Id> setContactIds = new Set<Id>();
        for(Opportunity oOppo: lstOppo){
            setContactIds .add(oOppo.Ship_To_Contact__c);
                }

 if(setContactIds!=null)
 {
 Map<Id,Contact> mapContact = new Map<Id,Contact>([Select Id, AccountId FROM Contact where Id=:setContactIds]);
 

    for (Opportunity Opponew: lstOppo)
         {

             if(mapContact!=null && mapContact.isEmpty()==false )
               {
       
                ID ShiptoAccount = Opponew.Ship_To_Account__c;
                ID ShiptoContact = Opponew.Ship_To_Contact__c;
                ID AccountId = mapContact.get(ShiptoContact).AccountId;
                    if(ShiptoAccount ==null && ShiptoContact!=null)
                        {
        
                            Opponew.Ship_To_Account__c =AccountId; 
        
       
                            }      
       
                }
        }

        
        }
        }
}