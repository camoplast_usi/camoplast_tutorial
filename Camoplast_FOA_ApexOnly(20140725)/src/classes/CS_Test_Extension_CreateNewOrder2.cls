/*********************************************************************************
Class Name      : CS_Test_Extension_CreateNewOrder
Description     : Class to Cover CS_Extension_CreateNewOrder
Created By      : Jayadev Rath
Created Date    : 02-Sep-2014
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Jayadev Rath                02-Sep-2014            Initial Version
*********************************************************************************/
@isTest
Private Class CS_Test_Extension_CreateNewOrder2{
    
    Private Static TestMethod Void Test_CreateNewOrderFromQuote(){
    
        // Create a PriceBook
        Pricebook2 oPriceBook=new Pricebook2(Name='TestPriceBook',Construction_Discount_Class__c= 'C',MH_Discount_Class__c= 'B');
        Insert oPriceBook;
    
        // Create a Product:
        Product2 oProduct = new Product2(Name='TestProduct');
        Insert oProduct;
        
        // Create a PriceBookEntry Record with above pricebook and product:
        PriceBookEntry oPbe = new PriceBookEntry(PriceBook2Id = oPriceBook.Id, Product2Id = oProduct.Id,UnitPrice =200);
        Insert oPbe;
        
        // Create an Account:
        Account oAcc = new Account(Name = 'TestAccount',CurrencyIsoCode = 'USD',Con_Discount_Class__c = 'C',MH_Discount_Class__c =  'B');
        Insert oAcc;
        
        // For the above Account create an Opportunity
        Opportunity oOpportunity = new Opportunity(Name = 'TestOpportunity', AccountId=oAcc.Id, CloseDate= Date.Today().addDays(10),StageName='Qualification');
        Insert oOpportunity;
        
        // Create a Quote with the new Opportunity:
        Quote oQuote = new Quote(Name='TestQuote',OpportunityId = oOpportunity.Id);
        Insert oQuote;
        
        // Create Quote line items:
    }
}