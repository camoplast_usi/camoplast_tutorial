public class CS_Opportunity_Line_Item_Update{
/*********************************************************************************
Method Name : updateApprovalStatusOppoLi
Description : To update the Approval Status field on Opportunity Line Item based on Sales Price.
Return Type : void
Parameter : 1. lstOppoLine
*********************************************************************************/

 public static void updateApprovalStatusOppoLi(List<OpportunityLineItem> lstOppoLine)
{


Set<Id> setOppoLinePBIds = new Set<Id>();
for(OpportunityLineItem oOpp: lstOppoLine){
setOppoLinePBIds.add(oOpp.PricebookEntryId);
}
system.debug('OppPbId'+setOppoLinePBIds);               
Map<Id,PricebookEntry> mapPricebookDetails = new Map<Id,PricebookEntry>([Select Id, UnitPrice FROM PricebookEntry WHERE Id = :setOppoLinePBIds]);
   for (OpportunityLineItem OppnewLi: lstOppoLine)
         {
                            
           ID PricebookEntryId =  OppnewLi.PricebookEntryId; 
           Decimal ListPrice = mapPricebookDetails.get(PricebookEntryId ).UnitPrice;
           If(OppnewLi.UnitPrice<ListPrice*0.95)
                {
                OppnewLi.Approval_Status__c='Required';
                }
                
            else
                {
                OppnewLi.Approval_Status__c='Not Required';
                }
            
           }
}
}