public with sharing class DTT_Account_Map_Ctrl {

	private final Account refAccount;
    private boolean ShippingMode = false;
    
    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public DTT_Account_Map_Ctrl(ApexPages.StandardController stdController) {
        
        refAccount = [Select Id, Ship_To__c, Name, BillingStreet, BillingCity, BillingState, BillingCountry, ShippingStreet, ShippingCity, ShippingState, ShippingCountry from Account where Id =: stdController.getRecord().Id];
        
        if (refAccount.Ship_To__c)
            ShippingMode = true;    
        
        else if (refAccount.BillingStreet == null)
        {

            if (refAccount.ShippingStreet != null)
                ShippingMode = true;

            if (refAccount.ShippingCity != null)
                ShippingMode = true;
        }
    }

    public void Reload()
    {

    }

    public void Shipping()
    {
        ShippingMode = true;
    }

    public void Billing()
    {
        ShippingMode = false;
    }

    public string SearchString
    {
        get
        {
            string retVal = ShippingMode ? CleanAddress(ShippingAddress) : CleanAddress(BillingAddress);   
            system.debug(retVal);
            return retVal;
        }
    }

    public string BillingAddress
    {
        get
        {
            //string address =  string.format('{0} {1} {2} {3} {4}', new string[]{refAccount.Name, refAccount.BillingStreet,  refAccount.BillingCity, refAccount.BillingState, refAccount.BillingCountry});
            string address =  string.format('{0} {1} {2} {3} {4}', new string[]{'', refAccount.BillingStreet,  refAccount.BillingCity, refAccount.BillingState, refAccount.BillingCountry});
            return address;
        }
    }

    public string ShippingAddress
    {
        get
        {
            //string address = string.format('{0} {1} {2} {3} {4}', new string[]{ refAccount.Name, refAccount.ShippingStreet, refAccount.ShippingCity, refAccount.ShippingState, refAccount.ShippingCountry});
            string address = string.format('{0} {1} {2} {3} {4}', new string[]{ '', refAccount.ShippingStreet, refAccount.ShippingCity, refAccount.ShippingState, refAccount.ShippingCountry});
            return address;
        }
    }

    public string CleanAddress(string address)
    {
        return address.replace('null', '').trim();
    }

    public boolean ShippingVisible
    {
        get
        {
            boolean retVal = false;
            
            if (refAccount.ShippingCountry != null)
                retVal = true;

            if (refAccount.ShippingState != null)
                retVal = true;
            
            if (refAccount.ShippingCity != null)
                retVal = true;

            if (refAccount.ShippingStreet != null)
                retVal = true;

            return retVal;
        }
    }

    public boolean BillingVisible
    {
        get
        {
            boolean retVal = false;
            
            if (refAccount.BillingCountry != null)
                retVal = true;

            if (refAccount.BillingState != null)
                retVal = true;
            
            if (refAccount.BillingCity != null)
                retVal = true;

            if (refAccount.BillingStreet != null)
                retVal = true;
            
            return retVal;
        }
    }

    public boolean BothVisible
    {
        get
        {
            return ShippingVisible && BillingVisible;
        }
    }


}