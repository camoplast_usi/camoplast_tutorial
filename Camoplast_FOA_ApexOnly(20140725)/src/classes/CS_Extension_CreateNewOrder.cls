/*************************************************************************************
Classe Name : CS_Extension_CreateNewOrder
Description : Create a new order from Quote (Used for the button click on Quote)
Created By  : Jayadev Rath
Created Date: 24-July-14
Modification Log:
---------------------------------------------------------------------------------- 
Developer             Date             Description
---------------------------------------------------------------------------------- 
Jayadev Rath         24-Jul-2014       Initial Version
Jayadev Rath         31-Jul-2014       Updated CreateOrder() method to populate QuoteLineItems and Term&Condition records from Quote record.
Jayadev Rath         26-Aug-2014       Updated CreateOrder() method to populate new additional fields from Quote Line Item to Order Item (To avoid validation rules)
Divya A N            09-Sep-2014       Updated CreateOrder() method to populate Ship To Account from the junction object Related Account
************************************************************************************/
Public Class CS_Extension_CreateNewOrder{
    
    Public String sQuoteId {get;set;}
    // Stores Quote to Order record type names mapping
    Private Map<String,String> mapQuoteToOrderRecordType {
        get{
            return new Map<String,String>{
                'EMEA_Sales' => 'EMEA_Sales_Order',
                'EMEA_Service' => 'EMEA_Service_Order',
                //'EMEA_Sales_Quote' => 'EMEA_Sales_Order',
                //'EMEA_Service_Quote' => 'EMEA_Service_Order',
                'North_America' => 'North_America'
            };
        }
        set;
    }
    
    
    /*********************************************************************************
    Method Name    : Constructor
    Description    : Collect Quote Id from Standard Controller
                     2) Copy all the Line Items.    
                     3) Copy all Terms & Condition records
    Return Type    : None
    Parameter      : None
    *********************************************************************************/
    public CS_Extension_CreateNewOrder(ApexPages.StandardController controller){
        // Get the Quote details:
        sQuoteId = controller.getId();
    }
    
    
    /*********************************************************************************
    Method Name    : createOrder
    Description    : 1) Create an Order record with details from Quote.    
                     2) Copy all the Line Items.    
                     3) Copy all Terms & Condition records
    Return Type    : PageReference 
    Parameter      : None
    *********************************************************************************/
    Public PageReference createOrder(){
        PageReference oRedirectionPage;
        try {
            
            if(sQuoteId == null || sQuoteId =='') {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.CS_Quote_Information_Missing));
                return null;
            }
            Quote oQuote = [SELECT Id,Incoterms__c,AccountId,OpportunityId, Opportunity.Ship_To_Account__c, RecordType.DeveloperName,
                            Opportunity.AccountId,Opportunity.CS_Main_Pricebook__c, Opportunity.Quote_Prepared_For__c,
                            (SELECT Id,Description,Discount,PricebookEntryId,Quantity,UnitPrice, Discount_Reason__c, Discount_Reason_Values__c,
                                 Wex__c, Fitted_On_Site__c,Torque_Setting__c, Tyre_Condition__c, Location__c, Wheel_Position__c FROM QuoteLineItems),
                            (SELECT Id,Terms_and_Conditions__c,Show_on_Print__c FROM Terms_and_Conditions__r)
                            FROM Quote WHERE Id= :sQuoteId];
                            //
            Id OrderRecordTypeId;
            // Get the Order Record Type Name same as Quote record type
            if(oQuote.RecordType.DeveloperName != Null) {
                List<RecordType> lstRecordType = [SELECT Id FROM RecordType WHERE sObjectType = 'Order' AND DeveloperName = :mapQuoteToOrderRecordType.get(oQuote.RecordType.DeveloperName) ];
                if(lstRecordType != null)
                    OrderRecordTypeId = lstRecordType[0].Id;
            }
            //Divya 9/9 :To Select The Junction Object Record for the combination of Ship To and Sold To Accounts used in the Quote
            list<Acc_Junction__c> oAccJunction = [SELECT Id FROM Acc_Junction__c WHERE Ship_To__c =: oQuote.Opportunity.Ship_To_Account__c AND Sold_To__c =: oQuote.Opportunity.AccountId];
            
            
            // Create a new Order:
            Order oNewOrder = new Order();
            oNewOrder.RecordTypeId = OrderRecordTypeId;
            oNewOrder.AccountId = oQuote.Opportunity.AccountId;
            oNewOrder.Status= 'Draft';
            oNewOrder.QuoteId = oQuote.Id;
            oNewOrder.PoNumber='Please enter PO if required';//Laxmy hardcoded this. Awaiting design decision, once made will update this. 9/15/2014.
            for(Acc_Junction__c relatedAccount :oAccJunction){
            oNewOrder.Ship_To_Account_RelatedAccount__c = relatedAccount.Id;
            }
            //Divya 9/9 : Ship_To_Account will now look up to the Related Accounts Junction object
            //oNewOrder.Ship_To_Account__c = oQuote.Opportunity.Ship_To_Account__c;
            oNewOrder.PriceBook2Id = oQuote.Opportunity.CS_Main_Pricebook__c;
            oNewOrder.BillToContactId = oQuote.Opportunity.Quote_Prepared_For__c;
            oNewOrder.EffectiveDate = Date.Today();
            //oNewOrder.Incoterms__c=oQuote.Incoterms__c;
            
            // Set the bOrderCreatedFromQuote flag to true, so that Term&Condition records will not be copied from Account. (copy from quote in following steps)
            CS_CONSTANTS.bOrderCreatedFromQuote = True;
            Insert oNewOrder;
            
            // After the Order is created, create Order Items:
            If(oQuote.QuoteLineItems != null && oQuote.QuoteLineItems.size()>0) {
                List<OrderItem> lstOrderItem = new List<OrderItem>();
                For(QuoteLineItem qli: oQuote.QuoteLineItems) {
                    // Create a new Order Item from Quote Line Items
                    OrderItem oOrdItem = new OrderItem();
                    oOrdItem.OrderId = oNewOrder.Id;
                    oOrdItem.Description = qli.Description;
                    oOrdItem.Discount__c = qli.Discount;
                    oOrdItem.Discount_Reason__c = qli.Discount_Reason__c;
                    oOrdItem.Discount_Reason_Values__c = qli.Discount_Reason_Values__c;
                    oOrdItem.Fitted_On_Site__c = qli.Fitted_On_Site__c;
                    oOrdItem.Torque_Setting__c = qli.Torque_Setting__c;
                    oOrdItem.Tyre_Condition__c = qli.Tyre_Condition__c;
                    oOrdItem.Wex__c = qli.Wex__c;
                    oOrdItem.Wheel_Position__c = qli.Wheel_Position__c;
                    oOrdItem.PricebookEntryId = qli.PricebookEntryId;
                    oOrdItem.Quantity = qli.Quantity;
                    oOrdItem.UnitPrice = qli.UnitPrice;
                    //Maryam added location 
                    oOrdItem.Location__c = qli.Location__c;
                    
                    
                    // Add the new Order Item to the insert list
                    lstOrderItem.add(oOrdItem);
                }
                // If any Order Items found, then insert those
                If(lstOrderItem.size()>0)    
                    Insert lstOrderItem;
            }
            
            // If any Term & Condition record found for the Quote, insert those for the Order too (Not copied from Account)
            If(oQuote.Terms_and_Conditions__r != null && oQuote.Terms_and_Conditions__r.size()>0) {
                List<Terms_and_Conditions__c> lstTnC = new List<Terms_and_Conditions__c>();
                
                // Create a Term Condition Record for every Term Condition record on Quote
                For(Terms_and_Conditions__c tnc: oQuote.Terms_and_Conditions__r){
                    Terms_and_Conditions__c oOrderTnC = new Terms_and_Conditions__c();
                    oOrderTnC.OrderId__c = oNewOrder.Id;
                    oOrderTnC.Terms_and_Conditions__c = tnc.Terms_and_Conditions__c;
                    oOrderTnC.Show_on_Print__c = tnc.Show_on_Print__c;
                    
                    // Add the new Term Condition record for insertion:
                    lstTnC.add(oOrderTnC);
                }
                // If any Term Condition records found for the order, insert those
                If(lstTnC.size()>0)
                    Insert lstTnC;
            }
            
            // If no errors found, Set the page reference to the Order page.
            oRedirectionPage = new PageReference('/'+oNewOrder.Id);
        }catch(Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.GetMessage()));
            oRedirectionPage = null;
        }
        return oRedirectionPage;
    }
    
    /*********************************************************************************
    Method Name    : Cancel
    Description    : Redirect back to Quote Page
    Return Type    : PageReference 
    Parameter      : None
    *********************************************************************************/
    Public PageReference Cancel() {
        PageReference oQuotePage = new PageReference('/'+sQuoteId);
        oQuotePage.setRedirect(true);
        return oQuotePage;
    }
}