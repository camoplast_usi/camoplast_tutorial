/*********************************************************************************
Class Name      : CS_Trigger_Order
Description     : This class will be invoked by the triggers on Order 
Created By      : Jayadev Rath
Created Date    : 14-Jul-2014
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Jayadev Rath             14-Jul-2014              Initial Version
Maryam Abbas             24-Jul-2014              Added methods to update pricebook and terms and conditions
Divya                    24-07-2014               Show_on_Print__c Carry forward
Jayadev Rath             01-Aug-2014              Added a flag to skip Terms & Condtions records copying, when Order is created from Quote.
Jayadev Rath             20-Aug-2014              Added a method to create JSON structure (which will be later sent to Informatica)
Maryam                   20-Aug-2014              Added updateBilltoShipto method
Divya A N                10-Sep-2014              Updated Logic to take Ship To Account on Orders from the Related Accounts Junction Object
*********************************************************************************/

public class CS_Trigger_Order{

    /*********************************************************************************
    Method Name    : updateOrderFields
    Description    : Method to set order field values as per business logic
                        a. Set the location either from Ship To Account or Sold To Account or as 'ZSNA'
                        b. Set the 'Tax' field to 0 if Tax exemption is checked at Ship to Account level
    Return Type    : void 
    Parameter      : 1. lstOrders: List of order records to upate
    Author         : Maryam
    *********************************************************************************/
    public static void updateOrderFields(List<Order> lstOrders){
        
        // Collect all Account information:
        Set<Id> setAccountIds = new Set<Id>();
        Set<Id> setOrdIds = new Set<Id>();
        for(Order oOrder: lstOrders){
            setAccountIds.add(oOrder.AccountId);
            setOrdIds.add(oOrder.Id);           
            if(oOrder.Related_Account_Ship_To__c !=null) setAccountIds.add(oOrder.Related_Account_Ship_To__c);
        }

        // Get the account details from database
        Map<Id,Account> mapAccountDetails = new Map<Id,Account>([SELECT Id, Locations__c ,Tax_Exemption__c FROM Account where Id in : setAccountIds]);
        Map<Id,Order> mapOrd = new Map<Id,Order>([Select Id, Ship_To_Account_RelatedAccount__r.Ship_To__c,Ship_To_Account_RelatedAccount__c,Related_Account_Ship_To__c, AccountId FROM Order where Id=:setOrdIds]);                    
        String recordType;
        list<Location__C>LOC=[Select Id from Location__c where Location_code__c='ZSNA' limit 1];
        for(Order oOrder: lstOrders){
          // Update the Location field: Consider ZSNA implying there is no CSC available (no default value specified at the Sold To & Ship To level).
          // oOrder.Location__c = 'ZSNA';
          if((mapOrd != null && mapOrd.IsEmpty() == false) && ((mapOrd.get(oOrder.Id).Ship_To_Account_RelatedAccount__c != oOrder.Ship_To_Account_RelatedAccount__c)||(mapOrd.get(oOrder.Id).AccountId != oOrder.AccountId))||(mapOrd == null || mapOrd.IsEmpty() == true)){            
                // If any Ship To Account information is present, it should be prioritized
                   Map<ID, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Order.getRecordTypeInfosById();
                   if(rtMap.get(oOrder.RecordTypeId) !=null){
                           recordType = rtMap.get(oOrder.RecordTypeId).getName();
                   } 
                   if(recordType == 'North America'){
                        // If any Ship To Account information is present, it should be prioritized
                        if(oOrder.Ship_To_Account_RelatedAccount__c != null && mapAccountDetails.get(oOrder.Related_Account_Ship_To__c).Locations__c != null){
                            oOrder.Location__c = mapAccountDetails.get(oOrder.Related_Account_Ship_To__c).Locations__c;
                            oOrder.Selling_Location__c = mapAccountDetails.get(oOrder.Related_Account_Ship_To__c).Locations__c;
                        }
                        //  If any Location is specified at Sold To Account level consider that
                        else if(mapAccountDetails.get(oOrder.AccountId).Locations__c != null){
                             oOrder.Location__c = mapAccountDetails.get(oOrder.AccountId).Locations__c;
                             oOrder.Selling_Location__c = mapAccountDetails.get(oOrder.AccountId).Locations__c;
                        }
                        else{
                               if(LOC!=null && LOC.isEmpty()==false){                              
                                   ID LOCID=LOC.get(0).Id;
                                   System.Debug('Location Id'+LOCID);
                                   oOrder.Location__c = LOC.get(0).Id;
                                   oOrder.Selling_Location__c = LOC.get(0).Id;
                               }
                        
                        }
                    }
                    else{
                        if(mapAccountDetails.get(oOrder.AccountId).Locations__c != null){
                             oOrder.Location__c = mapAccountDetails.get(oOrder.AccountId).Locations__c;
                             oOrder.Selling_Location__c = mapAccountDetails.get(oOrder.AccountId).Locations__c;
                        }                        
                        else{
                            oOrder.Location__c = null;
                            oOrder.Selling_Location__c = null;
                        }
                    
                    
                    }
            }
                
            // Update the Location field:  Set this to Zero if Tax exemption is checked at Ship to Account level
            if(oOrder.Related_Account_Ship_To__c != null  && mapAccountDetails.get(oOrder.Related_Account_Ship_To__c).Tax_Exemption__c == true)
                oOrder.Tax__c = '0';
        }
    }
    
    /*********************************************************************************
    Method Name : updateOrderPriceBook, updateTermsandConditions
    Description : To update the PriceBookId field and terms and conditions everytime an Order is created
    Return Type : void
    Parameter : lstOrder
    Author: Maryam Abbas
    Date : 15/7/2014
    *********************************************************************************/
    public static void updateOrderPriceBook(List<Order> lstOrder)
    { 
            Set<Id> setAccountIds = new Set<Id>();
            for(Order oOrder: lstOrder){
                setAccountIds.add(oOrder.AccountId);
            }
            List<Pricebook2> CS_DefaultPrice = [Select Id FROM Pricebook2 WHERE Pricebook2.IsStandard = true];
                 
            Map<Id,Account> mapAccountDetails = new Map<Id,Account>([SELECT Assigned_Price_Book__c FROM Account where Id in : setAccountIds]);
            for (Order CS_Order: lstOrder){
               
                if(mapAccountDetails.get(CS_Order.AccountId).Assigned_Price_Book__c != null){ 
                    CS_Order.PriceBook2Id = mapAccountDetails.get(CS_Order.AccountId).Assigned_Price_Book__c;
                } 
                else{                   
                    CS_Order.PriceBook2Id = CS_DefaultPrice.get(0).Id; 
                }
                
                 Map<ID, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Order.getRecordTypeInfosById();
                   if(rtMap.get(CS_Order.RecordTypeId) !=null)
                   {
                        String recordType = rtMap.get(CS_Order.RecordTypeId).getName();
                       if(recordType=='North America')
                       {
                        CS_Order.PriceBookId__c=CS_Order.PriceBook2Id;
                        }
                    }    
            }
        /*for (Order CS_Order : lstOrder){
            Account CS_Pricebook = [Select Assigned_Price_Book__c FROM Account WHERE Account.Id = :CS_Order.AccountId];
            if(CS_Pricebook.Assigned_Price_Book__c != null){ 
                CS_Order.PriceBookId__c = CS_Pricebook.Assigned_Price_Book__c;
            } 
            else{
                List<Pricebook2> CS_DefaultPrice = [Select Id FROM Pricebook2 WHERE Pricebook2.IsStandard = true];
                CS_Order.PriceBookId__c = CS_DefaultPrice.get(0).Id; 
            }
        }*/
    } 
    public static void updateTermsandConditions(List<Order> lstOrder) {
        //If the Order is directly created from Account then only copy the Term&Condtion records. (No need when created from Quote. TnC records will be copied from Quote)
        If(!CS_CONSTANTS.bOrderCreatedFromQuote) {
        
            /* List<Terms_and_Conditions__c> CS_Order_Terms_List= new List<Terms_and_Conditions__c>();
            for (Order CS_Order: lstOrder){
                List<Terms_and_Conditions__c> CS_Terms_List;
                if (CS_Order.Ship_To_Account_RelatedAccount__r == null)
                    CS_Terms_List= [Select Terms_and_Conditions__c, OrderId__c,Show_on_Print__c FROM Terms_and_Conditions__c WHERE AccountId__c = :CS_Order.AccountId];
                else 
                    CS_Terms_List= [Select Terms_and_Conditions__c, OrderId__c,Show_on_Print__c FROM Terms_and_Conditions__c WHERE (AccountId__c = :CS_Order.AccountId OR AccountId__c = :CS_Order.Ship_To_Account_RelatedAccount__r.Ship_To__c)];
             
               
                Terms_and_Conditions__c CS_Order_Term = new Terms_and_Conditions__c();
                for (Terms_and_Conditions__c CS_Term: CS_Terms_List){
               
                    CS_Order_Term = new Terms_and_Conditions__c();
                    CS_Order_Term.OrderId__c = CS_Order.Id;
                    CS_Order_Term.Terms_and_Conditions__c=CS_Term.Terms_and_Conditions__c;
                    CS_Order_Term.Show_on_Print__c = CS_Term.Show_on_Print__c;
                    CS_Order_Terms_List.add(CS_Order_Term);
                }    
                
            } 
            insert CS_Order_Terms_List;*/
            Set<Id> setAccountIds = new Set<Id>();
            for(Order oOrder: lstOrder){
                setAccountIds.add(oOrder.AccountId);
            }
                           
            Set<Id> setShippingAccountIds = new Set<Id>();
            for(Order oOrder: lstOrder){
                setShippingAccountIds.add(oOrder.Related_Account_Ship_To__c);
            }
            
            List<Terms_and_Conditions__c> CS_Order_Terms_List= new List<Terms_and_Conditions__c>();
            
            Map<Id, List<Terms_and_Conditions__c>> TnCMap = new Map<Id, List<Terms_and_Conditions__c>>();
            Map<Id, List<Terms_and_Conditions__c>> TnCShippingMap = new Map<Id, List<Terms_and_Conditions__c>>();
    
            List<Account> Accounts = [Select Id, (Select Terms_and_Conditions__c, Show_on_Print__c From Terms_and_Conditions__r) From Account where Id in :setAccountIds];
            List<Account> ShippingAccounts = [Select Id, (Select Terms_and_Conditions__c, Show_on_Print__c From Terms_and_Conditions__r) From Account where Id in :setShippingAccountIds];
          
            // loop through all retrieved Accounts 
            for(Account a : Accounts)
            {
               List<Terms_and_Conditions__c> TnC = a.getSObjects('Terms_and_Conditions__r'); // get all Terms and Conditions for the Account
               TnCMap.put(a.Id, TnC); // put the Account Terms and Conditions in a map by Account Id
               
            }
            for(Account sa : ShippingAccounts)
            {
               List<Terms_and_Conditions__c> TnCShip = sa.getSObjects('Terms_and_Conditions__r'); // get all Terms and Conditions for the Account
               TnCShippingMap.put(sa.Id, TnCShip); // put the Account Terms and Conditions in a map by Account Id
               
            }
            List<Terms_and_Conditions__c> TnClst = new List<Terms_and_Conditions__c>();
            List<Terms_and_Conditions__c> TnCShiplst = new List<Terms_and_Conditions__c>();
            
          
            Terms_and_Conditions__c CS_Order_Term;
            for (Order CS_Order: lstOrder){          
                           
                  // Terms_and_Conditions__c CS_term = TnClst.get(0);
                 if( CS_Order.AccountId !=null)
                 {     TnClst = TnCMap.get(CS_Order.AccountId); 
                       if(TnClst != null)
                       {    for (Terms_and_Conditions__c CS_Term: TnClst){
                                
                                CS_Order_Term = new Terms_and_Conditions__c();
                                CS_Order_Term.OrderId__c = CS_Order.Id;
                                CS_Order_Term.Terms_and_Conditions__c=CS_Term.Terms_and_Conditions__c;
                                CS_Order_Term.Show_on_Print__c = CS_Term.Show_on_Print__c;
                                CS_Order_Terms_List.add(CS_Order_Term);
                            }  
                        } 
                 }
                 system.debug(CS_Order.Ship_To_Account_RelatedAccount__c + '###');
                 if( CS_Order.Ship_To_Account_RelatedAccount__c !=null )
                 {    TnCShiplst = TnCShippingMap.get(CS_Order.Related_Account_Ship_To__c);
                      if(TnCShiplst !=null){           
                          for (Terms_and_Conditions__c CS_Term: TnCShiplst){
                                
                                CS_Order_Term = new Terms_and_Conditions__c();
                                CS_Order_Term.OrderId__c = CS_Order.Id;
                                CS_Order_Term.Terms_and_Conditions__c=CS_Term.Terms_and_Conditions__c;
                                CS_Order_Term.Show_on_Print__c = CS_Term.Show_on_Print__c;
                                CS_Order_Terms_List.add(CS_Order_Term);
                          }
                      }
                 }
            }
            insert CS_Order_Terms_List;
        }        
    }
    
/*********************************************************************************
Method Name : updateOrderBilltoContact
Description : To update the Bill to Contact with the Account's primary Contact with Role as Orders Main Contact and
update Ship to Account based on Ship to Contact.
Return Type : void
Parameter : lstOrder
Author: Laxmy M Krishnan
Date : 7/31/2014
*********************************************************************************/

 public static void updateOrderBilltoContact(List<Order>lstOrder)
{

// Collect all Account information from the Orders.
        Set<Id> setAccountIds = new Set<Id>();
        for(Order oOrder: lstOrder){
            setAccountIds.add(oOrder.AccountId);
                }
                          
  if(setAccountIds!=null)
 { 
 //Get the Account Contact Roles 
 list<AccountContactRole> ContactId = [Select ContactId FROM AccountContactRole WHERE AccountId = :setAccountIds AND Role='Orders Main Contact' AND IsPrimary=True limit 1 ];
 
 
         for (Order Ordnew: lstOrder)
             {

                if (ContactId!=null && ContactId.isEmpty()==false)
                    {
                        ID accId=Ordnew.AccountId;
                        ID BillContact = Ordnew.BillToContactId;
                        ID Contact = ContactId.get(0).ContactId;
                             if (BillContact==null )
                                    {        
                                        Ordnew.BillToContactId=Contact;
                                    }
            
                    }
 
 
             }  

     
        }        
    }
    /*********************************************************************************
    Method Name : updateOrderShiptoAcc to Account
    Description : To Ship to Account based on Ship to Contact.
    Return Type : void
    Parameter : lstOrder
    Author: Laxmy M Krishnan
    Date : 7/31/2014
    *********************************************************************************/
    public static void updateOrderShiptoAcc(List<Order>lstOrder)
    {
    
    // Collect all Contactinformation from the Orders.
            Set<Id> setContactIds = new Set<Id>();
            for(Order oOrder: lstOrder){
                setContactIds .add(oOrder.ShipToContactId);
                    }
    
     if(setContactIds!=null)
     {
     Map<Id,Contact> mapContact = new Map<Id,Contact>([Select Id, AccountId FROM Contact where Id=:setContactIds]);
     
    
        for (Order Ordnew: lstOrder)
             {
    
                 if(mapContact!=null && mapContact.isEmpty()==false )
                   {
           System.debug('mapContact'+mapContact);
                    ID ShiptoAccount = Ordnew.Related_Account_Ship_To__c;
                    ID ShiptoContact = Ordnew.ShipToContactId;
                    ID AccountId = mapContact.get(ShiptoContact).AccountId;
                        if(ShiptoAccount ==null && ShiptoContact!=null)
                            {
            
                                Ordnew.Ship_To_Account_RelatedAccount__r.Ship_To__c =AccountId; 
            
           
                                }      
           
                    }
            }
    
            
        }
    }
    
    
    /*********************************************************************************
    Method Name    : SyncJSONDetails
    Description    : Method to create a JSON formatted structure for recently created or updated JSON files, which would be sent to Informatica for further processing
    Return Type    : void 
    Parameter      : 1. lstOrders: List of order records to process the JSON
    *********************************************************************************/
    /*  Mapping of Fields (* = Hardcoded, need correct mapping)
        SALESFORCE                    ->    EPICOR
        Customer_Id__c                ->    cust_code
        Location__c                   ->    location
        Multiple_Ship_To__c           ->    multiple_flag
        Scheduled_Ship_Date__c        ->    req_ship_date
        *Tax__c                       ->    tax_id
        * ??                          ->    tax_perc
        TotalAmount                   ->    total_amt_order
        *Invoice_Total__c             ->    total_invoice
        * ??                          ->    user_code
        
        
        OrderItem Details             ->    orderdetail
            *Quantity                 ->    ordered
    */
    public static void SyncJSONDetails(List<Order> lstOrders){
        // Query for Order details & Order Items after Insert/Update of an Order
        List<Order> lstOrdersWithItems = [SELECT Id, Customer_Id__c, Location__c, Multiple_Ship_To__c, Scheduled_Ship_Date__c,
                                TotalAmount, Invoice_Total__c,
                                (SELECT Id,Quantity FROM OrderItems)
                                FROM Order WHERE Id IN :lstOrders ];
        
        List<Map<String,Object>> lstAllOrderDetails = new List<Map<String,Object>>();
        
        for(Order oOrder: lstOrdersWithItems){
            Map<String,Object> mapOrderDetails = new Map<String,Object>();
            mapOrderDetails.put('cust_code',         oOrder.Customer_Id__c);
            mapOrderDetails.put('location',          oOrder.Location__c);
            mapOrderDetails.put('req_ship_date',     oOrder.Scheduled_Ship_Date__c);
            mapOrderDetails.put('total_amt_order',   oOrder.TotalAmount);
            mapOrderDetails.put('total_invoice',     oOrder.Invoice_Total__c);
            mapOrderDetails.put('multiple_flag',     (oOrder.Multiple_Ship_To__c == true ? 'Y' : 'N'));
            mapOrderDetails.put('tax_perc',          0.00);
            mapOrderDetails.put('tax_id',            'EXEMPT');
            mapOrderDetails.put('user_code',         '0');
            
            // Get the Order Item details:
            List<Map<String,Object>> lstAllOrderItems = new List<Map<String,Object>>();
            for(OrderItem oOrderItem : oOrder.OrderItems){
                Map<String,Object> mapOrderItemDetails = new Map<String,Object>();
                mapOrderItemDetails.put('ordered',oOrderItem.Quantity);            
                lstAllOrderItems.add(mapOrderItemDetails);
            }
            
            mapOrderDetails.put('orderdetail',lstAllOrderItems);
            lstAllOrderDetails.add(mapOrderDetails);
        }
        
        // Create the JSON file from the Map:
        String sOrderJSON = JSON.SerializePretty(lstAllOrderDetails);
        System.Debug('@@@@ sOrderJSON: '+ sOrderJSON);
    }
/*********************************************************************************
Method Name : updateOrderFieldsAcc
Description : To update fields in Order based on Sold To Account's fields.
Return Type : void
Parameter : lstOrder
Author: Laxmy M Krishnan
Date : 8/21/2014
*********************************************************************************/
    public static void updateOrderFieldsAcc(List<Order>lstOrder)
    {
    // Collect all Account information from the Orders.
        Set<Id> setAccountIds = new Set<Id>();
        for(Order oOrder: lstOrder){
                                    setAccountIds.add(oOrder.AccountId);
                                    }
                                    
         
         if(setAccountIds!=null)
             {
             Map<Id,Account> mapAccountDetails = new Map<Id,Account>([SELECT Locations__c,Price_List__c,Price_Group__c,Incoterms__c,Incoterms_Location__c,Terms__c,Complete_Delivery__c,Payment_Method__c,CurrencyIsoCode FROM Account where Id in : setAccountIds]);
              for (Order Ordnew: lstOrder)
             {
              if(mapAccountDetails !=null && mapAccountDetails .isEmpty()==false )
                   {
                   /*Map<ID, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Order.getRecordTypeInfosById();
                   if(rtMap.get(Ordnew.RecordTypeId) !=null)
                   {
                       String recordType = rtMap.get(Ordnew.RecordTypeId).getName();
                       if(recordType=='EMEA Sales Order' || recordType=='EMEA Service Order')
         {*/
                   
                   ID SoldToAcc=Ordnew.AccountId;
                   System.Debug('SoldToAcc'+SoldToAcc);
                 //  Ordnew.Location__c=mapAccountDetails.get(SoldToAcc).Locations__c;
                   Ordnew.Price_List__c=mapAccountDetails.get(SoldToAcc).Price_List__c; 
                   Ordnew.Price_Group__c=mapAccountDetails.get(SoldToAcc).Price_Group__c;
                   Ordnew.Incoterms__c=mapAccountDetails.get(SoldToAcc).Incoterms__c;
                   Ordnew.Incoterms_Location__c=mapAccountDetails.get(SoldToAcc).Incoterms_Location__c;
                   Ordnew.Payment_Terms__c=mapAccountDetails.get(SoldToAcc).Terms__c;
                   //Ordnew.Complete_Delivery__c=mapAccountDetails.get(SoldToAcc).Complete_Delivery__c;
                   Ordnew.Payment_Method__c=mapAccountDetails.get(SoldToAcc).Payment_Method__c;
                   Ordnew.CurrencyIsoCode=mapAccountDetails.get(SoldToAcc).CurrencyIsoCode;
                   //}
                   //}
                   }
             }
             }
             
    }
/*********************************************************************************
Method Name : createOrderfromQuote
Description : To create Order and Order Line Items from Quote and Quote Line Items respectively.
Return Type : void
Parameter : lstOrder
Author: Laxmy M Krishnan
Date : 8/25/2014
*********************************************************************************/
public static void createOrderfromQuote(List<Order> lstOrder)
    {
    
    // Collect Quote Id from Order
        Set<Id> setQuoteIds = new Set<Id>();
        for(Order oOrder: lstOrder){
            setQuoteIds.add(oOrder.QuoteId);
                                    }
        System.debug('setQuoteIds  '+setQuoteIds);
        
   if(setQuoteIds!=null)
      {
      System.Debug('Inside QuoteId loop');
        Map<Id,Quote> mapQuoteDetails = new Map<Id,Quote>([SELECT RecordTypeId,Quote_Type__c,Sales_Organization__c,Distribution_Channel__c,Division__c,Account.Id,ShippingAddress,Requested_Delivery_Date__c,Warehouse__c,CurrencyIsoCode,Price_List__c,Price_Group__c,Order_Reason__c,Incoterms__c,Incoterms_Location__c,Payment_Terms__c,Billing_Block__c,Payment_Method__c,Date_Job_Sheet_Processed__c,Time_On_Site__c,VOR_Machine__c,Document_Date__c,Job_Date_Received__c,Service_Technician_Name__c,Customer_Job_Number__c,Customer_Engineering_Team__c,Customer_Engineering_Region__c,Internal_Job_number__c,Third_Party_Job_Number__c,Truck_Make__c,Truck_Model__c,Truck_Fleet__c,POD_Name__c,Hour_Reading__c,Ship_To__c FROM Quote where Id in : setQuoteIds]);
        
        set<Id> shipToId = new set<Id>();
        set<Id> soldToId = new set<Id>();
        for(Quote oquote : mapQuoteDetails.values()){
            shipToId.add(oquote.Ship_To__c);
            soldToId.add(oquote.Account.Id);
        }
        
        
     list<Acc_Junction__c> lstAccJunction = [SELECT Id,Ship_To__c,Sold_To__c FROM Acc_Junction__c WHERE Ship_To__c in : shipToId  AND Sold_To__c  in : soldToId];
     map<string,Id> mapAccJunction = new map<string,Id>();
     for(Acc_Junction__c relatedAccount : lstAccJunction){
        mapAccJunction.put(string.valueOf(relatedAccount.Ship_To__c) + '-' + string.valueOf(relatedAccount.Sold_To__c), relatedAccount.Id);
        }
        
  
        for (Order Ordnew: lstOrder)
             {
              if(mapQuoteDetails !=null && mapQuoteDetails .isEmpty()==false )
                   {
                   System.Debug('Inside QuoteMap loop');
                   Map<ID, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Order.getRecordTypeInfosById();
                       if(rtMap.get(Ordnew.RecordTypeId) !=null)
                       {
                           String recordType = rtMap.get(Ordnew.RecordTypeId).getName();
                           System.debug('recordType' +recordType);
                           if(recordType=='EMEA Sales Order' || recordType=='EMEA Service Order' )
                           {
                           System.Debug('Inside Record loop');
                             ID quoteId=Ordnew.QuoteId;
                             System.debug('quoteId '+quoteId);
                             //Ordnew.Type=mapQuoteDetails.get(quoteId).Quote_Type__c;
                             Ordnew.Sales_Organization__c=mapQuoteDetails.get(quoteId).Sales_Organization__c;
                             Ordnew.Distribution_Channel__c=mapQuoteDetails.get(quoteId).Distribution_Channel__c;
                             Ordnew.Division__c=mapQuoteDetails.get(quoteId).Division__c;                            
                             Ordnew.Requested_Delivery_Date__c=mapQuoteDetails.get(quoteId).Requested_Delivery_Date__c;
                             Ordnew.Warehouse__c=mapQuoteDetails.get(quoteId).Warehouse__c;
                             Ordnew.CurrencyIsoCode=mapQuoteDetails.get(quoteId).CurrencyIsoCode;
                             Ordnew.Price_List__c=mapQuoteDetails.get(quoteId).Price_List__c;
                             Ordnew.Price_Group__c=mapQuoteDetails.get(quoteId).Price_Group__c;
                             Ordnew.Order_Reason__c=mapQuoteDetails.get(quoteId).Order_Reason__c;
                             Ordnew.Incoterms__c=mapQuoteDetails.get(quoteId).Incoterms__c;
                             Ordnew.Incoterms_Location__c=mapQuoteDetails.get(quoteId).Incoterms_Location__c;
                             Ordnew.Billing_Block__c=mapQuoteDetails.get(quoteId).Billing_Block__c;
                             Ordnew.Billing_Date__c=System.Today();
                             Ordnew.Payment_Method__c=mapQuoteDetails.get(quoteId).Payment_Method__c;
                             Ordnew.Date_Job_Sheet_Processed__c=mapQuoteDetails.get(quoteId).Date_Job_Sheet_Processed__c;
                             Ordnew.Time_On_Site__c=mapQuoteDetails.get(quoteId).Time_On_Site__c;
                             Ordnew.VOR_Machine__c=mapQuoteDetails.get(quoteId).VOR_Machine__c;
                            //Ordnew.CurrencyIsoCode=mapQuoteDetails.get(quoteId).CurrencyIsoCode;
                             Ordnew.Distribution_Channel__c='11';
                             Ordnew.Document_Date__c=System.Today();
                             //Ordnew.Sales_Organization__c='3100';
                             //Ordnew.Ship_To_Account__c='0011100000URAyG';
                             Ordnew.Ship_To_Account_RelatedAccount__c = mapAccJunction.get(string.valueOf(mapQuoteDetails.get(quoteId).Ship_To__c) + '-' +string.valueof(mapQuoteDetails.get(quoteId).Account.Id));
                                
                             //Divya 10-09 Commented as part of US-47226
                            //Ordnew.Ship_To_Account_RelatedAccount__r.Ship_To__c=mapQuoteDetails.get(quoteId).Ship_To__c;
                             //Ordnew.Incoterms__c='DAP';
                             System.debug('Currency '+mapQuoteDetails.get(quoteId).CurrencyIsoCode);
                             Ordnew.Date_Job_Date_Received__c=mapQuoteDetails.get(quoteId).Job_Date_Received__c;
                             Ordnew.Service_Technician_Name__c=mapQuoteDetails.get(quoteId).Service_Technician_Name__c;
                             Ordnew.Customer_Job_Number__c=mapQuoteDetails.get(quoteId).Customer_Job_Number__c;
                             Ordnew.Customer_Engineering_Team__c=mapQuoteDetails.get(quoteId).Customer_Engineering_Team__c;
                             Ordnew.Customer_Engineering_Region__c=mapQuoteDetails.get(quoteId).Customer_Engineering_Region__c;
                             Ordnew.Internal_Job_number__c=mapQuoteDetails.get(quoteId).Internal_Job_number__c;
                             Ordnew.Third_Party_Job_Number__c=mapQuoteDetails.get(quoteId).Third_Party_Job_Number__c;
                             Ordnew.Truck_Make__c=mapQuoteDetails.get(quoteId).Truck_Make__c;
                             Ordnew.Truck_Model__c=mapQuoteDetails.get(quoteId).Truck_Model__c;
                             Ordnew.POD_Name__c=mapQuoteDetails.get(quoteId).POD_Name__c;
                             Ordnew.Hour_Reading__c=mapQuoteDetails.get(quoteId).Hour_Reading__c;
                             
                           }
                           
                         }
                     }
                }
 
    }
    }    
    
    /*********************************************************************************
    Method Name    : updateBillToShipToFields
    Description    :
    Return Type    : void 
    Parameter      : 1. lstOrder: List of order records to upate
    *********************************************************************************/
    public static void updateBillToShipToFields(List<Order> lstOrder){
            
            Set<Id> setOrderIds = new Set<Id>();
            Set<Id> setBillToAccountIds = new Set<Id>();
            Set<Id> setShipToAccountIds = new Set<Id>();
            Set<Id> setBillToContactIds = new Set<Id>();
            Set<Id> setShipToContactIds = new Set<Id>();
            
            for(Order oOrder: lstOrder){             
                
                setOrderIds.add(oOrder.Id);
                setBillToAccountIds.add(oOrder.AccountId);
                setShipToAccountIds.add(oOrder.Related_Account_Ship_To__c);
                system.debug(oOrder.Related_Account_Ship_To__c + '****');
                setBillToContactIds.add(oOrder.BillToContactId);
                setShipToContactIds.add(oOrder.ShipToContactId);
            }
        
     list<Acc_Junction__c> lstAccJunction = [SELECT Id,Ship_To__c,Sold_To__c FROM Acc_Junction__c WHERE Ship_To__c in : setShipToAccountIds  OR Sold_To__c  in : setBillToAccountIds];
     map<string,Id> mapAccJunction = new map<string,Id>();
     for(Acc_Junction__c relatedAccount : lstAccJunction){
        mapAccJunction.put(string.valueOf(relatedAccount.Ship_To__c) + '-' + string.valueOf(relatedAccount.Sold_To__c), relatedAccount.Id);
        }
           
             //Get the Account Contact Roles 
             list<AccountContactRole> BillContactId = [Select ContactId FROM AccountContactRole WHERE AccountId = :setBillToAccountIds AND Contact.AccountId in : setBillToAccountIds  AND Role='Orders Main Contact' /*AND IsPrimary=True*/ limit 1 ];
           
             list<AccountContactRole> ShipContactId = [Select ContactId FROM AccountContactRole WHERE AccountId = :setShipToAccountIds AND Contact.AccountId in : setShipToAccountIds AND (Role='Service' OR Role='Orders Main Contact') /*AND IsPrimary=True*/ limit 1 ];
                  
             Map<Id,Order> mapOrder = new Map<Id,Order>([Select Id, Ship_To_Account_RelatedAccount__c,Related_Account_Ship_To__c, ShipToContactId, AccountId, BillToContactId FROM Order where Id in :setOrderIds]);
             Map<Id,Contact> mapShipToContact = new Map<Id,Contact>([Select Id, AccountId FROM Contact where Id in :setShipToContactIds]); 
             Map<Id,Contact> mapBillToContact = new Map<Id,Contact>([Select Id, AccountId FROM Contact where Id in :setBillToContactIds]);  
             
             if(mapOrder != null && mapOrder.IsEmpty() == false)                              
             {
                 for (Order Ordnew: lstOrder)
                 {   
                      if(mapOrder.get(Ordnew.Id).Ship_To_Account_RelatedAccount__c != Ordnew.Ship_To_Account_RelatedAccount__c)
                        if(ShipContactId!=null&&ShipContactId.IsEmpty()==false)
                         Ordnew.ShipToContactId=ShipContactId.get(0).ContactId;
                        else 
                         Ordnew.ShiptoContactId=null;
                     else if(mapOrder.get(Ordnew.Id).ShipToContactId != Ordnew.ShipToContactId)
                        if(Ordnew.ShipToContactId!=null&& mapShipToContact!=null && mapShipToContact.IsEmpty()==false)
                          Ordnew.Ship_To_Account_RelatedAccount__c = mapAccJunction.get(string.valueOf(mapShipToContact.get(Ordnew.ShipToContactId).AccountId) + '-' +string.valueof(Ordnew.AccountId));
                          //Ordnew.Ship_To_Account_RelatedAccount__r.Ship_To__c =mapShipToContact.get(Ordnew.ShipToContactId).AccountId;
                        else
                         Ordnew.Ship_To_Account_RelatedAccount__c = null;                     
                     if(mapOrder.get(Ordnew.Id).AccountId != Ordnew.AccountId)
                       if (BillContactId!=null&&BillContactId.IsEmpty()==false)
                         Ordnew.BillToContactId=BillContactId.get(0).ContactId;
                       else 
                         Ordnew.BillToContactId=null;
                     //else if(mapOrder.get(Ordnew.Id).BillToContactId != Ordnew.BillToContactId)
                     //     Ordnew.AccountId=mapBillToContact.get(Ordnew.BillToContactId).AccountId; 
                  }
              }
              else{
                 for (Order Ordnew: lstOrder)
                 {   if(Ordnew.Ship_To_Account_RelatedAccount__c!=null&&ShipContactId!=null&&ShipContactId.IsEmpty()==false)
                         Ordnew.ShipToContactId=ShipContactId.get(0).ContactId;
                     else if(Ordnew.ShiptoContactId!=null && mapShipToContact!=null && mapShipToContact.IsEmpty()==false) {
                          Ordnew.Ship_To_Account_RelatedAccount__c = mapAccJunction.get(string.valueOf(mapShipToContact.get(Ordnew.ShipToContactId).AccountId) + '-' +string.valueof(Ordnew.AccountId));
                         //Ordnew.Ship_To_Account_RelatedAccount__r.Ship_To__c =mapShipToContact.get(Ordnew.ShipToContactId).AccountId;
                     }
                     if(Ordnew.AccountId != null&&BillContactId!=null&&BillContactId.IsEmpty()==false)
                         Ordnew.BillToContactId=BillContactId.get(0).ContactId;
                   // else if(Ordnew.BilltoContactId!=null)
                   //    Ordnew.AccountId=mapBillToContact.get(Ordnew.BillToContactId).AccountId; 
                    
                  }
              
              
              }
    }


}