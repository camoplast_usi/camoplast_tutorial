/*************************************************************************************
Trigger Name : CS_QuoteTrigger
Description : Trigger for ‘Quote' object. 
Created By : Maryam Abbas
Created Date : 18-Jul-14
Modification Log:
---------------------------------------------------------------------------------- 
Developer Date Description
---------------------------------------------------------------------------------- 
Maryam 14-Jul-14 Initial Version
Maryam  8-Aug-14 Call updateAccountInfo method
************************************************************************************/
trigger CS_QuoteTrigger on Quote(before insert, after insert) {

    if(Trigger.isAfter && Trigger.isInsert){
    CS_Trigger_Quote.updateTermsandConditions(trigger.new);
    
    
    }
    if(Trigger.isBefore && Trigger.isInsert){
        CS_Trigger_Quote.updateOptyInfo(trigger.new);
    }
    /* if(Trigger.isAfter && Trigger.isInsert){
        CS_Trigger_Quote.updateQuoteProductInfo(trigger.new);
    }*/
    
}