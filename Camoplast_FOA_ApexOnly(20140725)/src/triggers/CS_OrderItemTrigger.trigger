/*************************************************************************************
Trigger Name : CS_OrderItemTrigger
Description : Trigger for ‘OrderItem' object. 
Created By : Laxmy M Krishnan
Created Date : 8-27-2014
Modification Log:
---------------------------------------------------------------------------------- 
Developer Date Description
---------------------------------------------------------------------------------- 
Maryam  27-Aug-14 Initial Version
************************************************************************************/
trigger CS_OrderItemTrigger on OrderItem(before insert, after insert) {

     if(Trigger.isbefore && Trigger.isInsert){
          CS_Update_OrderLineItem.updateOrderItemNumber(trigger.new);
          for (OrderItem e : trigger.new)
          {
            e.Quantity__c = e.Quantity;
          }
        //CS_Update_OrderLineItem.copyQuoteLineItemInfo(trigger.new);
        
        }
    if(Trigger.isAfter && Trigger.isUpdate){
        CS_Update_OrderLineItem.copyQuoteLineItemInfo(trigger.new);
    }
   
    
}