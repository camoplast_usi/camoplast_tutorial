<!--**********************************************************************************
VF Page Name    : CS_EmailQuote
Description     : Supports the Publisher action/Salesforce button. Emails the latest Quote from Opportunity when clicked using Conga.
Created By      : Jayadev Rath
Created Date    : 17-July-2014
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Jayadev Rath             17-July-2014              Initial Version
Jayadev Rath             23-July-2014              Updated the redirection logic from Web pages & prevented automatic emailing of quote
**********************************************************************************-->
<apex:page standardController="Opportunity" extensions="CS_Extension_EmailQuote" id="QuoteEmailPage">
    <apex:form id="frm">
        <apex:actionFunction action="{!EmailQuote}" name="emailQuote" reRender="messages"/>
        <apex:actionFunction action="{!returnToOpportunity}" name="returnToOpportunity"/>
        
        <script type="text/javascript">
            /* Redirection logic: If from Salesforce1, navigate using sforce.one else navigate using normal VF action method */
            function redirectToOpportunity() {
                if(typeof sforce != 'undefined' && typeof sforce.one != 'undefined' && sforce.one != null)
                    sforce.one.navigateToSObject('{!Opportunity.Id}','detail');    /* If invoked from Salesforce1, navigate back to Opportunity record */
                else
                    returnToOpportunity();    /* If invoked from Salesforce Web page (by CSRs) */
            }
            /* Check if opportunity requires any approval */
            window.onload=function(){
                if('{!sErrorFound}' != 'Yes'){
                    emailQuote();
                    redirectToOpportunity();
                }
            };
        </script>
        <!-- Button & page message displayed if any error found in Opportunity level or quote creation/pdf creation level -->
        <apex:pagemessages id="messages"/>
        <apex:commandButton value="Go Back to Opportunity" onclick="redirectToOpportunity(); return false;"/>
    </apex:form>
</apex:page>