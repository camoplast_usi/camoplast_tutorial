/*********************************************************************************
Class Name      : CS_REST_TestingController 
Description     : Parent class for REST call.  Taken from https://gist.githubusercontent.com/noeticpenguin/7083556/raw/c2cc33ae82739ea662af43676f18f4284eecf792/RestClient.java
Created By      : Pierre Dufour
Created Date    : 2014-08-29
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
Pierre                      2014-08-29             Initial Version
Jayadev Rath                2014-09-01             Added debug statements to check the outgoing request.
----------------------------------------------------------------------------------
**********************************************************************************/ 
Public with sharing virtual class DTT_RestClient {
    Public class RestClientException extends Exception {}

    /*
     * class variable creation - DO NOT EDIT
     */

    Public Map<String,String> headers;
    Public String url;
    Public String method;
    Public String body;
    Public HttpRequest request;
    Public HttpResponse response;
    Public String responseBody;
    Public String lastDebugMessage;
    //MA added error message to be displayed for inventory
    Public String errormessage;
    Http h;
    
    /*
     * Constants - Edit to your satisfaction
     */

    Public Static Integer TIMEOUT = 60000; // Default HTTP Timeout
    Public Boolean DEBUG = true; // should we print debug statements?

    /*
     * Constructors:
     */

    Public DTT_RestClient() {} //Prevent empty objects.

    //master constructor with everything required. 
    Public DTT_RestClient(String url, String method, Map<String,String> headers, String body) {
        try {
            this.h = new Http();
            this.request = buildRequest(headers, url, method.toUpperCase(), body);
            log('Outbound Request', request,  request.getBody());
            this.response = makeRequest(this.h, this.request);
            this.responseBody = handleResponse(this.response);
            this.errormessage = 'Success';
        } catch(Exception e) {
            //log the error, but set as much as we can with the input given
            log('Failed to execute callout. SFDC Reports: ',e, e.getMessage());
            if(headers != null) this.headers = headers;
            this.body = body;
            this.url = url;
            this.method = method.toUpperCase();
            this.errormessage = e.getMessage();
        }
    }

     //master constructor with everything required. 
    Public void DTT_Post(String url, String method, Map<String,String> headers, String body) {
        try {
            this.h = new Http();
            this.request = buildRequest(headers, url, method.toUpperCase(), body);
            log('Outbound Request', request,  request.getBody());
            this.response = makeRequest(this.h, this.request);
            this.responseBody = handleResponse(this.response);
            this.errormessage = 'Success';
        } catch(Exception e) {
            //log the error, but set as much as we can with the input given
            log('Failed to execute callout. SFDC Reports: ',e, e.getMessage());
            if(headers != null) this.headers = headers;
            this.body = body;
            this.url = url;
            this.method = method.toUpperCase();
            this.errormessage = e.getMessage();
        }
    }
   
    
    Public DTT_RestClient(String url, String method, String body) {
        this(url, method, null, body);
    }
    
    Public DTT_RestClient(String url, String method, Map<String,String> headers) {
        this(url, method, headers, null);
    }
    
    Public DTT_RestClient(String url, String method) {
        this(url, method, new Map<String,String>(), null);
    }

    /*
     * Helper Methods - These do the actual work.
     */

  Public Static HttpRequest buildRequest(map<String,String> headers, String url, String method, String body){
    HttpRequest request = new HttpRequest();
    request.setTimeout(TIMEOUT); // timeout in milliseconds 
    if (headers != null) {
            for(String hkey : headers.keySet()){
            request.setHeader(hkey, headers.get(hkey)); 
        }   
    }
      request.setEndpoint(url);
      request.setMethod(method);
      if (body != null && body.length() > 0) {
        request.setBody(body);
      }
      return request;
  }

    Public Static HttpResponse makeRequest(Http h, HttpRequest request){


        HttpResponse response = h.send(request);
        
        if (response.getStatusCode() > 299) {
            throw new RestClientException('Failed to recieve a success code from remote. Code was: ' + response.getStatusCode() + ' request was ' + request + ' Response Body is: ' + response.getBody());
        }
        return response;    
    }  

    Public string handleResponse(HttpResponse response){
        log('Response', response, response.getBody());
        return response.getBody();
    }
    
    
    /**
    * GET Convenance Methods.
    **/
    // DTT_RestClient.get('http://www.google.com/?q=convenance')
    Public String get(String url) {
        DTT_RestClient x = new DTT_RestClient(url, 'get', null, null);
        log(url, x, null);
        return x.responseBody;
    }
    
    // DTT_RestClient.get('http://www.google.com/', HeaderMap{'q', 'convenance'})
    Public String get(String url, map<String,String> headers) {
        DTT_RestClient x = new DTT_RestClient(url, 'get', headers, null);
        system.debug(x.responseBody);
        return x.responseBody;
    }
    
    // DTT_RestClient.get('http://www.google.com/', HeaderMap{'q', 'convenance'}, 'some body of text for unknown reason.')
    Public String get(String url, map<String,String> headers, String body) {
        DTT_RestClient x = new DTT_RestClient(url, 'get', headers, body);
        return x.responseBody;
    }
    
    Public HttpResponse post(String url, Map<String,String> headers, String Body) {
        DTT_RestClient x = new DTT_RestClient(url, 'POST', headers, body);
        return x.response;
    }

    Public String request(String url, String method, Map<String,String> headers){
        DTT_RestClient x = new DTT_RestClient(url, method, headers);
        return x.responseBody;
    }

    Public String request(String url, String method, String body){
        DTT_RestClient x = new DTT_RestClient(url, method, body);
        return x.responseBody;
    }

    Public String request(String url, String method){
        DTT_RestClient x = new DTT_RestClient(url, method);
        return x.responseBody;
    }

    /*
     * Private helper methods: Only this class should use them.
     */
    
    public void log(String header, Object obj, String msg) {
        String startStop = '\n===============================================================================';
        String logOutput = startStop;
        logOutput += (header != null) ? '\n== Header: ' + header : 'Header: No Header Set';
        logOutput += (obj != null) ? '\n== Obj string Rep: ' + obj : '\n No Obj set';
        logOutput += (msg != null) ? '\n== ' + msg : '';
        logOutput += startStop;
        if(DEBUG){
            lastDebugMessage = logOutput;
            System.debug(logOutput);
        }   
    }

    public class InventoryNA extends DTT_RestClient
    {

        public  InventoryNA(DTT_RestPayload_NA.ProductInformation productInfo)
        {
            super();
            String sJSONBody = JSON.Serialize(productInfo);
            DTT_Post( 'https://ps1w2.rt.informaticacloud.com/active-bpel/services/REST/000V28/InventorySyncService', 'POST', InventoryHeader, sJSONBody);
        }

        public map<string, string> InventoryHeader
        {
            get
            {
                map<string, string> retVal = new map<string, string>();
                retVal.put('Content-type', 'application/json');
                retVal.put('Authorization', 'Basic cGlkdWZvdXJAZGVsb2l0dGUuY2EuZGV2OndlbGNvbWUxMjM0');
                return retval;
            }
        }

        public list<DTT_RestResponse_NA.ProductWrapper> ParseBody()
        {
            
            List<DTT_RestResponse_NA.ProductWrapper> retVal = new List<DTT_RestResponse_NA.ProductWrapper>();
            if (responseBody != null)
            {
                responseBody.replace('_void', 'void_x');
                retVal = (List<DTT_RestResponse_NA.ProductWrapper>)JSON.deserialize(responseBody , List<DTT_RestResponse_NA.ProductWrapper>.class);
            }
            return retVal;
        }

    }

    public class InventoryEMEA extends DTT_RestClient
    {

        public  InventoryEMEA(DTT_RestPayload_EMEA.ProductInformation productInfo)
        {
            super();
            String sJSONBody = JSON.Serialize(productInfo);
         //   DTT_Post( 'https://ps1w2.rt.informaticacloud.com/active-bpel/services/REST/000V28/InventorySyncSAPService', 'POST', InventoryHeader, sJSONBody);
       DTT_Post('https://ps1w2.rt.informaticacloud.com/active-bpel/services/REST/000V28/GetInventoryInfoService', 'POST', InventoryHeader, sJSONBody);
        }

        public map<string, string> InventoryHeader
        {
            get
            {
                map<string, string> retVal = new map<string, string>();
                retVal.put('Content-type', 'application/json');
                retVal.put('Authorization', 'Basic cGlkdWZvdXJAZGVsb2l0dGUuY2EuZGV2OndlbGNvbWUxMjM0');
                return retval;
            }
        }

        public list<DTT_RestResponse_EMEA.ProductWrapper> ParseBody()
        {
            List<DTT_RestResponse_EMEA.ProductWrapper> retVal = new List<DTT_RestResponse_EMEA.ProductWrapper>();
            if(responseBody!=null){
            integer startIndex = responseBody.indexOf('"item"');
            integer endIndex = responseBody.indexOf('"xmlns:n0"');


            if (startIndex > 0)
            {
                string tempBody = responseBody.substring(startIndex + 7, endIndex - 2);
                system.debug('DTT_RestClient.cls.DTT_RestClient_InventoryEMEA '  + tempBody);   

                retVal = (List<DTT_RestResponse_EMEA.ProductWrapper>)JSON.deserialize(tempBody , List<DTT_RestResponse_EMEA.ProductWrapper>.class);
            
            }
            }
            return retVal;
        }
    }

    
}