/*************************************************************************************
Classe Name : CS_Extension_NewOrderFromAccount
Description : Create a new order from Account. Check for all validations before navigating to the order creation page:
Created By  : Jayadev Rath
Created Date: 22-Aug-14
Modification Log:
---------------------------------------------------------------------------------- 
Developer             Date             Description
---------------------------------------------------------------------------------- 
Jayadev Rath         22-Aug-2014       Initial Version
Divya A N            16-Sep-2014       Added Extra Check to prevent usrs from creating Orders from the recently viewed Orders
************************************************************************************/

Public Class CS_Extension_NewOrderFromAccount{
    
    Private Final String sACCOUNT_CREDIT_STATUS = 'INACTIVE';
    Public String sAccountId {get;set;}
    
    /*********************************************************************************
    Method Name    : Constructor
    Description    : Get the Account information
    Return Type    : None
    Parameter      : Standard Controller
    *********************************************************************************/
    Public CS_Extension_NewOrderFromAccount(ApexPages.StandardController controller) {
        // Get the account details first from the page level parameters
        sAccountId = ApexPages.CurrentPage().getParameters().get('aid');
        
    }
    
    /*********************************************************************************
    Method Name    : Redirect
    Description    : From the account information, validate all conditions. If passed, redirect to Order creation page else throw appropriate error message.
    Return Type    : PageReference
    Parameter      : None
    *********************************************************************************/
    Public PageReference Redirect() {
        if(sAccountId == null)
        {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.CS_Sold_To_Account_Validation));
                    return null;
        }
        // Check if this is an account Id or not (needs to be generailised)
        if(sAccountId != null && sAccountId != '' && sAccountId.StartsWith('001')) {
            // Collect the Account details from account Id:
            List<Account> lstAccount = [SELECT Id, Credit_Status__c FROM Account WHERE Id = :sAccountId.left(15)];
            if(lstAccount != null) {
                
                // Check if the credit status is InActive. If InActive don't proceed.
                if(lstAccount[0].Credit_Status__c == sACCOUNT_CREDIT_STATUS) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.CS_Credit_Status_Validation));
                    return null;
                }
                // More validation to be added here:
            }
        }
        
        
        // If all validtions passed, Redirect to new Order creation page:
        //PageReference oNewOrderPage = new PageReference('/801/e?nooverride=1');
        PageReference oNewOrderPage = new PageReference('/setup/ui/recordtypeselect.jsp?ent=Order&retURL=%2F'+sAccountId+'&save_new_url=%2F801%2Fe%3Fnooverride=1%26retURL%3D%252F'+sAccountId+'%26aid%3D'+sAccountId);
        // Put all the parameters from the old page to the new order creation page
        for(String s: ApexPages.CurrentPage().getParameters().keySet()){
            // Skip the 'save_new' parameter as it is causing a invalid session error:
            if(s != 'save_new')
                oNewOrderPage.getParameters().put(s, ApexPages.CurrentPage().getParameters().get(s));
        }
        return oNewOrderPage;
        
        
        
    }
    
    /*********************************************************************************
    Method Name    : Redirect
    Description    : Go back to Account screen
    Return Type    : PageReference
    Parameter      : None
    *********************************************************************************/
    Public PageReference Cancel() {
        if(sAccountId != null){
        return new PageReference('/'+sAccountId);
        }
        else
        return new PageReference('/001/o');
    }
}