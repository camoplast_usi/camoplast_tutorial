/*********************************************************************************
Class Name      : CS_Order_PostControl
Description     : This is class is the extension for CS_OrderStatusBanner
Created By      : Reju Palathingal
Created Date    : 2014-08-31
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Reju                        31-08-2014            Initial Version
Jayadev Rath                16-09-2014            Added the webservice call for POST() method
Jayadev Rath                17-09-2014            Added visual elements for In Progress Status
*********************************************************************************/
public with sharing class CS_Order_PostControl {


    private Order refOrder = null;
    public Boolean refreshPage {get; set;}
    Public Boolean bInProgress {get;set;}
    
    public CS_Order_PostControl(ApexPages.StandardController controller) {
        if (controller != null)
            if (controller.getRecord() != null)
                    for (Order oAcc : [Select Id, Posted__c,Record_Type_Name__c ,Last_Sync_Datetime_with_ERP__c,lastmodifieddate,Is_Synced_From_SFDC__c  from Order where Id =: controller.getRecord().Id limit 1]) 
                        refOrder = oAcc;
    }

    public PageReference Post() {
        // Make the callout test:
        bInProgress = true;
        refreshPage = true;
        try {
            // POST to SAP only for SAP record type Orders:
            if(refOrder.Record_Type_Name__c =='EMEA_Sales_Order' || refOrder.Record_Type_Name__c =='EMEA_Service_Order')
                PostCallout_EMEA();
            refOrder.Posted__c = true;
            refOrder.Last_Sync_Datetime_with_ERP__c  = system.now();
            update refOrder;
        }
        Catch (Exception e){
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));//Label.CS_Connectivity_Issue));
            refreshPage = false;
            system.debug('Exception caught: $$$: '+e.getMessage()); 
        }
        return null;
    }
    
    public void Sync()
    {
//The 
            if(refOrder.Record_Type_Name__c == 'EMEA_Sold_To' || (refOrder.Posted__c  && (refOrder.Last_Sync_Datetime_with_ERP__c <= refOrder.lastModifiedDate))){
            refOrder.Last_Sync_Datetime_with_ERP__c  = system.now();
             update refOrder;
              system.debug(refOrder.Is_Synced_From_SFDC__c + '$$$$');
              
             }
        
       system.debug(refOrder.Last_Sync_Datetime_with_ERP__c + '####');  
      /* if(refAccount.Last_Sync_Datetime_with_ERP__c == NULL){
            refAccount.Last_Sync_Datetime_with_ERP__c  = system.now();
        }*/
        refreshPage = true;
        
    }
    
    Public String CS_Order_refresh
    {
    get{
    
    Map<String,Object> mapJsonBody = new Map<String,Object>();
            
            // Prepare the JSONBody:
            mapJsonBody.put('SAP_Order_Number', refOrder.Type);
            mapJsonBody.put('Session_id',UserInfo.getSessionId());
            mapJsonBody.put('ServerUrl',DTT_ICRT_Helper.ServerURL);
            
            
            String sJSONBody = JSON.serializePretty(mapJsonBody);
            
            return sJSONBody;
            }
    }
    
    
    //Headers for the outbound call:
    Public map<string, string> mapHeader {
        get{
                map<string, string> retVal = new map<string, string>();
                retVal.put('Content-type', 'application/json');
                retVal.put('Authorization', 'Basic cGlkdWZvdXJAZGVsb2l0dGUuY2EuZGV2OndlbGNvbWUxMjM0');
                return retval;
        }
    }
    
    /*********************************************************************************
    Method Name    : PostCallout_EMEA
    Description    : Posts the Order details to EMEA
    Return Type    : Void
    Parameter      : None
    *********************************************************************************/
    Public void PostCallout_EMEA() {

        Order oOrder = [SELECT Id, Type,Sales_Organization__c,Distribution_Channel__c,Division__c,Sales_Office__c,Sales_Group__c,
        
        AccountId,Account.AccountNumber,Ship_To_Account__c,Ship_To_Account_RelatedAccount__r.Ship_To__r.AccountNumber,CreatedById,CreatedBy.Name,ShipToContactId, ShipToContact.Name,Account.Material_Service_Type__c,
        CreatedBy.SAP_UserId__c,
        
        PoNumber,PoDate,Requested_Delivery_Date__c,Warehouse__c,CurrencyIsoCode,Price_List__c,Price_Group__c,Order_Reason__c,POD_Relevant__c,Incoterms__c,Incoterms_Location__c,Payment_Terms__c,Billing_Block__c,Billing_Date__c,Payment_Method__c,Order_Notes__c,Time_On_Site__c,Time_Off_Site__c,VOR_Machine__c,Service_Technician_Name__c,Customer_Job_Number__c,Customer_Engineering_Team__c,Customer_Engineering_Region__c,Internal_Job_number__c,Third_Party_Job_Number__c,Truck_Make__c,Truck_Model__c,Truck_Serial__c,Truck_Fleet__c,POD_Name__c,Hour_Reading__c,LastModifiedDate,SAP_Order_Number__c,Complete_Delivery__c,CreatedDate,Document_Date__c,
        Date_Job_Date_Received__c,Date_Job_Sheet_Processed__c,SAP_External_Id__c, Account_Material_Service_Type__c,
            (SELECT Id, Item__c,Global_PID__c,Quantity__c,Location__c,Location__r.Location_Code__c,Wheel_Position__c,Tyre_Condition__c,Torque_Setting__c,Wex__c,Fitted_On_Site__c, PriceBookEntry.product2.Unit_Of_Measure__c,PriceBookEntry.product2.Global_PID__c, Sales_Unit__c
            FROM OrderItems)
        
        
        FROM Order WHERE Id= :refOrder.Id   ];     //'801110000006Rjh'
        
        // KTGRD - Account.Material_Service_Type__c
        
        Map<String,Object> mapOrderPayload = new Map<String,Object>();
        mapOrderPayload.put('Session_id',UserInfo.getSessionId());
        mapOrderPayload.put('ServerUrl',DTT_ICRT_Helper.ServerURL);
        mapOrderPayload.put('Type',oOrder.Type);
        mapOrderPayload.put('Sales_Organization',oOrder.Sales_Organization__c);
        mapOrderPayload.put('Distribution_Channel',oOrder.Distribution_Channel__c);
        mapOrderPayload.put('Division',oOrder.Division__c);
        mapOrderPayload.put('Sales_Office',oOrder.Sales_Office__c);
        mapOrderPayload.put('Sales_Group',oOrder.Sales_Group__c);
        mapOrderPayload.put('Account_Name',oOrder.Account.AccountNumber);
        mapOrderPayload.put('Ship_To_Account',oOrder.Ship_To_Account_RelatedAccount__r.Ship_To__r.AccountNumber);
        mapOrderPayload.put('PO_Number',oOrder.PoNumber);
        mapOrderPayload.put('PO_Date',oOrder.PoDate);    // Order in mm.dd.yyyy format. Transformation In IOC
        mapOrderPayload.put('Requested_Delivery_Date',oOrder.Requested_Delivery_Date__c);
        mapOrderPayload.put('Warehouse',oOrder.Warehouse__c);
        mapOrderPayload.put('Order_Currency',oOrder.CurrencyIsoCode);
        mapOrderPayload.put('Price_List',oOrder.Price_List__c);
        mapOrderPayload.put('Price_Group',oOrder.Price_Group__c);
        mapOrderPayload.put('Order_Reason',oOrder.Order_Reason__c);
        mapOrderPayload.put('POD_Relevant',oOrder.POD_Relevant__c);
        mapOrderPayload.put('Incoterms',oOrder.Incoterms__c);
        mapOrderPayload.put('Incoterms_Location',oOrder.Incoterms_Location__c);
        mapOrderPayload.put('Payment_Terms',oOrder.Payment_Terms__c);
        mapOrderPayload.put('Billing_Block',oOrder.Billing_Block__c);
        mapOrderPayload.put('Billing_Date',oOrder.Billing_Date__c);
        mapOrderPayload.put('Payment_Method',oOrder.Payment_Method__c);
        mapOrderPayload.put('Order_Notes',oOrder.Order_Notes__c);
        mapOrderPayload.put('Date_Job_Date_Received',oOrder.Date_Job_Sheet_Processed__c);
        mapOrderPayload.put('Date_Job_Sheet_Processed',oOrder.Date_Job_Sheet_Processed__c);
        mapOrderPayload.put('Time_On_Site',oOrder.Time_On_Site__c);
        mapOrderPayload.put('Time_Off_Site',oOrder.Time_Off_Site__c);
        mapOrderPayload.put('VOR_Machine',oOrder.VOR_Machine__c);
        mapOrderPayload.put('Ship_To_Contact_Name',oOrder.ShipToContact.Name);
        mapOrderPayload.put('Service_Technician_Name',oOrder.Service_Technician_Name__c);
        mapOrderPayload.put('Customer_Job_Number',oOrder.Customer_Job_Number__c);
        mapOrderPayload.put('Customer_Engineering_Team',oOrder.Customer_Engineering_Team__c);
        mapOrderPayload.put('Customer_Engineering_Region',oOrder.Customer_Engineering_Region__c);
        mapOrderPayload.put('Internal_Job_number',oOrder.Internal_Job_number__c);
        mapOrderPayload.put('Third_Party_Job_Number',oOrder.Third_Party_Job_Number__c);
        mapOrderPayload.put('Truck_Make',oOrder.Truck_Make__c);
        mapOrderPayload.put('Truck_Model',oOrder.Truck_Model__c);
        mapOrderPayload.put('Truck_Serial',oOrder.Truck_Serial__c);
        mapOrderPayload.put('Truck_Fleet',oOrder.Truck_Fleet__c);
        mapOrderPayload.put('POD_Name',oOrder.POD_Name__c);
        mapOrderPayload.put('Hour_Reading',oOrder.Hour_Reading__c);
        mapOrderPayload.put('Last_Modified_Date',oOrder.LastModifiedDate);
        mapOrderPayload.put('SAP_Order_Number',oOrder.SAP_Order_Number__c);
        mapOrderPayload.put('Complete_Delivery',oOrder.Complete_Delivery__c);
        //mapOrderPayload.put('Created_By_ID',oOrder.CreatedBy.Name);
        
        // Passing the SAP user name for now:
        mapOrderPayload.put('Created_By_ID',oOrder.CreatedBy.SAP_UserId__c);
        
        mapOrderPayload.put('Created_Date',oOrder.CreatedDate);
        mapOrderPayload.put('Document_Date',oOrder.Document_Date__c);
        //mapOrderPayload.put('Created_Date',String.valueOf(oOrder.CreatedDate).left(10));
        //mapOrderPayload.put('Document_Date',String.valueOf(oOrder.Document_Date__c).left(10));
        
        //mapOrderPayload.put('Material_Service_Type',oOrder.Account.Material_Service_Type__c);
        
        // New field on Order is available.
        mapOrderPayload.put('Material_Service_Type',oOrder.Account_Material_Service_Type__c);
        
        // This field will determine a CREATE or UPDATE Call in Informatica (If null, then Create call)
        mapOrderPayload.put('SAP_External_Id',oOrder.SAP_External_Id__c);
        
        List<Map<String,Object>> lstOrderItems = new List<Map<String,Object>>();
        
        For(OrderItem oi: oOrder.OrderItems){
            // Global_PID__c,Quantity__c,Location__c,Wheel_Position__c,Tyre_Condition__c,Torque_Setting__c,Wex__c,Fitted_On_Site__c FROM OrderItems)
            Map<String,Object> mapOrderItem = new Map<String,Object>();
            
            mapOrderItem.put('Material',oi.PriceBookEntry.product2.Global_PID__c);
            mapOrderItem.put('Order_Quantity',oi.Quantity__c);
            //mapOrderItem.put('UoM',oi.PriceBookEntry.product2.Unit_Of_Measure__c);
            mapOrderItem.put('Location',oi.Location__r.Location_Code__c);
            mapOrderItem.put('Wheel_Position',oi.Wheel_Position__c);
            mapOrderItem.put('Tyre_Condition',oi.Tyre_Condition__c);
            mapOrderItem.put('Torque_Setting',oi.Torque_Setting__c);
            mapOrderItem.put('Wex',oi.Wex__c);
            mapOrderItem.put('Fitted_On_Site',oi.Fitted_On_Site__c);
            //mapOrderItem.put('Line_Item_Number',String.valueOf(oi.Item__c));
            mapOrderItem.put('Line_Item_Number',oi.Item__c);
            mapOrderItem.put('Sales_Unit',oi.Sales_Unit__c);
            lstOrderItems.add(mapOrderItem);
        }
        // Add all the Order Items
        mapOrderPayload.put('Items',lstOrderItems);
        
        String sOrderJSONBody = JSON.SerializePretty(mapOrderPayload);
        
        System.debug('@@ Order JSON:'+sOrderJSONBody);

        
        // Make the callout
        
        System.debug('@@ sOrderJSONBody' + sOrderJSONBody);
        DTT_RestClient restClient = new DTT_RestClient('https://ps1w2.rt.informaticacloud.com/active-bpel/services/REST/000V28/POSTOrdersEMEAService','POST',mapHeader, sOrderJSONBody);
        System.debug(restClient.responseBody);
        System.debug('@@ Response Body' + restClient.responseBody);
        // Check if response is Empty or has any content:
        if(restClient.responseBody == null) {
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.CS_Connectivity_Issue));
            return;
        }
        
        // Temporary step, as payload columns are having SPACES. Which is breaking the parsing.
        String sSAPOrderNumber;
        /*
        If(restClient.responseBody.length() == 63) {    // Response for a POST has 63 characters
            sSAPOrderNumber = restClient.responseBody.substring(54,61);
            refOrder.SAP_External_Id__c = sSAPOrderNumber;    // What should be the External ID
            refOrder.SAP_Order_Number__c = sSAPOrderNumber;
            Update refOrder;
        }
        
        */
        
        
        
        DTT_RestResponse_EMEA.OrderWrapper lstOrderDetails = (DTT_RestResponse_EMEA.OrderWrapper)JSON.deserialize(restClient.responseBody , DTT_RestResponse_EMEA.OrderWrapper.class);
        if(lstOrderDetails != null) {
            refOrder.SAP_External_Id__c = lstOrderDetails.Order_Number;
            refOrder.SAP_External_Id__c = lstOrderDetails.Order_Number;
            Update refOrder;
        }
        
    }
    
    // Change bInProgress  to true to display the In Progress Icon on the page level.
    Public Void ChangeToInProgress(){
        system.debug('#### changing to in progress');
        bInProgress = true;
        //bInProgress = (bInProgress == null ? true : !bInProgress);
        //return null;
    }
 }