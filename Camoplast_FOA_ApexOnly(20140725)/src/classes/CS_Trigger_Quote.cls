/*********************************************************************************
Class Name : CS_Trigger_Quote
Description : This class will be invoked by the triggers on Quote
Created By : Maryam Abbas
Created Date : 14-Jul-14
Modification Log:
---------------------------------------------------------------------------------- 
Developer Date Description
---------------------------------------------------------------------------------- 
Maryam Abbas 14-Jul-14   Initial Version
Divya A N    23-07-14    Adding the Show_on_Print__c carry forward
Maryam Abbas 28-Jul-14   Bulkified code
Maryam Abbas 8-Aug-14    Added method updateOptyInfo
*********************************************************************************/

public class CS_Trigger_Quote
{
    public static void updateTermsandConditions(List<Quote> lstQuote)
    { 
        
        Set<Id> setOptyIds = new Set<Id>();
        for(Quote oQuote: lstQuote){
            setOptyIds.add(oQuote.OpportunityId);
        }  
        
        List<Terms_and_Conditions__c> CS_lstQuoteTerms= new List<Terms_and_Conditions__c>();
        
        Map<Id, List<Terms_and_Conditions__c>> mapTnC= new Map<Id, List<Terms_and_Conditions__c>>();
       
        List<Opportunity> lstOpty = [Select Id, (Select Terms_and_Conditions__c, Show_on_Print__c From Terms_and_Conditions__r) From Opportunity where Id in :setOptyIds];
      
        for(Opportunity oOpty : lstOpty)
        {
           List<Terms_and_Conditions__c> TnC = oOpty.getSObjects('Terms_and_Conditions__r'); // get all Terms and Conditions for the Opty
           mapTnC.put(oOpty.Id, TnC); // put the Terms and Conditions in a map by Opportunity Id
           
        }        
        List<Terms_and_Conditions__c> lstTnC = new List<Terms_and_Conditions__c>();
        
        Terms_and_Conditions__c CS_oQuoteTerm;
        
        for (Quote CS_Quote: lstQuote){          
             if( CS_Quote.OpportunityId !=null)
             {     lstTnC = mapTnC.get(CS_Quote.OpportunityId); 
                   if(lstTnC != null)
                   {    for (Terms_and_Conditions__c CS_oTerm: lstTnC){
                            
                            CS_oQuoteTerm= new Terms_and_Conditions__c();
                            CS_oQuoteTerm.QuoteId__c = CS_Quote.Id;
                            CS_oQuoteTerm.Terms_and_Conditions__c=CS_oTerm.Terms_and_Conditions__c;
                            CS_oQuoteTerm.Show_on_Print__c = CS_oTerm.Show_on_Print__c;
                            CS_lstQuoteTerms.add(CS_oQuoteTerm);
                        }
                   }   
             }           
        }
        insert CS_lstQuoteTerms;        
    }
/*********************************************************************************
Method Name : updateOptyInfo
Description : To transfer opportunity info to quote
Return Type : void
Parameter : 1. lstOpportunity
*********************************************************************************/
    public static void updateOptyInfo(List<Quote> lstQuote)
    {      
            String recordType='';
            Set<Id> CS_setOptyIds= new Set<Id>();
            for(Quote oQuote: lstQuote){
                CS_setOptyIds.add(oQuote.OpportunityId);
            }
                          
            Map<Id,Opportunity> mapOptyAccountDetails = new Map<Id,Opportunity>([SELECT Sales_Office__c,Location__c,Selling_Location__c,Service_Technician_Name__c, Customer_Job_Number__c, Customer_Engineering_Team__c, Customer_Engineering_Region__c, Internal_Job_number__c, Third_Party_Job_Number__c, Truck_Make__c, Truck_Model__c, Truck_Serial__c, Truck_Fleet__c, POD_Name__c, Hour_Reading__c,Ship_To_Account__c,Opportunity_Type__c, Sales_Organization__c, Distribution_Channel__c, Division__c, Requested_Delivery_Date__c, Order_Reason__c, POD_Relevant__c, Billing_Block__c, Billing_Date__c, Date_Job_Sheet_Processed__c, Time_On_Site__c, VOR_Machine__c,  Warehouse__c, CurrencyIsoCode, Price_List__c, Payment_Terms__c, Price_Group__c, Incoterms__c, Incoterms_Location__c, Complete_Delivery__c, Payment_Method__c FROM Opportunity where Id in : CS_setOptyIds]);
          
            for (Quote CS_oQuote : lstQuote){
                Map<ID, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Quote.getRecordTypeInfosById();
                if(rtMap.get(CS_oQuote .RecordTypeId) !=null)
                    recordType = rtMap.get(CS_oQuote.RecordTypeId).getName();
                if(recordType=='EMEA Sales Quote'|| recordType =='EMEA Service Quote'){
                    if( CS_oQuote.OpportunityId!=null)
                    {
                        CS_oQuote.Location__c = CS_oQuote.Location__c == null?mapOptyAccountDetails.get(CS_oQuote.OpportunityId).Location__c:CS_oQuote.Location__c;
                        CS_oQuote.Selling_Location__c = CS_oQuote.Selling_Location__c == null?mapOptyAccountDetails.get(CS_oQuote.OpportunityId).Selling_Location__c:CS_oQuote.Selling_Location__c;
                        CS_oQuote.Price_List__c =CS_oQuote.Price_List__c== null?mapOptyAccountDetails.get(CS_oQuote.OpportunityId).Price_List__c:CS_oQuote.Price_List__c ;
                        CS_oQuote.Price_Group__c=CS_oQuote.Price_Group__c==null?mapOptyAccountDetails.get(CS_oQuote.OpportunityId).Price_Group__c:CS_oQuote.Price_Group__c ;
                        CS_oQuote.Incoterms__c = CS_oQuote.Incoterms__c==null?mapOptyAccountDetails.get(CS_oQuote.OpportunityId).Incoterms__c:CS_oQuote.Incoterms__c ;
                        CS_oQuote.Incoterms_Location__c =CS_oQuote.Incoterms_Location__c==null?mapOptyAccountDetails.get(CS_oQuote.OpportunityId).Incoterms_Location__c:CS_oQuote.Incoterms_Location__c ;
                        CS_oQuote.Payment_Terms__c = CS_oQuote.Payment_Terms__c==null?mapOptyAccountDetails.get(CS_oQuote.OpportunityId).Payment_Terms__c:CS_oQuote.Payment_Terms__c;
                        //CS_oQuote.Complete_Delivery__c= CS_oQuote.Complete_Delivery__c==false?mapOptyAccountDetails.get(CS_oQuote.OpportunityId).Complete_Delivery__c:CS_oQuote.Complete_Delivery__c;
                        //CS_oQuote.Quote_Type__c= CS_oQuote.Quote_Type__c==null?mapOptyAccountDetails.get(CS_oQuote.OpportunityId).Opportunity_Type__c:CS_oQuote.Quote_Type__c;
                        CS_oQuote.Sales_Organization__c= CS_oQuote.Sales_Organization__c==null?mapOptyAccountDetails.get(CS_oQuote.OpportunityId).Sales_Organization__c:CS_oQuote.Sales_Organization__c;
                        CS_oQuote.Sales_Office__c= CS_oQuote.Sales_Office__c== null?mapOptyAccountDetails.get(CS_oQuote.OpportunityId).Sales_Office__c:CS_oQuote.Sales_Office__c;
                        CS_oQuote.Distribution_Channel__c= CS_oQuote.Distribution_Channel__c==null?mapOptyAccountDetails.get(CS_oQuote.OpportunityId).Distribution_Channel__c:CS_oQuote.Distribution_Channel__c;
                        CS_oQuote.Division__c=CS_oQuote.Division__c==null?mapOptyAccountDetails.get(CS_oQuote.OpportunityId).Division__c:CS_oQuote.Division__c;
                        CS_oQuote.Requested_Delivery_Date__c=CS_oQuote.Requested_Delivery_Date__c==null?mapOptyAccountDetails.get(CS_oQuote.OpportunityId).Requested_Delivery_Date__c:CS_oQuote.Requested_Delivery_Date__c;
                        CS_oQuote.Order_Reason__c= CS_oQuote.Order_Reason__c==null?mapOptyAccountDetails.get(CS_oQuote.OpportunityId).Order_Reason__c:CS_oQuote.Order_Reason__c;
                        CS_oQuote.POD_Relevant__c= CS_oQuote.POD_Relevant__c==false?mapOptyAccountDetails.get(CS_oQuote.OpportunityId).POD_Relevant__c:CS_oQuote.POD_Relevant__c;
                        CS_oQuote.Billing_Block__c=CS_oQuote.Billing_Block__c==null?mapOptyAccountDetails.get(CS_oQuote.OpportunityId).Billing_Block__c:CS_oQuote.Billing_Block__c;
                        CS_oQuote.Billing_Date__c= CS_oQuote.Billing_Date__c==null?mapOptyAccountDetails.get(CS_oQuote.OpportunityId).Billing_Date__c:CS_oQuote.Billing_Date__c;
                        CS_oQuote.Date_Job_Sheet_Processed__c= CS_oQuote.Date_Job_Sheet_Processed__c==null?mapOptyAccountDetails.get(CS_oQuote.OpportunityId).Date_Job_Sheet_Processed__c:CS_oQuote.Date_Job_Sheet_Processed__c;
                        CS_oQuote.Time_On_Site__c=CS_oQuote.Time_On_Site__c==null?mapOptyAccountDetails.get(CS_oQuote.OpportunityId).Time_On_Site__c:CS_oQuote.Time_On_Site__c;
                        CS_oQuote.VOR_Machine__c= CS_oQuote.VOR_Machine__c==false?mapOptyAccountDetails.get(CS_oQuote.OpportunityId).VOR_Machine__c:CS_oQuote.VOR_Machine__c;
                        CS_oQuote.Payment_Method__c= CS_oQuote.Payment_Method__c==null?mapOptyAccountDetails.get(CS_oQuote.OpportunityId).Payment_Method__c:CS_oQuote.Payment_Method__c;
                        CS_oQuote.Ship_To__c=CS_oQuote.Ship_To__c==null?mapOptyAccountDetails.get(CS_oQuote.OpportunityId).Ship_to_Account__c:CS_oQuote.Ship_To__c;
                        CS_oQuote.Service_Technician_Name__c= CS_oQuote.Service_Technician_Name__c==null?mapOptyAccountDetails.get(CS_oQuote.OpportunityId).Service_Technician_Name__c:CS_oQuote.Service_Technician_Name__c;
                        CS_oQuote.Customer_Job_Number__c= CS_oQuote.Customer_Job_Number__c==null?mapOptyAccountDetails.get(CS_oQuote.OpportunityId).Customer_Job_Number__c:CS_oQuote.Customer_Job_Number__c;
                        CS_oQuote.Customer_Engineering_Team__c= CS_oQuote.Customer_Engineering_Team__c==null?mapOptyAccountDetails.get(CS_oQuote.OpportunityId).Customer_Engineering_Team__c:CS_oQuote.Customer_Engineering_Team__c;
                        CS_oQuote.Customer_Engineering_Region__c= CS_oQuote.Customer_Engineering_Region__c==null?mapOptyAccountDetails.get(CS_oQuote.OpportunityId).Customer_Engineering_Region__c:CS_oQuote.Customer_Engineering_Region__c;
                        CS_oQuote.Internal_Job_number__c= CS_oQuote.Internal_Job_number__c==null?mapOptyAccountDetails.get(CS_oQuote.OpportunityId).Internal_Job_number__c:CS_oQuote.Internal_Job_number__c;
                        CS_oQuote.Third_Party_Job_Number__c= CS_oQuote.Third_Party_Job_Number__c==null?mapOptyAccountDetails.get(CS_oQuote.OpportunityId).Third_Party_Job_Number__c:CS_oQuote.Third_Party_Job_Number__c;
                        CS_oQuote.Truck_Make__c= CS_oQuote.Truck_Make__c==null?mapOptyAccountDetails.get(CS_oQuote.OpportunityId).Truck_Make__c:CS_oQuote.Truck_Make__c;
                        CS_oQuote.Truck_Model__c=CS_oQuote.Truck_Model__c==null?mapOptyAccountDetails.get(CS_oQuote.OpportunityId).Truck_Model__c:CS_oQuote.Truck_Model__c;
                        CS_oQuote.Truck_Serial__c=CS_oQuote.Truck_Serial__c==null? mapOptyAccountDetails.get(CS_oQuote.OpportunityId).Truck_Serial__c:CS_oQuote.Truck_Serial__c;
                        CS_oQuote.Truck_Fleet__c= CS_oQuote.Truck_Fleet__c==null?mapOptyAccountDetails.get(CS_oQuote.OpportunityId).Truck_Fleet__c:CS_oQuote.Truck_Fleet__c;
                        CS_oQuote.POD_Name__c=CS_oQuote.POD_Name__c==null?mapOptyAccountDetails.get(CS_oQuote.OpportunityId).POD_Name__c:CS_oQuote.POD_Name__c;
                        CS_oQuote.Hour_Reading__c=CS_oQuote.Hour_Reading__c==null?mapOptyAccountDetails.get(CS_oQuote.OpportunityId).Hour_Reading__c:CS_oQuote.Hour_Reading__c;
                        if(CS_oQuote.Quote_Type__c == null||CS_oQuote.Quote_Type__c =='')
                            if(recordType=='EMEA Sales Quote')
                                CS_oQuote.Quote_Type__c='ZEQT';
                            else if (recordType =='EMEA Service Quote')
                                CS_oQuote.Quote_Type__c='ZEQS';
                        
                        //CSoQuote.CurrencyIsoCode = mapAccountDetails.get(CS_oQuote.AccountId).CurrencyIsoCode ;
                     }
                  
                }
                else if (recordType == 'North America')
                {   
                    if( CS_oQuote.OpportunityId!=null)
                        {     CS_oQuote.Location__c = CS_oQuote.Location__c == null?mapOptyAccountDetails.get(CS_oQuote.OpportunityId).Location__c:CS_oQuote.Location__c;
                              CS_oQuote.Selling_Location__c = CS_oQuote.Selling_Location__c == null?mapOptyAccountDetails.get(CS_oQuote.OpportunityId).Selling_Location__c:CS_oQuote.Selling_Location__c;
                       
                        }
                
                }
                     
                
            }    
                  
        
    }
    

}