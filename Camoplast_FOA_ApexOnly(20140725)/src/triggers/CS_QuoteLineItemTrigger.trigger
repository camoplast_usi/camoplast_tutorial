/*************************************************************************************
Trigger Name : CS_QuoteLineItemTrigger
Description : Trigger for ‘QuoteLineItem' object. 
Created By : Maryam Abbas
Created Date : 8-27-2014
Modification Log:
---------------------------------------------------------------------------------- 
Developer Date Description
---------------------------------------------------------------------------------- 
Maryam  27-Aug-14 Initial Version
************************************************************************************/
trigger CS_QuoteLineItemTrigger on QuoteLineItem(before insert, after insert) {

     if(Trigger.isAfter && Trigger.isInsert){
        CS_Trigger_QuoteLineItem.copyOptyLineItemInfo(trigger.new);
    }
   
    
}