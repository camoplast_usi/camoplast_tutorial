// Class for testing Vishal's Webservice for ICRT Test:
// CAN BE DELETED AFTER MIDDLEWARE TESTING IS COMPLETED
Public Class CS_ICRTServiceTest{
    
    Public String sNASoldToUrl ='https://ps1w2.rt.informaticacloud.com/active-bpel/services/REST/000V28/Vishal_AccSoldToNAService';
    Public String sInvUrl ='https://ps1w2.rt.informaticacloud.com/active-bpel/services/REST/000V28/Vishal_InventorySyncService';
    Public String sInvSAPUrl ='https://ps1w2.rt.informaticacloud.com/active-bpel/services/REST/000V28/InventorySyncSAPService';
    public String sInvEMEA = 'https://ps1w2.rt.informaticacloud.com/active-bpel/services/REST/000V28/GetInventoryInfoService';
    Public String sNAShipToUrl ='https://ps1w2.rt.informaticacloud.com/active-bpel/services/REST/000V28/Vishal_AccShipToNAService2';
    Public String sOrderSAPEndpoint =' https://ps1w2.rt.informaticacloud.com/active-bpel/services/REST/000V28/OrdersManualRefreshService';
    Public String sNAPdfServiceEndpoint ='https://ps1w2.rt.informaticacloud.com/active-bpel/services/REST/000V28/GetPDF_SAP_Orders';


    public String responseBody_EMEA {    
        get {
            return '[{"StopoOpenQty": 576,"Plant": 3101},{"StopoOpenQty": 0,"Plant": 3101}]';
        }
        set;
    }
    // Payload for Ship To request
    Public String requestBody_NAShipTo {
        get{
            return  '{"customer_code": "CS000001"}';
        }
        set;
    }
    
    // Payload for Sold To request
    Public String requestBody_NASoldTo {
        get{
            return  '{"customer_code": "CS000001"}';
        }
        set;
    }    
    
 // Payload for EMEA Order Manual Refresh
    Public String requestBody_EMEAOrderRefresh {
        get{
            return  '{"SAP_Order_Number": "5000697"}';
        }
        set;
    }  
    
    // Generic request body for Inventory Callout
    Public String requestBody_Inventory {
        get{
        return  '{'+
                '"ProductCodes": ['+
                '    "1.70.407",'+
                '    "1.805.8213",'+
                '    "P9203XFL"'+
                '  ],'+
                '  "Location": "1CHA",'+
                '  "SalesOrganization": "solidealUSA" '+
                '}';

        }
        set;
    }
      // Generic request body for Inventory Callout
    Public String requestBody_SAPInventory {
        get{


        return  '{'+
                '"DistrChan": "",'+
                 '"Division": "",'+
                  '"Material": "9.225.2255",'+
                '"Plant": "3101",'+
                '"SoldTo": "200005" '+
                '}';

        }
        set;
    }
       /* Public String requestBody_EMEAInventory {
        get{


        return  '{'+
                '"DistrChan": "",'+
                 '"Division": "",'+
                  '"Material":' + '['+'"9.225.2255",' + '"9.225.5002"' + '], ' +
                '"Plant": "3101",'+
                '"SoldTo": "200005" '+
                '}';

        }
        set;
    }*/
    
Public String requestBody_EMEAInventory {
        get{


        return  '{'+
                '"DistrChan": "",'+
                 '"Division": "",'+
                  '"Material":"9.225.5002", '+
                '"Plant": "3101",'+
                '"SoldTo": "200005" '+
                '}';

        }
        set;
    }    // Callout to ICRT webservice (for testing Inventory SAP Sync Service)
    Public Void ICRTCallout_SAPInventory(){
        HttpRequest request= new HttpRequest();
        request.setTimeout(60000);
        //='https://ps1w2.rt.informaticacloud.com/active-bpel/services/REST/000V28/trial1service';
        request.setEndpoint(sInvSAPUrl);
        request.setMethod('POST');
        
        request.setHeader('Content-type', 'application/json');
        request.setHeader('Authorization', 'Basic cGlkdWZvdXJAZGVsb2l0dGUuY2EuZGV2OndlbGNvbWUxMjM0');        
        
        request.setBody(requestBody_SAPInventory);
        
        system.debug('@@@@ Request: '+ request);
        system.debug('@@@@ Request body: '+ request.getBody());
        
        
        Http h = new Http();
        HttpResponse response = h.send(request);
        
        system.debug('@@@@ Response : '+ response);
        system.debug('@@@@ Response Body: '+ response.getBody());        
    }
        Public Void ICRTCallout_EMEAInventory(){
        HttpRequest request= new HttpRequest();
        request.setTimeout(60000);
        //='https://ps1w2.rt.informaticacloud.com/active-bpel/services/REST/000V28/trial1service';
        request.setEndpoint(sInvEMEA);
        request.setMethod('POST');
        
        request.setHeader('Content-type', 'application/json');
        request.setHeader('Authorization', 'Basic cGlkdWZvdXJAZGVsb2l0dGUuY2EuZGV2OndlbGNvbWUxMjM0');        
        
        request.setBody(requestBody_EMEAInventory);
        
        system.debug('@@@@ Request: '+ request);
        system.debug('@@@@ Request body: '+ request.getBody());
        
        
        Http h = new Http();
        HttpResponse response = h.send(request);
        
        system.debug('@@@@ Response : '+ response);
        system.debug('@@@@ Response Body: '+ response.getBody());        
        List<DTT_RestResponse_EMEA.ProductWrapper> retVal = new List<DTT_RestResponse_EMEA.ProductWrapper>();
            if (response.getBody()!= null)
            {
                response.getBody().replace('_void', 'void_x');
                retVal = (List<DTT_RestResponse_EMEA.ProductWrapper>)JSON.deserialize(responseBody_EMEA, List<DTT_RestResponse_EMEA.ProductWrapper>.class);
            }
          /*  for (DTT_RestResponse_EMEA.ProductWrapper test: retval)
            {     system.debug('@@@@ Response 1: ' + test.Plant);
            }*/
            

    }
    
    // Callout to ICRT webservice (for testing Ship to Account post service)
    Public Void ICRTCallout_NAShipTo(){
        HttpRequest request= new HttpRequest();
        request.setTimeout(60000);
        request.setEndpoint(sNAShipToUrl);
        request.setMethod('POST');
        
        request.setHeader('Content-type', 'application/json');
        request.setHeader('Authorization', 'Basic cGlkdWZvdXJAZGVsb2l0dGUuY2EuZGV2OndlbGNvbWUxMjM0');        
        
        request.setBody(requestBody_NAShipTo );
        
        system.debug('@@@@ Ship to Request: '+ request);
        system.debug('@@@@ Ship to Request body: '+ request.getBody());
        
        
        Http h = new Http();
        HttpResponse response = h.send(request);
        
        system.debug('@@@@ Ship to Response : '+ response);
        system.debug('@@@@ Ship to Response Body: '+ response.getBody());        
    }
    
    // Callout to ICRT webservice for EMEA Order Refresh
    Public Void ICRTCallout_EMEAOrderRefresh(){
        HttpRequest request= new HttpRequest();
        request.setTimeout(60000);
        request.setEndpoint(sOrderSAPEndpoint);
        request.setMethod('POST');
        
        request.setHeader('Content-type', 'application/json');
        request.setHeader('Authorization', 'Basic cGlkdWZvdXJAZGVsb2l0dGUuY2EuZGV2OndlbGNvbWUxMjM0');        
        
        request.setBody(requestBody_EMEAOrderRefresh );
        
        system.debug('@@@@ Ship to Request: '+ request);
        system.debug('@@@@ Ship to Request body: '+ request.getBody());
        
        
        Http h = new Http();
        HttpResponse response = h.send(request);
        
        system.debug('@@@@ Ship to Response : '+ response);
        system.debug('@@@@ Ship to Response Body: '+ response.getBody());        
    }

    
    
    // Callout to ICRT webservice (for testing Sold to Account post service)
    Public Void ICRTCallout_NASoldTo(){
        HttpRequest request= new HttpRequest();
        request.setTimeout(60000);
        request.setEndpoint(sNASoldToUrl);
        request.setMethod('POST');
        
        request.setHeader('Content-type', 'application/json');
        request.setHeader('Authorization', 'Basic cGlkdWZvdXJAZGVsb2l0dGUuY2EuZGV2OndlbGNvbWUxMjM0');        
        
        request.setBody(requestBody_NASoldTo );
        
        system.debug('@@@@ Sold To Request: '+ request);
        system.debug('@@@@ Sodl To Request body: '+ request.getBody());
        
        
        Http h = new Http();
        HttpResponse response = h.send(request);
        
        system.debug('@@@@ Sold to Response : '+ response);
        system.debug('@@@@ Sold to Response Body: '+ response.getBody());        
    }
    
    
    
    Public void NA_PDFtoSF(){
        HttpRequest request= new HttpRequest();
        request.setTimeout(60000);
        request.setEndpoint(sNAPdfServiceEndpoint);
        request.setMethod('POST');
        
        //request.setHeader('Content-type', 'application/json');
        request.setHeader('Authorization', 'Basic cGlkdWZvdXJAZGVsb2l0dGUuY2EuZGV2OndlbGNvbWUxMjM0');        
        
        //request.setBody(requestBody_NASoldTo );
        
        system.debug('@@@@ Sold To Request: '+ request);
        system.debug('@@@@ Sodl To Request body: '+ request.getBody());
        
        
        Http h = new Http();
        HttpResponse response = h.send(request);
        
        system.debug('@@@@ Sold to Response : '+ response);
        system.debug('@@@@ Sold to Response Body: '+ response.getBody());            
        
    }
}