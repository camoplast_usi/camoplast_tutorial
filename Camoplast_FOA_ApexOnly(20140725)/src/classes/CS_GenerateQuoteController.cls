// Class to support CS_GenerateQuote Page: Create Quote from Opportunity

Public class CS_GenerateQuoteController {

    Private Opportunity oOpp{get;set;}
    Public Quote oQuote{get;set;}
    Public String sEmailBody {get;set;}
    Public String sEndPointURL {get;set;}
    Public String sMessage{get;set;}
    Public String sQuoteId{get;set;}
    Public String sQuoteName{get;set;}
    
    public CS_GenerateQuoteController(ApexPages.StandardController controller) {
        oOpp = [SELECT Id,Name,AccountId,Pricebook2Id,Approval_Status__c,Quote_Status__c,Expiration_Date__c,Reason_for_Rejection__c,Description,
        		Quote_Prepared_For__c,Quote_Prepared_For__r.Phone, Quote_Prepared_For__r.Fax,Quote_Prepared_For__r.Email,Ship_To_Contact__c,
        		(SELECT Id,Description,Discount,PricebookEntryId,Quantity,UnitPrice FROM OpportunityLineItems)
        		FROM Opportunity WHERE Id = :controller.getId() ];
        		
        if(oOpp.Approval_Status__c == 'Required')
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Opportunity should be approved to generate a Quote'));
        
        
        list<Quote> lstQuote = [Select Id from Quote Where OpportunityId = :oOpp.Id and IsSyncing=true ];
        if(lstQuote!=null && lstQuote.size()>0)        
            oQuote = lstQuote[0];
    }
    
    // Create a Quote record and redirect to Opportunity Page
    Public Quote createQuote(){
        // Create a Quote record from Opportunity:
        Quote oQuoteRecord = new Quote(Name= oOpp.Name + '- Quote',OpportunityId=oOpp.Id, Pricebook2Id=oOpp.Pricebook2Id, Status=oOpp.Quote_Status__c,
        								ExpirationDate=oOpp.Expiration_Date__c, Reason_for_Rejection__c=oOpp.Reason_for_Rejection__c, Description=oOpp.Description,
        								Ship_To_Contact__c = oOpp.Ship_To_Contact__c, ContactId=oOpp.Quote_Prepared_For__c, Phone=oOpp.Quote_Prepared_For__r.Phone, 
        								Email=oOpp.Quote_Prepared_For__r.Email, Fax=oOpp.Quote_Prepared_For__r.Fax);
        insert oQuoteRecord;
        
        // Get Fresh Values from DB:
        oQuoteRecord = [SELECT Id,Name,ContactId FROM Quote WHERE Id =: oQuoteRecord.Id];
        
        // Create new QuoteLineItems:
        system.debug('%%'+'after quote creation: '+oOpp.OpportunityLineItems); 
        if(oOpp.OpportunityLineItems!=null && oOpp.OpportunityLineItems.size()>0){
            List<QuoteLineItem> lstQLI = new List<QuoteLineItem>();
            for(OpportunityLineItem oli : oOpp.OpportunityLineItems){
                QuoteLineItem qli = new QuoteLineItem();
                qli.QuoteId = oQuoteRecord.Id;
                qli.Description = oli.Description;
                qli.Discount = oli.Discount;
                qli.PricebookEntryId = oli.PricebookEntryId;
                qli.Quantity = oli.Quantity;
                qli.UnitPrice = oli.UnitPrice;
                lstQLI.add(qli);
            }
            insert lstQLI;
        }
        
        // Return Quote details to invoking method:
        return oQuoteRecord;
        
        //PageReference oRedirectPage = new PageReference('/'+oOpp.Id); 
        //oRedirectPage.setRedirect(true);
        //return oRedirectPage;        // We can directly redirect to Oppty Page
        //return null;
    }
    Public PageReference returnToOpportunity(){
        PageReference oRedirectPage = new PageReference('/'+oOpp.Id); 
        oRedirectPage.setRedirect(true);
        return oRedirectPage;
    }
    
    Public void Send(){
        oQuote = createQuote();    // Create a Quote when Send button is clicked
        if(oQuote!=null) {
            sQuoteName = oQuote.Name;
            sQuoteId =  String.valueOf(oQuote.Id).left(15);
            String sQuoteContactId = String.valueOf(oQuote.ContactId).left(15);
            
            String EMAIL_TEMPLATE_NAME='New_Quote',CONGA_QUERY_NAME='New Quote',CONGA_TEMPLATE_NAME='New Quote';
            String sCongaQueryId,sCongaTemplateId,sEmailTemplateId;
            // Get the Ids for preparing the Conga Outbound URL:
            // Get the Query ID:
            List<APXTConga4__Conga_Merge_Query__c> lstCongaQueries = [SELECT Id FROM APXTConga4__Conga_Merge_Query__c WHERE APXTConga4__Name__c= :CONGA_QUERY_NAME limit 1];
            if(lstCongaQueries!=null && lstCongaQueries.size()>0)
        		sCongaQueryId = String.valueOf(lstCongaQueries[0].Id).left(15);
        		
    		// Get the 'New Quote' Template Id:
    		List<APXTConga4__Conga_Template__c> lstCongaTemplates = [SELECT Id FROM APXTConga4__Conga_Template__c WHERE APXTConga4__Name__c = :CONGA_TEMPLATE_NAME limit 1];
    		if(lstCongaTemplates!=null && lstCongaTemplates.size()>0)
	    		sCongaTemplateId = String.valueOf(lstCongaTemplates[0].Id).left(15);
	    		
    		// Get the Email Template Details:
    		List<EmailTemplate> templates = [SELECT Id,Name FROM Emailtemplate WHERE DeveloperName = :EMAIL_TEMPLATE_NAME AND isActive=true LIMIT 1];
        	//System.assert(!(templates == null || templates.size()==0),'No Email Templates found while sending Conga outbound email');
        	if(templates!=null && templates.size()>0)
        		sEmailTemplateId = String.valueOf(templates[0].Id).left(15);
    		
            // Make a outbound call to Conga:
            //String sEndPointUrl = 'https://www.appextremes.com/apps/Conga/PointMerge.aspx';
            sEndPointUrl = 'https://composer.congamerge.com/composer8/index.html';
            
            sEndPointUrl += '?sessionId=' + UserInfo.GetSessionId();
            String sOrgId = UserInfo.getOrganizationId();
            sEndPointUrl += '&serverUrl='+ URL.getSalesforceBaseUrl().toExternalForm() + '/services/Soap/u/28.0/' + sOrgId.left(15);
            sEndPointUrl += '&Id='+ sQuoteId;
            
            sEndPointUrl += '&TemplateId='+ sCongaTemplateId;
            sEndPointUrl += '&EmailTemplateId=' + sEmailTemplateId;
            sEndPointUrl += '&EmailToId=' + sQuoteContactId;
            sEndPointUrl += '&EmailAdditionalTo=jarath@deloitte.com';
            //sEndPointUrl += '&LG3=2&LGAttachOption=0&AttachmentParentId=' + sQuoteId;
            sEndPointUrl += '&QueryId=' + sCongaQueryId + '?pv0=' + sQuoteId;
            sEndPointUrl += '&DefaultPDF=1&DS7=12&APIMode=12';
            
            /*
            String parameters = '&TemplateId=' + templateId.left(15)
                        +  congaUrlSegment
                        + '&OFN='+ EncodingUtil.urlEncode(cipherCloudFilePrefix + templateName,'UTF-8')
                        + '&EmailTemplateId=' + ((String)(templates[0].Id)).left(15)
                        // EmailToId parameter is mandatory (Lead/Contact/User Id)
                        + '&EmailToId=' + sEmailToId.left(15)
                        + '&EmailAdditionalTo='+ (sendToCurrentUser ? sCurrentUserEmail : ew.participant.Email__c)
                        + '&BML=Processing...&DefaultPDF=0&DS7=12';
                        //+ '&EmailToId=' + (String)(UserInfo.getUserId()).left(15)
            
            */
            // Call future callout method :
            makeCallout(sEndPointUrl);
            sMessage = 'Callout Request Sent';
            
            // Send an outbound Email:
            sendEmailToContact(sEmailBody);
        }
    }
    
    // Future method to Conga
    @Future(callout=true)
    public static void makeCallout(String congaURL){
            Http oHttp = new Http();          
            HttpRequest oHttpReq = new HttpRequest();            
            oHttpReq.setEndpoint(congaURL);
            oHttpReq.setMethod('GET');
            oHttpReq.setTimeout(60000);
            HttpResponse oHttpRes;
            oHttpRes = oHttp.send(oHttpReq);

            //sMessage = 'Callout Request Sent; Status is: '+ STring.valueOf(oHttpRes);    
    }
    
    // Send an Email via Apex after the Conga Callout is made:
    Public Static void sendEmailToContact(String sEmailBody){
    
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        // Strings to hold the email addresses to which you are sending the email.        
        List<String> toAddresses = new List<String>();// = new String[] {'jarath@deloitte.com'}; 
        toAddresses.add([SELECT Email FROM User Where Id=:UserInfo.GetUserId()][0].Email);
        mail.setToAddresses(toAddresses);
        
        // Specify the subject line for your email address.
        mail.setSubject('Quote Generated');

        // Specify the text content of the email.
        mail.setPlainTextBody(sEmailBody);
        //mail.setHtmlBody('Your case:<b> ' + case.Id +' </b>has been created.<p>'+'To view your case <a href=https://na1.salesforce.com/'+case.Id+'>click here.</a>');

        // Send the email
        //Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });   
    }
}