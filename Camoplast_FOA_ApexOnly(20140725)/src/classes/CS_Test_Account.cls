/*********************************************************************************
Class Name      : CS_TestClass_Account
Description     : This class is used for Test Classes for Functionalities on Account object
Created By      : Divya A N
Created Date    : 14-Jul-14
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Divya A N                  14-Jul-14               Initial Version
*********************************************************************************/
@isTest
    public class CS_Test_Account{
 /*********************************************************************************
 Method Name    : test_updateAssignedPriceBook
 Description    : Test method for the update Assigned Price Book trigger
 Return Type    : void
 Parameter      : nil                 
 *********************************************************************************/
    
        static testmethod void test_updateAssignedPriceBook(){
                 Pricebook2 pb=new Pricebook2();
                     pb.Name= 'ABCDE';
                     pb.Construction_Discount_Class__c= 'A';
                     pb.MH_Discount_Class__c= 'D';
                     pb.Track_Discount_Class__c  = 'R';
                 insert pb;
           test.starttest();  
            RecordType rec = [SELECT Id FROM RecordType where Name = 'NA Ship To'];   
                 Account acc= new Account();
                     acc.RecordType = rec;
                     acc.Name = 'Test Account';
                     acc.CurrencyIsoCode = 'EUR';            
                     acc.Con_Discount_Class__c = 'A';
                     acc.MH_Discount_Class__c =  'D';
                     acc.Tracks_Discount_Class__c = 'R';
                     
                 insert acc;
            Account accnt = [SELECT Assigned_Price_Book__c FROM Account WHERE ID = :acc.ID limit 1];  
             
               
            system.Assert(accnt.Assigned_Price_Book__c== pb.ID);  
               
                     accnt.Con_Discount_Class__c = 'B';  
                 update accnt ;
            accnt = [SELECT Assigned_Price_Book__c FROM Account WHERE ID = :accnt.ID limit 1];
              
            system.Assert(accnt.Assigned_Price_Book__c==null); 
                    
            test.stoptest();              
              }
          
     static testmethod void test_Account_PostControl(){ 
      Pricebook2 pb=new Pricebook2();
                     pb.Name= 'ABCDE';
                     pb.Construction_Discount_Class__c= 'A';
                     pb.MH_Discount_Class__c= 'D';
                     pb.Track_Discount_Class__c  = 'R';
                 insert pb;
           test.starttest();  
            RecordType rec = [SELECT Id FROM RecordType where Name = 'NA Ship To'];   
                 Account oAcc= new Account();
                     oAcc.RecordType = rec;
                     oAcc.Name = 'Test Account';
                     oAcc.CurrencyIsoCode = 'EUR';            
                     oAcc.Con_Discount_Class__c = 'A';
                     oAcc.MH_Discount_Class__c =  'D';
                     oAcc.Tracks_Discount_Class__c = 'R';
                 insert oAcc;
        PageReference pageRef = Page.CS_AccountStatusBanner;
        pageRef.getParameters().put('Id',oAcc.Id);
        Test.setCurrentPageReference(pageRef);
        
        oAcc = [SELECT RecordTypeId FROM Account WHERE ID =: oAcc.Id LIMIT 1];
        CS_Account_PostControl AccCon = new CS_Account_PostControl(new ApexPages.StandardController(oAcc));
        
        AccCon.Post();
        oAcc = [SELECT Last_Sync_Datetime_with_ERP__c,lastmodifieddate,Epicor_External_Id__c ,SAP_External_Id__c,Posted__c,Is_Synced_From_SFDC__c  FROM Account WHERE id =: oAcc.Id Limit 1];
        
        system.assert(oAcc.Posted__c== true);
        system.assert(oAcc.Is_Synced_From_SFDC__c  == true);
        
        oAcc.Con_Discount_Class__c  = 'B';
            update oAcc;
            oAcc = [SELECT Last_Sync_Datetime_with_ERP__c,lastmodifieddate,Epicor_External_Id__c ,SAP_External_Id__c,Posted__c,Is_Synced_From_SFDC__c  FROM Account WHERE id =: oAcc.Id Limit 1];
            system.assert(oAcc.Posted__c== true);
            system.assert(oAcc.Last_Sync_Datetime_with_ERP__c <= oAcc.lastmodifieddate);
        
        AccCon.Sync();
            oAcc = [SELECT Last_Sync_Datetime_with_ERP__c,lastmodifieddate,Epicor_External_Id__c ,SAP_External_Id__c,Posted__c,Is_Synced_From_SFDC__c  FROM Account WHERE id =: oAcc.Id Limit 1];
        
        system.assert(oAcc.Posted__c== true);
        system.assert(oAcc.Is_Synced_From_SFDC__c  == true);
        
        }
    }