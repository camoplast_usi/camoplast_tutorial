/****************************************************************************************************************************************** 
* Class Name   : Risk_Issues_BI
* Description  : Puts the Identified By to the creator
* Created By   : Deloitte Consulting
* 
*****************************************************************************************************************************************/

trigger Risk_Issues_BI on Risks_Issues__c (before insert) {
    if (trigger.isBefore && trigger.isInsert) {                 //before insert 
        for(Risks_Issues__c oRisk : trigger.New){                //for all new records
            if(oRisk.Identified_By__c == null)                  //if there is no 'risk identified by' value
                oRisk.Identified_By__c = UserInfo.getUserId();  //set identified to the current user
        }       
    }

}