/*********************************************************************************
Class Name      : CS_Trigger_Account
Description     : This class will be invoked by the triggers on Accounts 
Created By      : Divya A N 
Created Date    : 14-Jul-14
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Divya A N                  14-Jul-14              Initial Version
Divya A N                  28-Jul-2014            Defaulting the Discount Classes
Pierre D                   01-Aug-2014            Moving the default values at the fields level
*********************************************************************************/

public class CS_Trigger_Account{
 
     /*********************************************************************************
     Method Name    : updateAssignedPriceBook
     Description    : To update the Assigned Price Book field when Account is created/updated
     Return Type    : void
     Parameter      : 1. lstAccount: List of new Account
     *********************************************************************************/

    public static void updateAssignedPriceBook(List<Account> lstAccount)
    {
        List<Pricebook2> lstPriceBook = [SELECT id,Construction_Discount_Class__c,MH_Discount_Class__c, Track_Discount_Class__c FROM Pricebook2];
     
        for(Account oAcc: lstAccount){
            //The Pricebook Selection logic should work only for NA Accounts
            if(oAcc.Record_Type_Name__c == 'NA_Ship_To' || oAcc.Record_Type_Name__c  == 'NA_Sold_To') {
           
                if(oAcc.Con_Discount_Class__c != null && oAcc.MH_Discount_Class__c != null  && true) //oAcc.Track_Discount_Class__c != null)
                {
                    Boolean bPriceBookFound = false;
                    for(Pricebook2 oPB  : lstPriceBook)
                    {
                        if(oAcc.Con_Discount_Class__c == oPB.Construction_Discount_Class__c && oAcc.MH_Discount_Class__c == oPB.MH_Discount_Class__c && oAcc.Tracks_Discount_Class__c == oPB.Track_Discount_Class__c){
                            oAcc.Assigned_Price_Book__c = oPB.Id ;
                            bPriceBookFound = true;
                            break;
                        }
                    }
                    if(! bPriceBookFound)    oAcc.Assigned_Price_Book__c = null;
                }
            }
        }   
    
    }
    public static void createJunctionObjectRecord(List<Account> lstAccount) 
    {
        list<Acc_Junction__c> lstAccJunction = new list<Acc_Junction__c>();
            for(Account oAcc: lstAccount){
            if(oAcc.Sold_To_Account_ID__c != null)
            {
             Acc_Junction__c accJunction= new Acc_Junction__c();
                accJunction.Ship_To__c = oAcc.Id;
                accJunction.Sold_To__c = oAcc.Sold_To_Account_ID__c;
                accJunction.CurrencyIsoCode = oAcc.CurrencyIsoCode;
            lstAccJunction.add(accJunction);
            }
        }
        insert lstAccJunction;
    }
        
}