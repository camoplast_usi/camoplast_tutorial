trigger DTT_Task_Short on Task (before insert, before update) {
	
	for (Task t : trigger.new)
	{
     	if (t.Description != null)
			t.Short_Description__c = t.Description.length() > 125 ? t.Description.substring(0, 125) + '...' : t.Description;
	}
}