/*********************************************************************************
Class Name      : opportunityProductEntryExtension
Description     : This is class is the extension controller for page opportunityProductEntry.page
Created By      : Pierre Dufour
Created Date    : 2014-07-17
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Pierre Dufour               2014-07-17             Quick fix the search
Laxmy                       2014-07-23             Getting Product Names for Autocomplete. 
Divya A N                   2014-07-18             Dynamic of Total Sales and Total Weight
Divya A N                   2014-07-24             Location picklist
Laxmy                       2014-07-28             Getting Quote price book for searching Products.
Maryam                      2014-07-31             Added discounts, freight and weight deductions implemented
Jayadev Rath                2014-07-31             Added InventoryDetails() method for filling the inventory specific fields   
Maryam                      2014-08-11             Added volume calculations
Laxmy                       2014-08-11             Added default value to the Locations pick list field. 
Maryam                      2014-08-18             Added methods weightConversion, recordTypeWeightLabel and updated ApplyDiscount()
                                                   and UpdatePriceandWeight() to display Weight/Volume ratio and calculate totals based on 
                                                   unit of measure and region
*********************************************************************************/



public with sharing class opportunityProductEntryExtension {

    public Opportunity theOpp {get;set;}
    public Opportunity theOppParent{get;set;}
    public String searchString {get;set;}
    public opportunityLineItem[] shoppingCart {get;set;}
    public priceBookEntry[] AvailableProducts {get;set;}
    public Pricebook2 theBook {get;set;}    
    public String toSelect {get; set;}
    public String toUnselect {get; set;}
    public Decimal Totalsales {get; set;}
    public String userLocale{get;set;}
    public String weightLabel{get;set;}
    public String recordType{get;set;}
    public String totalWeightLabel {get;set;}
    public String weightMetric {get;set;}
    public Decimal freightDiscConverted {get; set;}
    public Boolean removeProduct {get;set;}
    public Decimal prodweight{get; set;}
    public Decimal prodWeightConverted {get;set;}
    public Decimal addedDiscountPercent{get; set;}
    public Decimal accountDiscountWeight{get; set;}
    public Decimal addeddiscount{get; set;}
    public Decimal totalVolume{get; set;}
    public Boolean freightdisc {get; set;}
    public Boolean weightdisc {get; set;}
    public Boolean overLimit {get;set;}
    public Boolean multipleCurrencies {get; set;}
    private Boolean forcePricebookSelection = false;
    private Boolean pageLoad = true;
    private opportunityLineItem[] forDeletion = new opportunityLineItem[]{};
    Public String sSelectedLocation {get;set;}
    Public String sSelectedProductLocation {get;set;}
    Public Boolean bGetDataFromExtSystem {get;set;}
    


    public opportunityProductEntryExtension(ApexPages.StandardController controller) {
         
         
        
        // Need to know if org has multiple currencies enabled
        multipleCurrencies = UserInfo.isMultiCurrencyOrganization();
        // For first time search the inventory data should be fetched from external system
        bGetDataFromExtSystem = true;

        // Get information about the Opportunity being worked on
       
        if(multipleCurrencies)
            theOpp = database.query('select Id, Pricebook2Id, Added_Discount__c,Freight_Discount_Applied__c, Account_Wt_for_Discount__c,Account_Percent_Wt_Discount__c,Account_Wt_for_Discount_Unit__c, Weight_Discount_Applied__c, Amount,Total_Price__c,Pricebook2.Name,CS_Quote_Price_Book__c, CurrencyIsoCode,Ship_To_Account__r.Locations__r.Name  from Opportunity where Id = \'' + controller.getRecord().Id + '\' limit 1');
           
        else
            theOpp = [select Id, Pricebook2Id, Added_Discount__c, Freight_Discount_Applied__c,Weight_Discount_Applied__c, Account_Wt_for_Discount__c,Account_Percent_Wt_Discount__c,Account_Wt_for_Discount_Unit__c,Amount,Total_Price__c,PriceBook2.Name,Ship_To_Account__c,CS_Quote_Price_Book__c,Ship_To_Account__r.Locations__r.Name from Opportunity where Id = :controller.getRecord().Id limit 1];
            ID qID =theOpp.CS_Quote_Price_Book__c; 
      
        // If products were previously selected need to put them in the "selected products" section to start with
        shoppingCart = [select Id, Opportunity.Added_Discount__c, Quantity, Sales_Price__c,Volume__c,Product2.Unit_of_Measure__c,PriceBookEntry.Product2.Unit_of_Measure__c,PriceBookEntry.Product2.Volume__c,TotalPrice,UnitPrice, ListPrice, Notes__c, Description, Weight__c, PriceBookEntry.UnitPrice,PriceBookEntryId, PriceBookEntry.Name, Discount_Reason__c,PriceBookEntry.IsActive, PriceBookEntry.Product2Id, PriceBookEntry.Product2.Name,PriceBookEntry.Product2.Location__c, PriceBookEntry.Product2.Family, PriceBookEntry.Product2.Weight__c , PriceBookEntry.Product2.ProductCode, PriceBookEntry.Product2.Global_PID__c,Discount, PriceBookEntry.PriceBook2Id, Wt_Vol_Ratio__c, Quantity__c from opportunityLineItem where OpportunityId=:theOpp.Id ];
        
        
        
        //Maryam Aug 15 - Get Record type
        Map<ID, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Opportunity.getRecordTypeInfosById();
        if(rtMap.get(theOpp.RecordTypeId) !=null)
            recordType = rtMap.get(theOpp.RecordTypeId).getName();
        recordTypeWeightLabel();
        initializeSalesPrice(); 
        
        //Update Total Price & Weight
        updatePriceAndWeight();
        // Check if Opp has a pricebook associated yet
       
        
        if(theOpp.Pricebook2Id == null){
            Pricebook2[] activepbs = [select Id, Name from Pricebook2 where isActive = true limit 2];
            if(activepbs.size() == 2){
                forcePricebookSelection = true;
                theBook = new Pricebook2();
            }
            else{
                theBook = activepbs[0];
            }
        }
        else if(qID!=null)
        
        {              
        
        Pricebook2[] quotepbs=[select Id,Name from Pricebook2 where Id=:qID limit 1];
        theBook = quotepbs[0];
        }
        else
        {
        theBook = theOpp.Pricebook2;
        }
        
        // Pierre Dufour - 2014-07-17 - No product populated on load, just on search.  
        //if(!forcePricebookSelection)
        //    updateAvailableList();
    }
    
    // this is the 'action' method on the page
    public PageReference priceBookCheck(){
    
        // if the user needs to select a pricebook before we proceed we send them to standard pricebook selection screen
        if(forcePricebookSelection){        
            return changePricebook();
        }
        else{
        
            //if there is only one active pricebook we go with it and save the opp
            if(theOpp.pricebook2Id != theBook.Id){
                try{
                    theOpp.Pricebook2Id = theBook.Id;
                    update(theOpp);
                }
                catch(Exception e){
                    ApexPages.addMessages(e);
                }
            }
            
            return null;
        }
    }
       
    public String getChosenCurrency(){
    
        if(multipleCurrencies)
            return (String)theOpp.get('CurrencyIsoCode');
        else
            return '';
    }
    



    public void updateAvailableList() {
        //system.assert(false, 'Value is: '+sSelectedLocation);
    system.debug('sSelectedLocation $$$%% '+ sSelectedLocation);
        // We dynamically build a query string and exclude items already in the shopping cart
        String qString = 'select Id, Pricebook2Id, IsActive, UnitPrice, Product2.Unit_of_Measure__c,Product2.Volume__c,Product2.Product_Line__c, Product2.Brand__c, Product2.ProductCode, Product2.Name, Product2.Global_PID__c ,Product2.Family, Product2.IsActive, Product2.Weight__c, Product2.Description, Product2.ABC_Classification__c, Product2.Location__c, Product2.On_hand__c, PriceBookEntry.Product2.Reserved__c, PriceBookEntry.Product2.Ordered__c , PriceBookEntry.Product2.Transit__c , PriceBookEntry.Product2.AvailableInventory__c from PricebookEntry where IsActive=true and Pricebook2Id = \'' + theBook.Id + '\'';
        
        if(multipleCurrencies)
            qstring += ' and CurrencyIsoCode = \'' + theOpp.get('currencyIsoCode') + '\'';
        
        // note that we are looking for the search string entered by the user in the name OR description
        // modify this to search other fields if desired
        // Pierre Dufour - 2014-07-17 - Fix the search (3)
        if(searchString!=null){
            qString += string.format('and (Product2.Name like \'\'%{0}%\'\' or Product2.ProductCode = \'\'{0}\'\'  or Product2.Global_PID__c = \'\'{0}\'\')', new string[]{searchString});
    
        }
        
        
        Set<Id> selectedEntries = new Set<Id>();
        prodweight = 0.0;
        Totalsales =0;
        
        for(opportunityLineItem d:shoppingCart){
            selectedEntries.add(d.PricebookEntryId);
            updatePriceAndWeight();
            
            //lstprice= d.ListPrice;
           /* if((d.Weight__c != null) && (d.Weight__c != ''))
            {
                prodweight = prodweight + Decimal.valueof(d.Weight__c); 
              
            }
            if(d.UnitPrice != null && d.UnitPrice != 0)
            {
                Totalsales = Totalsales + d.UnitPrice; 
            }*/         
        }
        
        if(selectedEntries.size()>0){
            String tempFilter = ' and Id not in (';
            for(Id i : selectedEntries){
                tempFilter+= '\'' + (String)i + '\',';
            }
            String extraFilter = tempFilter.substring(0,tempFilter.length()-1);
            extraFilter+= ')';
            
            qString+= extraFilter;
        }
        
        qString+= ' order by Product2.Name';
        qString+= ' limit 101';
        
        system.debug('qString:' +qString);        
        AvailableProducts = database.query(qString);
        
        // We only display up to 100 results... if there are more than we let the user know (see vf page)
        if(AvailableProducts.size()==101){
            AvailableProducts.remove(100);
            overLimit = true;
        }
        else{
            overLimit=false;
        }
        system.debug('sSelectedLocation $$$%% '+ sSelectedLocation);
        
        CS_REST_TestingController.bGetDataFromExtSystem = bGetDataFromExtSystem;
        // Fill the inventory related columns (from the Callout)
        //AvailableProducts = CS_REST_TestingController.UpdateInventoryDetails(AvailableProducts,sSelectedLocation);
        
        //Maryam recalculate discount if product removed
        if(removeProduct==true)
        {    removeProduct=false;
             applyDiscount();
        }
       
        
    }
    
    public void addToShoppingCart(){
    
        // This function runs when a user hits "select" button next to a product
    
        for(PricebookEntry d : AvailableProducts){
            if((String)d.Id==toSelect  && d.Product2.Location__c == sSelectedProductLocation){
            system.debug(d.Product2.Location__c +'%%%%'+ sSelectedProductLocation);
                shoppingCart.add(new opportunityLineItem(OpportunityId=theOpp.Id, PriceBookEntry=d, PriceBookEntryId=d.Id, UnitPrice=d.UnitPrice,Sales_Price__c= d.UnitPrice,OpportunityHistory__c=d.Product2Id));
               
                break;
            }
        }
        
        // While adding to Shopping Cart no need to fetch the inventory data from external system. 
        bGetDataFromExtSystem = false;
        updateAvailableList();  
    }
    //LMK: 7/23/2014 This is for getting the list of Product Names available.
    
   public list<Product2>getProductlist 
    
    {get
        {
            
            ID PBId=theOpp.pricebook2Id;
         
            return[select name from product2 where Id in (select Product2Id from PricebookEntry where Pricebook2Id=:PBId)limit 999];
        }
    }





    
    public PageReference details(){
    
        // This function runs when a user hits "select" button next to a product
    
        
        return new PageReference('/'+AvailableProducts.get(0).id);  
    }
    
    public PageReference SaveQuote(){
    
        // This function runs when a user hits "select" button next to a product
    
        
        return null;  
    }
    

    public PageReference removeFromShoppingCart(){
    
        // Maryam flag to indicate removal has taken place, so discount to be recalculated
        removeProduct=true;
        // This function runs when a user hits "remove" on an item in the "Selected Products" section
        
        Integer count = 0;
    
        for(opportunityLineItem d : shoppingCart){
            if((String)d.PriceBookEntryId==toUnselect){
            
                if(d.Id!=null)
                    forDeletion.add(d);
            
                shoppingCart.remove(count);
                break;
            }
            count++;
        }
        
        updateAvailableList();
        
        return null;
    }
    
    public PageReference onSave(){
    
        // If previously selected products are now removed, we need to delete them
        if(forDeletion.size()>0)
            delete(forDeletion);
        
        //To Apply Entered Discounts on Save if Apply Discount is not clicked by the user manually
        applyDiscount();
        
           
        // Previously selected products may have new quantities and amounts, and we may have new products listed, so we use here
        try{
            if(shoppingCart.size()>0)
                {    upsert(shoppingCart);  
                                     
                }
            upsert(theOpp);
             
                
        }
        catch(Exception e){
            ApexPages.addMessages(e);
            return null;
        }  
           
        // After save return the user to the Opportunity
        return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));
    }
    
    public PageReference onCancel(){
 
        // If user hits cancel we commit no changes and return them to the Opportunity   
        return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));
    }
    
    //Maryam update weight to kilograms for EMEA and Lbs for North America records
    public Decimal weightConversion(Decimal weight, String unit){
        if (unit== 'Lbs'){
          
            return weight/0.453592;
        }
        else 
        {    
            return weight*0.453592;
        }
    }
    //Maryam update weight label EMEA users
    public void recordTypeWeightLabel(){
        if (recordType=='North America'){
            totalWeightLabel = 'Total Weight (Lbs):';
            freightDiscConverted = Integer.valueOf(Label.CS_Freight_Discount);
            addedDiscountPercent = theOpp.Account_Percent_Wt_Discount__c.intValue();
            weightLabel = String.valueOf(theOpp.Account_Percent_Wt_Discount__c.intValue()) + '% Weight Discount';
            if(theOpp.Account_Wt_for_Discount_Unit__c == 'KG' )
                accountDiscountWeight = weightConversion(theOpp.Account_Wt_for_Discount__c, 'Lbs');
            else
                accountDiscountWeight = theOpp.Account_Wt_for_Discount__c;
           
        }
        else
        {   weightLabel = Label.CS_Percent_Weight_Discount + '% Weight Discount';
            addedDiscountPercent = Integer.valueOf(Label.CS_Percent_Weight_Discount);
            totalWeightLabel = 'Total Weight (KG):';
            freightDiscConverted = weightConversion(Integer.valueOf(Label.CS_Freight_Discount),'KG');
            accountDiscountWeight = weightConversion(Integer.valueOf(Label.CS_Weight_Discount), 'KG');
           
        }        
    }

    
    //Divya-Action for Apply Discount button
    public PageReference applyDiscount(){
    
    Totalsales =0.0;
    totalVolume = 0.0;
        for(OpportunityLineItem oli : shoppingCart) { 
            oli.Quantity = oli.Quantity__c;       
            if(oli.Discount != null && oli.Discount>= 0 && oli.Discount <=100)
               oli.Sales_Price__c= oli.PriceBookEntry.UnitPrice - (oli.PriceBookEntry.UnitPrice  * (oli.Discount/100));
         
            else if(oli.Discount == null && (oli.Sales_Price__c!= oli.PriceBookEntry.UnitPrice)){
                oli.Sales_Price__c= oli.PriceBookEntry.UnitPrice;
                
            }
            else if(oli.Discount < 0 || oli.Discount >100) {
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Discount can not be negative or greater than 100%.'));
                break;
            }              
             
            Totalsales = Totalsales + (oli.Sales_Price__c* oli.Quantity); 
            //Maryam - update weight volume ratio
            if(oLi.PriceBookEntry.Product2.Volume__c!= null && oLi.Quantity!= null&& oLi.PriceBookEntry.Product2.Volume__c>0)
            {   if(oLi.PriceBookEntry.Product2.Weight__c!=null&&oLi.PriceBookEntry.Product2.Unit_of_Measure__c!=null&&oLi.PriceBookEntry.Product2.Unit_of_Measure__c!='')
                    oli.Wt_Vol_Ratio__c = String.valueOf((Decimal.valueOf(oli.PriceBookEntry.Product2.Weight__c)* oLi.Quantity).intValue()) + oli.PriceBookEntry.Product2.Unit_of_Measure__c+ '/' + String.valueOf((oLi.PriceBookEntry.Product2.Volume__c*oLi.Quantity).intValue())+'m3';
               // totalVolume = totalVolume + (oli.PriceBookEntry.Product2.Volume__c * oli.Quantity); 
            }        
        }
        //Maryam - Refreshing weight and volume before calculation added discount
        prodweight =0.0;
        totalVolume =0.0;
        for(OpportunityLineItem oLi : shoppingCart) {                     
            if(oLi.PricebookEntry.Product2.Weight__c!= null && oLi.PricebookEntry.Product2.Weight__c != ''&&oLi.PriceBookEntry.Product2.Unit_of_Measure__c!='' &&oLi.PriceBookEntry.Product2.Unit_of_Measure__c!=null)
            {   if(oLi.Quantity!=null){
                    if(recordType == 'North America'){
                         if(oli.PriceBookEntry.Product2.Unit_of_Measure__c =='KG')
                             prodWeightConverted =   weightConversion(Decimal.Valueof(oLi.PricebookEntry.Product2.Weight__c),'Lbs');
                         else 
                             prodWeightConverted = Decimal.Valueof(oLi.PricebookEntry.Product2.Weight__c);
                         prodweight = prodweight + (prodWeightConverted * oLi.Quantity); 
                    }
                    else if(recordType == 'EMEA'){
                         if(oli.PriceBookEntry.Product2.Unit_of_Measure__c =='Lbs')
                               prodWeightConverted =   weightConversion(Decimal.Valueof(oLi.PricebookEntry.Product2.Weight__c),'KG');
                         else 
                               prodWeightConverted = Decimal.Valueof(oLi.PricebookEntry.Product2.Weight__c);
                         prodweight = prodweight + (prodWeightConverted * oLi.Quantity); 
                    }
                 }
            }  
            else
                 ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Warning,'Product ' +oLi.PriceBookEntry.Product2.Name + ' does not have weight/unit of measure information.'));
          
            if(oLi.PriceBookEntry.Product2.Volume__c!= null&&oLi.PriceBookEntry.Product2.Volume__c>0)
            {   if(oLi.Quantity!= null)
                    totalVolume= totalVolume + (oLi.PriceBookEntry.Product2.Volume__c * oLi.Quantity); 
            }  
            else
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Warning,'Product ' +oLi.PriceBookEntry.Product2.Name + ' does not have volume information.'));
          
          
            
        }
        
        if(theOpp.Added_Discount__c< 0 || theOpp.Added_Discount__c >100) {
               ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Discount can not be negative or greater than 100%.'));
               
        } 
        //Maryam - Applying added discount, freight and weight deductions
           
        else if(theOpp.Added_Discount__c != null)        
        {    if(prodweight > freightDiscConverted )
             {    theOpp.Freight_Charges__c = 0; 
                  theOpp.Freight_Discount_Applied__c= true;             
             }                  
             else
                 theOpp.Freight_Discount_Applied__c= false; 
             if(prodweight > accountDiscountWeight)
             {                        
                    if(theOpp.Weight_Discount_Applied__c== false || theOpp.Weight_Discount_Applied__c==null)
                    {   Totalsales = Totalsales  - (Totalsales * (theOpp.Added_Discount__c + addedDiscountPercent )/100);      
                        theOpp.Added_Discount__c = theOpp.Added_Discount__c + addedDiscountPercent;
                        theOpp.Weight_Discount_Applied__c= true;
                        
                    }
                    else 
                        Totalsales = Totalsales  - (Totalsales * (theOpp.Added_Discount__c)/100);                            
             }            
             else 
             {    if(theOpp.Weight_Discount_Applied__c== true)
                  {    
                       if(theOpp.Added_Discount__c >=addedDiscountPercent)
                       {     Totalsales = Totalsales  - (Totalsales * (theOpp.Added_Discount__c-addedDiscountPercent )/ 100);
                             theOpp.Added_Discount__c = theOpp.Added_Discount__c-addedDiscountPercent ;
                       }                             
                       theOpp.Weight_Discount_Applied__c=false;
                                               
                  }
                  else
                  {    Totalsales = Totalsales  - (Totalsales * (theOpp.Added_Discount__c/ 100));
                       
                  }
             } 

        }
        //Maryam added to update added discounts
        theOpp.Total_Price__c=Totalsales;
        totalVolume = totalVolume.setScale(2);
        prodweight = prodweight.setScale(2);
        return null;
    }
    //Maryam Initialize Sales Price
    public PageReference initializeSalesPrice(){
        
         for(OpportunityLineItem oLi : shoppingCart) { 
                
                if(oLi.Sales_Price__c== null){
                    oLi.Sales_Price__c= oLi.PriceBookEntry.UnitPrice; 
                   
                }            
          }
          return null;
    }
    //Divya:Update Price & Weight
    public PageReference updatePriceAndWeight(){
        
                Totalsales=0.0;
                prodweight=0.0;
                totalVolume = 0.0;
        for(OpportunityLineItem oLi : shoppingCart) { 
            
            if(oLi.UnitPrice!= null && oLi.UnitPrice!= 0 && oLi.Quantity!= null){
               Totalsales = Totalsales + (oLi.UnitPrice* oLi.Quantity); 
               
            } 
           
            if((oLi.PricebookEntry.Product2.Weight__c!= null) && (oLi.PricebookEntry.Product2.Weight__c != '')&& oLi.PriceBookEntry.Product2.Unit_of_Measure__c!=null && oLi.PriceBookEntry.Product2.Unit_of_Measure__c!='')
            {    if(oLi.Quantity!=null){
                     if(recordType == 'North America'){
                         if(oli.PriceBookEntry.Product2.Unit_of_Measure__c =='KG')
                               prodWeightConverted =   weightConversion(Decimal.Valueof(oLi.PricebookEntry.Product2.Weight__c),'Lbs');
                         else 
                             prodWeightConverted = Decimal.Valueof(oLi.PricebookEntry.Product2.Weight__c);
                         prodweight = prodweight + (prodWeightConverted * oLi.Quantity); 
                    }
                    else if(recordType =='EMEA'){
                         if(oli.PriceBookEntry.Product2.Unit_of_Measure__c =='Lbs')
                               prodWeightConverted =   weightConversion(Decimal.Valueof(oLi.PricebookEntry.Product2.Weight__c),'KG');
                         else 
                               prodWeightConverted = Decimal.Valueof(oLi.PricebookEntry.Product2.Weight__c);
                         prodweight = prodweight + (prodWeightConverted * oLi.Quantity);  
                    }
                }
                
            }   
            else
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Warning,'Product ' +oLi.PriceBookEntry.Product2.Name + ' does not have weight/unit of measure information.'));
                
            //Maryam added volume calculation
            if(oLi.PriceBookEntry.Product2.Volume__c!= null && oLi.PriceBookEntry.Product2.Volume__c>0)
            {  if(oLi.Quantity!=null&&oLi.PricebookEntry.Product2.Weight__c!=null&&oLi.PricebookEntry.Product2.Unit_of_Measure__c!=null && oLi.PricebookEntry.Product2.Unit_of_Measure__c!=''){
                   oli.Wt_Vol_Ratio__c = String.valueOf((Decimal.valueOf(oli.PriceBookEntry.Product2.Weight__c)* oLi.Quantity).intValue()) + oli.PriceBookEntry.Product2.Unit_of_Measure__c+'/' + String.valueOf((oLi.PriceBookEntry.Product2.Volume__c*oLi.Quantity).intValue())+'m3';
                   totalVolume = totalVolume + (oli.PricebookEntry.Product2.Volume__c * oli.Quantity); 
               }
            }
            else
               ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Warning,'Product ' +oLi.PriceBookEntry.Product2.Name + ' does not have volume information.'));
                
                     
        }
     
        totalVolume = totalVolume.setScale(2);
        prodweight = prodweight.setScale(2);
 
        return null;
    } 
         
      public List<SelectOption> getCountriesOptions(){
            list<Location__c> lstLocation = [SELECT Name FROM Location__c WHERE Active__c= True ORDER BY Name];
            List<SelectOption> Options = new List<SelectOption>();
            //LMK 8/11/2014: Added this to display the default value.
             String def = theOpp.Ship_To_Account__r.Locations__r.Name;
             if(def!=null)
             {
            Options.add(new SelectOption('Select',def));
            }
           
                for(Location__c oL: lstLocation)
                {
                    
                    Options.add(new SelectOption(oL.Name, oL.Name));
                }
        return Options;
    }  
    public String getsSelectedLocation () {
        return sSelectedLocation;
    }        
    
       public PageReference changePricebook(){
    
        // This simply returns a PageReference to the standard Pricebook selection screen
        // Note that is uses retURL parameter to make sure the user is sent back after they choose
    
        PageReference ref = new PageReference('/oppitm/choosepricebook.jsp');
        ref.getParameters().put('id',theOpp.Id);
        ref.getParameters().put('retURL','/apex/opportunityProductEntry?id=' + theOpp.Id);
        
        return ref;
    }
}