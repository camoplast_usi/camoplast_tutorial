public with sharing class DTT_Order_PostCtrl {


    private Order refOrder = null;
    public Boolean refreshPage {get; set;}
    

    public DTT_Order_PostCtrl(ApexPages.StandardController controller) {

        if (controller != null)
            if (controller.getRecord() != null)
                    for (Order o : [Select Id, Posted__c  from Order where Id =: controller.getRecord().Id limit 1]) 
                        refOrder = o;

        
    }

    public PageReference Post()
    {
        // Test the new Sync() method to create the JSON:
        Sync();
        refOrder.Posted__c = true;
        update refOrder;
        refreshPage = true;
        return null;
    }

    
        // Mapping of Salesforce vs SAP Fields:
        Map<String, String> mapOrderFields_SFToSAP = new Map<String,String> {
                                                            'Type' => 'AUART',
                                                            'Sales_Organization__c' => 'VKORG',
                                                            'Distribution_Channel__c' => 'VTWEG',
                                                            'Division__c' => 'SPART',
                                                            'Sales_Office__c' => 'VKBUR',
                                                            'Sales_Group__c' => 'VKGRP',
                                                            'AccountId' => 'KUNNR',
                                                            'Ship_To_Account__c' => 'KUNNR',
                                                            'PoNumber' => 'BSTNK',
                                                            'PoDate' => 'BSTDK',
                                                            'Requested_Delivery_Date__c' => 'KETDAT',
                                                            'Warehouse__c' => 'DWERK',                                                            
                                                            'Price_List__c' => 'PLTYP',
                                                            'Price_Group__c' => 'KONDA',
                                                            'Order_Reason__c' => 'AUGRU',
                                                            'POD_Relevant__c' => 'PODKZ',
                                                            'Incoterms__c' => 'INCO1',
                                                            'Incoterms_Location__c' => 'INCO2',
                                                            'Payment_Terms__c' => 'ZTERM',
                                                            'Billing_Block__c' => 'FAKSK',
                                                            'Billing_Date__c' => 'FKDAT',
                                                            'Payment_Method__c' => 'ZLSCH',
                                                            'Date_Job_Date_Received__c' => 'ZZDJOBSREC',
                                                            'Date_Job_Sheet_Processed__c' => 'ZZDJOBSPRO',
                                                            'Time_On_Site__c' => 'ZZTIMONSITE',
                                                            'Time_Off_Site__c' => 'ZZTIMOFFSITE',
                                                            'VOR_Machine__c' => 'ZZVORMECHN',
                                                            'ShipToContactId' => 'ZZSITECONTACT',
                                                            'Service_Technician_Name__c' => 'ZZFITTER',
                                                            'Customer_Job_Number__c' => 'ZZCUSTJOBNO',
                                                            'Customer_Engineering_Team__c' => 'ZZCUSTENGG',
                                                            'Customer_Engineering_Region__c' => 'ZZCUSTENGGRG',
                                                            'Internal_Job_number__c' => 'ZZINTJOBNO',
                                                            'Third_Party_Job_Number__c' => 'ZZEXTJOBNO',
                                                            'Truck_Make__c' => 'ZZTRUCKMAKE',
                                                            'Truck_Model__c' => 'ZZTRUCKMOD',
                                                            'Truck_Serial__c' => 'ZZTRUCKSERL',
                                                            'POD_Name__c' => 'ZZPODNAME',
                                                            'Hour_Reading__c' => 'ZZHOURRREAD',
                                                            'SAP_Order_Number__c' => 'AUFNR',
                                                            'SAP_Order_Number__c' => 'ZZORDERNO',
                                                            'Complete_Delivery__c' => 'AUTLF',
                                                            'CreatedById' => 'ERNAM'
                                                    };
        Map<String, String> mapOrderItemFields_SFToSAP = new Map<String,String> {
                                                            'Wheel_Position__c' => 'ZZWHLPOS',
                                                            'Tyre_Condition__c' => 'ZZTYRECOND',
                                                            'Torque_Setting__c' => 'ZZTORQUE',
                                                            'Wex__c' => 'ZZWEX',
                                                            'Fitted_On_Site__c' => 'ZZFIT_ON_SITE'
        };
    /*********************************************************************************
    Method Name    : Sync
    Description    : Method to create a JSON formatted structure for the order, which is sent to Informatica
    Return Type    : void 
    Parameter      : None
    *********************************************************************************/
    public void Sync(){
        
        // Form the Order query by adding required fields
        String sOrderQuery = 'SELECT Id, RecordTypeId ';
        // Add fields required for mapping:
        for(String fieldName : mapOrderFields_SFToSAP.keySet()){
            sOrderQuery+= ', '+ fieldName;
        }
        // Add Order Item Query:
        sOrderQuery+= ', (SELECT Id ';
        // Add order item fields to be mapped:
        for(String orderItemFieldName : mapOrderItemFields_SFToSAP .keySet()){
            sOrderQuery+= ', '+ orderItemFieldName;
        }
        // Close Order Item query
        sOrderQuery+= ' FROM OrderItems) ';
        // Close Order Query:
        sOrderQuery+= ' FROM  Order WHERE Id = \''+refOrder.Id+'\'';
        System.debug('@@ Final Order Query: '+sOrderQuery);
        
        // Query for Order details & Order Items after Insert/Update of an Order
        List<Order> lstOrdersWithItems = Database.Query(sOrderQuery);
        /*List<Order> lstOrdersWithItems = [SELECT Id, Customer_Id__c, Location__c, Multiple_Ship_To__c, Scheduled_Ship_Date__c,TotalAmount, Invoice_Total__c,
                                            Type,Sales_Organization__c,Distribution_Channel__c,Division__c,Sales_Office__c,
                                            Sales_Group__c,AccountId,Ship_To_Account__c,PoNumber,PoDate,Requested_Delivery_Date__c,
                                            Warehouse__c,CurrencyIsoCode,Price_List__c,Price_Group__c,Order_Reason__c,
                                            POD_Relevant__c,Incoterms__c,Incoterms_Location__c,Payment_Terms__c,Billing_Block__c,
                                            Billing_Date__c,Payment_Method__c,Order_Notes__c,Date_Job_Date_Received__c,Date_Job_Sheet_Processed__c,
                                            Time_On_Site__c,Time_Off_Site__c,VOR_Machine__c,ShipToContactId,Service_Technician_Name__c,
                                            Customer_Job_Number__c,Customer_Engineering_Team__c,Customer_Engineering_Region__c,
                                            Internal_Job_number__c,Third_Party_Job_Number__c,Truck_Make__c,Truck_Model__c,Truck_Serial__c,
                                            Truck_Fleet__c,POD_Name__c,Hour_Reading__c,SAP_Order_Number__c,Complete_Delivery__c,CreatedById,
                                            Delivery_Date__c, RecordTypeId, 
                                            (SELECT Id,Quantity,Wheel_Position__c,Tyre_Condition__c,Torque_Setting__c,Wex__c,Fitted_On_Site__c FROM OrderItems)
                                            FROM Order WHERE Id = :refOrder.Id];
        */
        
        List<Map<String,Object>> lstAllOrderDetails = new List<Map<String,Object>>();
        
        for(Order oOrder: lstOrdersWithItems){
            Map<String,Object> mapOrderDetails = new Map<String,Object>();
            for(String orderField: mapOrderFields_SFToSAP.keySet()) {
                mapOrderDetails.put(mapOrderFields_SFToSAP.get(orderField), oOrder.get(orderField));
            }
            // Get the Order Item details:
            List<Map<String,Object>> lstAllOrderItems = new List<Map<String,Object>>();
            for(OrderItem oOrderItem : oOrder.OrderItems){
                Map<String,Object> mapOrderItemDetails = new Map<String,Object>();
                for(String orderItemField: mapOrderItemFields_SFToSAP.keySet()) {
                    mapOrderItemDetails.put(mapOrderItemFields_SFToSAP.get(orderItemField), oOrderItem.get(orderItemField));
                
                }
                lstAllOrderItems.add(mapOrderItemDetails);
            }
            
            mapOrderDetails.put('orderdetail',lstAllOrderItems);
            lstAllOrderDetails.add(mapOrderDetails);
        }
        
        // Create the JSON file from the Map:
        String sOrderJSON = JSON.SerializePretty(lstAllOrderDetails);
        System.Debug('@@@@ sOrderJSON: '+ sOrderJSON);
    }    
}