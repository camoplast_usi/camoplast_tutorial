public class DTT_Account_ConvertCtrl {

    private Account refAccount = null;
    private User refCSR = null;
    public Boolean refreshPage {get; set;}


    public DTT_Account_ConvertCtrl(ApexPages.StandardController controller) {

        if (controller != null)
            if (controller.getRecord() != null)
                    for (Account a : [Select Id, Type from Account where Id =: controller.getRecord().Id limit 1]) 
                        refAccount = a;

        RefUser = [Select Id, Reference_CSR__c from User where ID =: UserInfo.getUserId() limit 1];
        refreshPage = false;

    }


    public User RefUser
    {
    	get;set;
    }

    public PageReference Convert()
    {
    	if (refAccount.Type == 'Prospect')
    	{
    		User csrUser = null;
    		
    		if (RefUser.Reference_CSR__c != null)
    		{
	    		for (User u : [Select Id, FirstName from User where Id =: RefUser.Reference_CSR__c limit 1])
	    			csrUser = u;

	    		if (csrUser != null)
	    		{
	    			ChatterMention(csrUser.Id, csrUser.FirstName, refAccount.Id);
                    AssignTask(csrUser.Id, refAccount.Id);
                    refAccount.Type = 'Convert Pending';
                    update refAccount;
                    refreshPage = true;
	    		}
	    	}
    	}

        return null;

	}

	public static void ChatterMention(Id userID, string userName, Id accountId)
    {
    	system.debug('ChatterMention Start');
    	ConnectApi.FeedType feedType = ConnectApi.FeedType.Record;
		ConnectApi.FeedItemInput input = new ConnectApi.FeedItemInput();
		ConnectApi.MessageBodyInput messageInput = new ConnectApi.MessageBodyInput();
		ConnectApi.TextSegmentInput textSegment;
		ConnectApi.MentionSegmentInput mentionSegment = new ConnectApi.MentionSegmentInput();

		messageInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();

		mentionSegment.id = userId;
		messageInput.messageSegments.add(mentionSegment);

		textSegment = new ConnectApi.TextSegmentInput();
		textSegment.text =  string.format('\n' + System.Label.Account_Convert_Message, new string[]{userName});
		messageInput.messageSegments.add(textSegment);

		input.body = messageInput;

		ConnectApi.FeedItem feedItemRep = ConnectApi.ChatterFeeds.postFeedItem(null, feedType, accountId, input, null);

    }

    public static void AssignTask(Id userId, Id accountId)
    {
        Task t = new Task();
        t.Subject = 'Convert to Customer';
        t.Type = 'Account Opening';
        t.OwnerId = userId;
        t.WhatId = accountId;
        t.Status = 'Not Started';
        insert t;

    }

}