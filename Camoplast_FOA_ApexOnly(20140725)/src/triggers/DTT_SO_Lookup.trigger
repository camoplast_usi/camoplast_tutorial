trigger DTT_SO_Lookup on Sales_Order__c (before insert) {

        map<string, Id> masterMap = new map<string, Id>();
        map<string, Id> shipToMap = new map<string, Id>();

        for (Sales_Order__c so : trigger.new)
        {
            if(so.Customer_Code__c != null)
                masterMap.put(so.Customer_Code__c, null);
            if(so.Ship_To__c != null)
                shipToMap.put(so.Ship_To__c, null);
        }

        if(!masterMap.isEmpty())
            for (Account a : [Select Id, Customer_Id__c, Ship_To_Id__c from Account where Ship_To__c = false and Customer_Id__c in : masterMap.keyset() ])
                masterMap.put(a.Customer_ID__c, a.Id);

        if(!shipToMap.isEmpty())
            for (Account a : [Select Id, Customer_Id__c, Ship_To_Id__c from Account where Ship_To__c = true and Ship_To_Id__c in : shipToMap.keyset() ])
                shipToMap.put(a.Ship_To_Id__c, a.Id);


        for (Sales_Order__c so : trigger.new)
        {
            if(so.Customer_Code__c != null)
                if (masterMap.containsKey(so.Customer_Code__c))
                    so.Customer_Account__c = masterMap.get(so.Customer_Code__c);
            
            if(so.Ship_To__c != null)
                if(shipToMap.containsKey(so.Ship_To__c))
                    so.Ship_To_Account__c = shipToMap.get(so.Ship_To__c);
        }


}