trigger DTT_Lead_Location on Lead (after insert, after update) {

	if (system.isBatch())
		return;

	if (system.isFuture())
		return;

	if (system.isScheduled())
		return;


	if (trigger.size ==1 )
	{
		for (Lead l : trigger.new)
		{
			if (l.Last_Geocoding__c == null || l.Last_Geocoding__c < Date.today())
			{
				if (Trigger.isInsert)
					if (l.Street != null) 
						DTT_LocationCallouts.UpdateLeadFuture(l.Id);

				if (Trigger.isUpdate)
					if ((l.Street != trigger.oldMap.get(l.Id).Street) || (l.Street != null && l.Last_Geocoding__c == null))
						DTT_LocationCallouts.UpdateLeadFuture(l.Id);
			}
		}
	}
}