/*********************************************************************************
Class Name      : CS_OrderProductsEntryExtn
Description     : This is class is the extension controller for page CS_OrderProductEntry
Created By      : Reju Palathingal
Created Date    : 2014-07-15
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Reju Palathingal        15-Jul-2014                Initial Version
Divya                   25-Jul-2014                Update Price & Weight
Divya                   25-Ju-2014                 Apply Discount button action
Divya                   4-Aug-2014                 Search Using Customer Item Number
Maryam                  4-Aug-2014                 Sales Order History
Maryam                  11-Aug-2014                Update discount sales price to fix Amount 
Laxmy                   11-Aug-2014                Implemented default value on Locations pick list 
Maryam                  18-Aug-2014                Added methods weightConversion, recordTypeWeightLabel and updated ApplyDiscount()
                                                   and UpdatePriceandWeight() to display Weight/Volume ratio and calculate totals based on 
                                                   unit of measure and region
Divya                   09-Sep-2014                Updated Ship To Account field with the Ship To Account field 
                                                   that looks up to the junction object acc_junction
*********************************************************************************/
public with sharing class CS_OrderProductsEntryExtn {

    public Order theOrder{get; set;}
    public String searchString {get;set;}
    public OrderItem[] shoppingCart {get;set;}
    public priceBookEntry[] AvailableProducts {get;set;}
    public Pricebook2 theBook {get;set;}   
    //Added for the post
    
    public String toSelect {get; set;}
    public String toUnselect {get; set;}
    public string ChosenValue{get;set;}
    public Decimal Total {get;set;}
    public Decimal Totalsales {get; set;}
    public Decimal prodweight{get; set;}
    public Decimal addeddiscount{get; set;}
    public Boolean freightdisc {get; set;}
    public Decimal freightDiscConverted {get; set;}
    public Boolean weightdisc {get; set;}
    public String weightLabel{get;set;}
    public String recordType{get;set;}
    public String totalWeightLabel {get;set;}
    public String weightMetric {get;set;}
    public Decimal totalVolume{get; set;}
    public Decimal prodWeightConverted {get;set;}
    public Decimal addedDiscountPercent{get; set;}
 
    public Boolean removeProduct {get;set;}
    public Decimal accountDiscountWeight{get; set;}
    public Boolean overLimit {get;set;}
    public Boolean multipleCurrencies {get; set;}
    
    private Boolean forcePricebookSelection = false;
    
    private OrderItem[] forDeletion = new OrderItem[]{};
    public String qString{get;set;}


    public CS_OrderProductsEntryExtn(ApexPages.StandardController controller) {

        // Need to know if org has multiple currencies enabled
        multipleCurrencies = UserInfo.isMultiCurrencyOrganization();

        // Get information about the Opportunity being worked on
        Id recId;
        if(controller.getRecord() != NULL)
            recId = controller.getRecord().Id;
        if(recId == NULL)
            recId = ApexPages.currentPage().getParameters().get('ordId');
        
        if(multipleCurrencies){{
        system.debug(controller+ '***');
            theOrder = database.query('select Id,AccountId,Added_Discount__c,Total_Price__c, Account_Wt_for_Discount__c,Account_Percent_Wt_Discount__c, Account_Wt_for_Discount_Unit__c,Freight_Discount_Applied__c,Weight_Discount_Applied__c, Pricebook2Id, Pricebook2.Name, CurrencyIsoCode,Ship_To_Account_RelatedAccount__r.Ship_To__r.Locations__r.Name from Order where Id =  \'' + recId + '\' limit 1');
            system.debug(controller.getRecord().Id + '***');}
        } else{
            theOrder = [select Id,AccountId,Added_Discount__c ,Total_Price__c,Freight_Discount_Applied__c,  Account_Wt_for_Discount__c,Account_Percent_Wt_Discount__c,Account_Wt_for_Discount_Unit__c,Weight_Discount_Applied__c,Pricebook2Id, PriceBook2.Name,Ship_To_Account_RelatedAccount__r.Ship_To__r.Locations__r.Name from Order where Id = :recId limit 1];
        }   
        // If products were previously selected need to put them in the "selected products" section to start with //Location__c
        shoppingCart = [select Id, Quantity, Total_Price__c, OrderHistory__c, Wt_Vol_Ratio__c,UnitPrice, ListPrice, Location__c, Description, PriceBookEntryId, PriceBookEntry.Name,
                        PriceBookEntry.IsActive,Quantity__c, PriceBookEntry.Product2Id, PriceBookEntry.Product2.Name,PriceBookEntry.Product2.Volume__c,PriceBookEntry.Product2.Unit_of_Measure__c,PricebookEntry.UnitPrice,
                        PriceBookEntry.PriceBook2Id,PricebookEntry.Product2.Weight__c,Discount__c,Back_Order__c,Discount_Reason__c, PriceBookEntry.Product2.Family,
                        PriceBookEntry.Product2.Global_PID__c,Notes__c,PricebookEntry.Product2.Location__c ,PriceBookEntry.Product2.Product_Line__c ,PriceBookEntry.Product2.ProductCode from OrderItem where OrderId=:theOrder.Id];
       
         //Maryam Aug 15 - Get Record type
        Map<ID, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Order.getRecordTypeInfosById();
        recordType = rtMap.get(theOrder.RecordTypeId).getName();
        recordTypeWeightLabel();
       
       
        //Calculate Total Price & Weight
        updatePriceAndWeight();
        // Check if Opp has a pricebook associated yet
        if(theOrder.Pricebook2Id == null){
            Pricebook2[] activepbs = [select Id, Name from Pricebook2 where isActive = true limit 2];
            if(activepbs.size() == 2){
                forcePricebookSelection = true;
                theBook = new Pricebook2();
            }
            else{
                theBook = activepbs[0];
            }
        }
        else{
            theBook = theOrder.Pricebook2;
        }
        
        
    }
    
    //Sales Order History Page
    public pageReference openSalesOrderHistory() 
    {
        pageReference pg = new pageReference('/apex/orderHistory');
        pg.setRedirect(true);
        return pg;
    }         
    // this is the 'action' method on the page
    public PageReference priceBookCheck(){
    
        // if the user needs to select a pricebook before we proceed we send them to standard pricebook selection screen
        if(forcePricebookSelection){        
            return changePricebook();
        }
        else{
        
            //if there is only one active pricebook we go with it and save the opp
            if(theOrder.pricebook2Id != theBook.Id){
                try{
                    theOrder.Pricebook2Id = theBook.Id;
                    update(theOrder);
                }
                catch(Exception e){
                    ApexPages.addMessages(e);
                }
            }
            
            return null;
        }
    }
       
    public String getChosenCurrency(){
    
        if(multipleCurrencies)
            return (String)theOrder.get('CurrencyIsoCode');
        else
            return '';
    }

    public void updateAvailableList() {
    
        // We dynamically build a query string and exclude items already in the shopping cart
        //Divya:Search Using Customer Item Number
            list<Custom_PID_Link__c> customPIDLink = [SELECT Product__c from Custom_PID_Link__c where Customer_Item_Number__c like : searchString];
            set<ID> forProducts = new set<ID>();
            for(Custom_PID_Link__c cPID:customPIDLink)
               forProducts.add(cPID.Product__c);
        qString = 'select Id, Pricebook2Id, IsActive, Product2.Name,Product2.Location__c, Product2.Volume__c, Product2.Unit_of_Measure__c,Product2.Family,Product2.Product_Line__c, Product2.IsActive,Product2.ProductCode,Product2.ABC_Classification__c,Product2.Description, UnitPrice,Product2.Weight__c,Product2.Global_PID__c,Product2.On_hand__c,Product2.Reserved__c,Product2.Ordered__c,Product2.Transit__c,Product2.AvailableInventory__c from PricebookEntry where IsActive=true and Pricebook2Id = \'' + theBook.Id + '\'';
        if(multipleCurrencies)
            qstring += ' and CurrencyIsoCode = \'' + theOrder.get('currencyIsoCode') + '\'';
        
        // note that we are looking for the search string entered by the user in the name OR description
        // modify this to search other fields if desired
        if(searchString!=null){
           qString += string.format('and (Product2.Name like \'\'%{0}%\'\' or Product2.Description like \'\'%{0}%\'\' or Product2.Id in : forProducts)', new string[]{searchString});
        }
        
        Set<Id> selectedEntries = new Set<Id>();
        Totalsales=0.0;
        prodweight=0.0;
        for(OrderItem d:shoppingCart){
            selectedEntries.add(d.PricebookEntryId);
            updatePriceAndWeight();
        }
        
        if(selectedEntries.size()>0){
            String tempFilter = ' and Id not in (';
            for(Id i : selectedEntries){
                tempFilter+= '\'' + (String)i + '\',';
            }
            String extraFilter = tempFilter.substring(0,tempFilter.length()-1);
            extraFilter+= ')';
            
            qString+= extraFilter;
        }
        
        qString+= ' order by Product2.Name';
        qString+= ' limit 101';
        
        system.debug('qString:' +qString);        
        AvailableProducts = database.query(qString);
        
        // We only display up to 100 results... if there are more than we let the user know (see vf page)
        if(AvailableProducts.size()==101){
            AvailableProducts.remove(100);
            overLimit = true;
        }
        else{
            overLimit=false;
        }
        //Maryam recalculate discount if product removed
        if(removeProduct==true)
        {    removeProduct=false;
             applyDiscount();
        }
    }
    
    public void addToShoppingCart(){
    
        // This function runs when a user hits "select" button next to a product
    
        for(PricebookEntry priceBookEntry : AvailableProducts){
            if((String)priceBookEntry.Id==toSelect){
                shoppingCart.add(new OrderItem(OrderId=theOrder.Id, PriceBookEntry=priceBookEntry, PriceBookEntryId=priceBookEntry.Id, UnitPrice=priceBookEntry.UnitPrice, OrderHistory__c=priceBookEntry.Product2Id));
                break;
            }
        }
        
        updateAvailableList();  
    }
    
    public list<Product2>getProductlist 
    
    {get
        {
            
            ID PBId=theOrder.pricebook2Id;
         
            return[select name from product2 where Id in (select Product2Id from PricebookEntry where Pricebook2Id=:PBId)limit 999];
        }
    }
    public PageReference removeFromShoppingCart(){
        
        // Maryam flag to indicate removal has taken place, so discount to be recalculated
        removeProduct=true;
        // This function runs when a user hits "remove" on an item in the "Selected Products" section
    
        Integer count = 0;
    
        for(OrderItem orderItem : shoppingCart){
            if((String)orderItem.PriceBookEntryId==toUnselect){
            
                if(orderItem.Id!=null)
                    forDeletion.add(orderItem );
            
                shoppingCart.remove(count);
                break;
            }
            count++;
        }
        
        updateAvailableList();
        
        return null;
    }
    
    public PageReference onSave(){
    
        // If previously selected products are now removed, we need to delete them
        if(forDeletion.size()>0)
            delete(forDeletion);
         //To Apply Entered Discounts on Save if Apply Discount is not clicked by the user manually
        applyDiscount();
        
      
    
        // Previously selected products may have new quantities and amounts, and we may have new products listed, so we use upsert here
        try{
            if(shoppingCart.size()>0)
                upsert(shoppingCart);
            upsert(theOrder);
          }
        catch(Exception e){
            ApexPages.addMessages(e);
            return null;
        }  
        // After save return the user to the Order
        return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));
    }
    
    public PageReference onCancel(){
 
        // If user hits cancel we commit no changes and return them to the Order   
        return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));
    }
    
   //Divya-Action for Apply Discount button
     //Divya-Action for Apply Discount button
    public PageReference applyDiscount(){
    
    Totalsales =0.0;
    totalVolume = 0.0;
        for(Orderitem oI1 : shoppingCart) { 
            oI1.Quantity= oI1.Quantity__c;       
            if(oI1.Discount__c != null && oI1.Discount__c>=0 && oI1.Discount__c <=100)
                oI1.UnitPrice= oI1.PriceBookEntry.UnitPrice - (oI1.PriceBookEntry.UnitPrice  * (oI1.Discount__c/100));
            else if(oI1.Discount__c == null && (oI1.UnitPrice != oI1.PriceBookEntry.UnitPrice)){
                oI1.UnitPrice= oI1.PriceBookEntry.UnitPrice;
            }
            else if(oI1.Discount__c < 0 || oI1.Discount__c >100) {
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Discount can not be negative or greater than 100%.'));
                break;
            }                
            Totalsales = Totalsales + (oI1.UnitPrice* oI1.Quantity);    
               //Maryam - update weight volume ratio
            if(oI1.PriceBookEntry.Product2.Volume__c!= null && oI1.Quantity!= null&& oI1.PriceBookEntry.Product2.Volume__c>0)
            {   if(oI1.PriceBookEntry.Product2.Weight__c!=null&&oI1.PriceBookEntry.Product2.Unit_of_Measure__c!=null&&oI1.PriceBookEntry.Product2.Unit_of_Measure__c!='')
                    oI1.Wt_Vol_Ratio__c = String.valueOf((Decimal.valueOf(oI1.PriceBookEntry.Product2.Weight__c)* oI1.Quantity).intValue()) + oI1.PriceBookEntry.Product2.Unit_of_Measure__c+ '/' + String.valueOf((oI1.PriceBookEntry.Product2.Volume__c*oI1.Quantity).intValue())+'m3';
                //totalVolume = totalVolume + (oI1.PriceBookEntry.Product2.Volume__c * oI1.Quantity); 
            }             
        }
      //Maryam - Refreshing weight and volume before calculation added discount
        prodweight =0.0;
        totalVolume =0.0;
            
        for(OrderItem oI1: shoppingCart) {                     
            if(oI1.PricebookEntry.Product2.Weight__c!= null && oI1.PricebookEntry.Product2.Weight__c != ''&&oI1.PriceBookEntry.Product2.Unit_of_Measure__c!='' &&oI1.PriceBookEntry.Product2.Unit_of_Measure__c!=null)
            {   if(oI1.Quantity!=null){
                    if(recordType == 'North America'){
                         if(oI1.PriceBookEntry.Product2.Unit_of_Measure__c =='KG')
                             prodWeightConverted =   weightConversion(Decimal.Valueof(oI1.PricebookEntry.Product2.Weight__c),'Lbs');
                         else 
                             prodWeightConverted = Decimal.Valueof(oI1.PricebookEntry.Product2.Weight__c);
                         prodweight = prodweight + (prodWeightConverted * oI1.Quantity); 
                    }
                    else{
                         if(oI1.PriceBookEntry.Product2.Unit_of_Measure__c =='Lbs')
                               prodWeightConverted =   weightConversion(Decimal.Valueof(oI1.PricebookEntry.Product2.Weight__c),'KG');
                         else 
                               prodWeightConverted = Decimal.Valueof(oI1.PricebookEntry.Product2.Weight__c);
                         prodweight = prodweight + (prodWeightConverted * oI1.Quantity); 
                    }
                 }
            }  
            else
                 ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Warning,'Product ' +oI1.PriceBookEntry.Product2.Name + ' does not have weight/unit of measure information.'));
          
            if(oI1.PriceBookEntry.Product2.Volume__c!= null&&oI1.PriceBookEntry.Product2.Volume__c>0)
            {   if(oI1.Quantity!= null)
                    totalVolume= totalVolume + (oI1.PriceBookEntry.Product2.Volume__c * oI1.Quantity); 
            }  
            else
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Warning,'Product ' +oI1.PriceBookEntry.Product2.Name + ' does not have volume information.'));
          
            
        }
            
            
        if(theOrder.Added_Discount__c< 0 || theOrder.Added_Discount__c >100) {
               ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Discount can not be negative or greater than 100%.'));
               
        }      
        else         
        {   if(theOrder.Added_Discount__c == null)   
                theOrder.Added_Discount__c = 0;
             
            if(prodweight > freightDiscConverted )
             {    theOrder.Freight_Charges__c = 0; 
                  theOrder.Freight_Discount_Applied__c = true;             
             }                  
             else
                 theOrder.Freight_Discount_Applied__c= false; 
             if(prodweight> accountDiscountWeight)
             {                        
                    if(theOrder.Weight_Discount_Applied__c== false || theOrder.Weight_Discount_Applied__c==null)
                    {   Totalsales = Totalsales  - (Totalsales * (theOrder.Added_Discount__c + addedDiscountPercent )/100);      
                        theOrder.Added_Discount__c = theOrder.Added_Discount__c + addedDiscountPercent ;
                        theOrder.Weight_Discount_Applied__c = true;
                   
                        
                    }
                    else 
                        Totalsales = Totalsales  - (Totalsales * (theOrder.Added_Discount__c)/100);                            
             }            
             else 
             {    if(theOrder.Weight_Discount_Applied__c== true)
                  {   if(theOrder.Added_Discount__c >=addedDiscountPercent )
                      {
                            Totalsales = Totalsales  - (Totalsales * (theOrder.Added_Discount__c - addedDiscountPercent )/ 100);
                            theOrder.Added_Discount__c = theOrder.Added_Discount__c - addedDiscountPercent ;
                           
                      }                 
                       theOrder.Weight_Discount_Applied__c=false;        
                  }
                  else
                  {    Totalsales = Totalsales  - (Totalsales * (theOrder.Added_Discount__c/ 100));
                        
                  }
             } 

        }
        totalVolume = totalVolume.setScale(2);
        prodweight = prodweight.setScale(2);
        theOrder.Total_Price__c = Totalsales;
        return null;
    }
    //Divya:Update Price & Weight
    public PageReference updatePriceAndWeight(){
                Totalsales=0.0;
                prodweight=0.0;
                totalVolume = 0.0;
                
        for(OrderItem oI : shoppingCart) { 
           
            if(oI.UnitPrice!= null && oI.UnitPrice!= 0 && oI.Quantity!= null){
               Totalsales = Totalsales + (oI.UnitPrice* oI.Quantity); 
            }  
                
            
            if((oI.PricebookEntry.Product2.Weight__c!= null) && (oI.PricebookEntry.Product2.Weight__c != '')&& oI.PriceBookEntry.Product2.Unit_of_Measure__c!=null && oI.PriceBookEntry.Product2.Unit_of_Measure__c!='')
            {    if(oI.Quantity!=null){
                     if(recordType == 'North America'){
                         if(oI.PriceBookEntry.Product2.Unit_of_Measure__c =='KG')
                             prodWeightConverted =   weightConversion(Decimal.Valueof(oI.PricebookEntry.Product2.Weight__c),'Lbs');
                         else 
                             prodWeightConverted = Decimal.Valueof(oI.PricebookEntry.Product2.Weight__c);
                         prodweight = prodweight + (prodWeightConverted * oI.Quantity); 
                    }
                    else{
                         if(oI.PriceBookEntry.Product2.Unit_of_Measure__c =='Lbs')
                               prodWeightConverted =   weightConversion(Decimal.Valueof(oI.PricebookEntry.Product2.Weight__c),'KG');
                         else 
                               prodWeightConverted = Decimal.Valueof(oI.PricebookEntry.Product2.Weight__c);
                         prodweight = prodweight + (prodWeightConverted * oI.Quantity);  
                    }
                }
                
            }   
            else
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Warning,'Product ' +oI.PriceBookEntry.Product2.Name + ' does not have weight/unit of measure information.'));
                
            //Maryam added volume calculation
            if(oI.PriceBookEntry.Product2.Volume__c!= null && oI.PriceBookEntry.Product2.Volume__c>0)
            {  if(oI.Quantity!=null&&oI.PricebookEntry.Product2.Weight__c!=null&&oI.PricebookEntry.Product2.Unit_of_Measure__c!=null && oI.PricebookEntry.Product2.Unit_of_Measure__c!=''){
                   oI.Wt_Vol_Ratio__c = String.valueOf((Decimal.valueOf(oI.PriceBookEntry.Product2.Weight__c)* oI.Quantity).intValue()) + oI.PriceBookEntry.Product2.Unit_of_Measure__c+'/' + String.valueOf((oI.PriceBookEntry.Product2.Volume__c*oI.Quantity).intValue())+'m3';
                   totalVolume = totalVolume + (oI.PricebookEntry.Product2.Volume__c * oI.Quantity); 
               }
            }
            else
               ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Warning,'Product ' +oI.PriceBookEntry.Product2.Name + ' does not have volume information.'));
                
                     
        }
            totalVolume = totalVolume.setScale(2);
            prodweight = prodweight.setScale(2);
            return null;
    } 
    //Maryam update weight to kilograms for EMEA and Lbs for North America records
    public Decimal weightConversion(Decimal weight, String unit){
        if (unit== 'Lbs'){
          
            return weight/0.453592;
        }
        else 
        {    
            return weight*0.453592;
        }
    }
    //Maryam update weight label EMEA users
    public void recordTypeWeightLabel(){
        if (recordType=='North America'){
            addedDiscountPercent = theOrder.Account_Percent_Wt_Discount__c.intValue();
            weightLabel = String.valueOf(theOrder.Account_Percent_Wt_Discount__c.intValue()) + '% Weight Discount';
            freightDiscConverted = Integer.valueOf(Label.CS_Freight_Discount);
            totalWeightLabel = 'Total Weight (Lbs):';
            if(theOrder.Account_Wt_for_Discount_Unit__c == 'KG' )
                accountDiscountWeight = weightConversion(theOrder.Account_Wt_for_Discount__c, 'Lbs');
            else
                accountDiscountWeight = theOrder.Account_Wt_for_Discount__c;
        }
        else
        {   weightLabel = Label.CS_Percent_Weight_Discount + '% Weight Discount';
            addedDiscountPercent = Integer.valueOf(Label.CS_Percent_Weight_Discount);
            totalWeightLabel = 'Total Weight (KG):';
            freightDiscConverted = weightConversion(Integer.valueOf(Label.CS_Freight_Discount),'KG');
            accountDiscountWeight = weightConversion(Integer.valueOf(Label.CS_Weight_Discount), 'KG');
           
        }        
    } 
    
     public List<SelectOption> getCountriesOptions(){
            list<Location__c> lstLocation = [SELECT Name FROM Location__c WHERE Active__c= True ORDER BY Name];
            List<SelectOption> Options = new List<SelectOption>();
            //LMK 8/11/2014: Added this to display the default value.
             String def = theOrder.Ship_To_Account_RelatedAccount__r.Ship_To__r.Locations__r.Name;
             if(def!=null)
             {
            Options.add(new SelectOption('Select',def));
            }
                 for(Location__c oL: lstLocation)
                {
                    
                    Options.add(new SelectOption(oL.Name, oL.Name));
                }
        return Options;
    }          
    
     public String getChosenValue() {
        return ChosenValue;
    }
    
    //[Divya :30-Jul]To store all the values of the Product Family that qualify as "Material"
     public String sValue{ get { return 'Construction Pneumatic;Industrial Pneumatic'; } set; }
    
    public PageReference changePricebook(){
    
        // This simply returns a PageReference to the standard Pricebook selection screen
        // Note that is uses retURL parameter to make sure the user is sent back after they choose
    
        PageReference ref = new PageReference('/_ui/busop/orderitem/ChooseOrderPricebook/e?');
        ref.getParameters().put('id',theOrder.Id);
        ref.getParameters().put('retURL','/apex/CS_OrderProductEntry?id=' + theOrder.Id);
        
        return ref;
    }
}