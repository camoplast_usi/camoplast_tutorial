global class DTT_ICRT_Helper {

	global static void PostLog(string status, string  message, datetime timestamp)
	{
		ICRT_Log__c newLog = new ICRT_Log__c();
		newLog.Status__c = status;
		newLog.Message__c = status;
		newLog.Timestamp__c = timestamp;
		insert newLog;
	}

	public static string ServerURL
	{
		get{return string.format('https://{0}-api.salesforce.com/services/Soap/u/27.0/{1}', new string[]{Instance, OrgId});}
		
	}

	public static String Instance {
    	get 
    	{
        	if (Instance == null) 
        	{
	            //
	            // Possible Scenarios:
	            //
	            // (1) ion--test1--nexus.cs0.visual.force.com  --- 5 parts, Instance is 2nd part
	            // (2) na12.salesforce.com      --- 3 parts, Instance is 1st part
	            // (3) ion.my.salesforce.com    --- 4 parts, Instance is not determinable

	            // Split up the hostname using the period as a delimiter
	            List<String> parts = System.URL.getSalesforceBaseUrl().getHost().replace('-api','').split('\\.');
	            if (parts.size() == 3) Instance = parts[0];
	            else if (parts.size() == 5) Instance = parts[1];
	            else Instance = null;
	        } 
		return Instance;
    	} private set;
	}

	public static String OrgId
	{
    	get{ return UserInfo.getOrganizationId();}
    }

    
}