global class DTT_LocationCallouts implements Database.Batchable<sObject>, Schedulable , Database.AllowsCallouts, Database.Stateful{


  private boolean mLeadUpdate = false;
  private boolean mAccountUpdate = false;
  private integer mMaxRequest = 0;
  private map<string, integer> mAPIKeys = null;

  public DTT_LocationCallouts()
  {
    InitMap();
  }

  public DTT_LocationCallouts(boolean leadUpdate, boolean accountUpdate)
  {
    this();
    mLeadUpdate = leadUpdate;
    mAccountUpdate = accountUpdate;
  }

  global void execute(SchedulableContext ctx)
  {
    if(Math.mod(Date.today().daysBetween(Date.today().toStartOfWeek()), 2) == 0)
    {
      DTT_LocationCallouts accountUpdate = new DTT_LocationCallouts(false, true);
      Database.ExecuteBatch(accountUpdate, 1);
    }
    else
    {
      DTT_LocationCallouts leadUpdate = new DTT_LocationCallouts(true, false);
      Database.ExecuteBatch(leadUpdate, 1);
    }
  }

  public static void Schedule()
  {
    DTT_LocationCallouts d = new DTT_LocationCallouts();
    String sch = '0 0 3 * * ?';  
    String jobID = system.schedule('Geolocation', sch, d);
  
  }


  global Database.QueryLocator start(Database.BatchableContext bc) {

    Database.QueryLocator retVal = null;
    //integer dailyLimit = integer.valueof(system.Label.Nearby_Limit);
    integer dailyLimit = mMaxRequest;

    if (mAccountUpdate)
      retVal =  Database.getQueryLocator([SELECT BillingCity,BillingCountry,BillingPostalCode,BillingState,BillingStreet, ShippingCity,ShippingCountry,ShippingPostalCode,ShippingState,ShippingStreet, Ship_To__c FROM Account WHERE (ShippingStreet != null or BillingStreet != null) order by Last_Geocoding__c asc NULLS FIRST Limit :dailyLimit  ]);

    if (mLeadUpdate)
      retVal =  Database.getQueryLocator([SELECT City, Country, PostalCode,Street, State FROM Lead WHERE IsConverted = false and Street != null order by Last_Geocoding__c asc NULLS FIRST limit : dailyLimit]);

    return retVal;
  }


  global void execute(Database.BatchableContext BC, list<Sobject> scope){

    for (sObject s : scope)
    {
      if (mLeadUpdate)
        UpdateLocation((Lead)s, true);
      else if (mAccountUpdate)
        UpdateLocation((Account)s, true);
    }
  }

  global void finish(Database.BatchableContext BC){

     // Get the ID of the AsyncApexJob representing this batch job
     // from Database.BatchableContext.
     // Query the AsyncApexJob object to retrieve the current job's information.
     AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
        TotalJobItems, CreatedBy.Email
        FROM AsyncApexJob WHERE Id =
        :BC.getJobId()];
     // Send an email to the Apex job's submitter notifying of job completion.
     Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
     String[] toAddresses = new String[] {'pidufour@deloitte.ca'};
     mail.setToAddresses(toAddresses);
     mail.setSubject('DTT_LocationCallouts ' + a.Status);
     mail.setPlainTextBody
     ('The batch Apex job processed ' + a.TotalJobItems +
     ' batches with '+ a.NumberOfErrors + ' failures.');
     Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

  }

  public void TestAccount(Id accountId)
  {
    Account a = [SELECT BillingCity,BillingCountry,BillingPostalCode,BillingState,BillingStreet, ShippingCity,ShippingCountry,ShippingPostalCode,ShippingState,ShippingStreet, Ship_To__c FROM Account WHERE id =: accountId ];        
    UpdateLocation(a, false);
    
  }

  public void TestLead(Id leadId)
  {
    Lead l = [SELECT City, Country, PostalCode,Street, State FROM Lead WHERE Id =: leadId];        
    UpdateLocation(l, false);
  }

  

  public void InitMap()
  {
    mAPIKeys = new map<string, integer>();
    map<string, Google_API__c> mapCustomSettings = Google_API__c.getAll();
    
    for (string user : mapCustomSettings.keyset() )
    {
      Google_API__c gAPI = mapCustomSettings.get(user);
      mAPIKeys.put(gAPI.Geo_Key__c, integer.valueof(gAPI.Request__c));
      mMaxRequest += integer.valueof(gAPI.Request__c);
    }

  }

  public  string GetNextKey(boolean batchCall)
  {
    string retVal = null;

    for (string googleKey : mAPIKeys.keyset())
    {
      if (batchCall)
      {
        if ( mAPIKeys.get(googleKey) > 0 )
        {
          retVAl = googleKey;
          mAPIKeys.put(googleKey, mAPIKeys.get(googleKey) - 1);
          break;
        }
      }
      else
      {
        if ( mAPIKeys.get(googleKey) == -1 )
        {
          retVal = googleKey;
          break;
        }
      }
    }
  
    system.debug('DTT_LocationCallouts GetNextKey ' + retVal);
    return retVal;
  }

 

  @future(callout=true)
  public static void UpdateLeadFuture(Id leadId)
  {
    Lead l = [SELECT City, Country, PostalCode,Street, State FROM Lead WHERE Id =: leadId];        
    DTT_LocationCallouts d = new DTT_LocationCallouts();
    d.UpdateLocation(l, false);
  }

  public void UpdateLocation(Lead l, boolean batchCall){

        string googleKey =  GetNextKey(batchCall);

        if (googleKey == null)
          return;

        string address = '';

        if (l.Street != null)
        address += l.Street +', ';
        if (l.City != null)
        address += l.City +', ';
        if (l.State != null)
        address += l.State +' ';
        if (l.PostalCode != null)
        address += l.PostalCode +', ';
        if (l.Country != null)
        address += l.Country; 

        map<string, string> locationMap = GetLocation(address, googleKey); 
        l.Location__Latitude__s =  locationMap.get('lat') != null ? double.valueof(locationMap.get('lat')) : null;
        l.Location__Longitude__s = locationMap.get('lon') != null ? double.valueof(locationMap.get('lon')) : null;


      // Pierre Dufour - Always stamp the geocoding date even if it fails.
      //if ( locationMap.get('lat') != null)
        l.Last_Geocoding__c = Date.today();
        l.Last_Geocoding_Log__c = locationMap.get('log');
        update l;
      }

      @future(callout=true)
      public static void UpdateAccountFuture(Id accountId)
      {

        Account a = [SELECT BillingCity,BillingCountry,BillingPostalCode,BillingState,BillingStreet, ShippingCity,ShippingCountry,ShippingPostalCode,ShippingState,ShippingStreet, Ship_To__c FROM Account WHERE id =: accountId ];        
        DTT_LocationCallouts d = new DTT_LocationCallouts();
        d.UpdateLocation(a, false);
      }

      public void UpdateLocation(Account a, boolean batchCall ){
      // gather account info

      string googleKey =  GetNextKey(batchCall);

      if (googleKey == null)
        return;
          
      
      // create an address string
      String address = '';
      
      if (a.Ship_To__c == false)
      {
        if (a.BillingStreet != null)
        address += a.BillingStreet +', ';
        if (a.BillingCity != null)
        address += a.BillingCity +', ';
        if (a.BillingState != null)
        address += a.BillingState +' ';
        if (a.BillingPostalCode != null)
        address += a.BillingPostalCode +', ';
        if (a.BillingCountry != null)
        address += a.BillingCountry;
      }
      else if (a.Ship_To__c == true)
      {
        if (a.ShippingStreet != null)
        address += a.ShippingStreet +', ';
        if (a.ShippingCity != null)
        address += a.ShippingCity +', ';
        if (a.ShippingState != null)
        address += a.ShippingState +' ';
        if (a.ShippingPostalCode != null)
        address += a.ShippingPostalCode +', ';
        if (a.ShippingCountry != null)
        address += a.ShippingCountry; 
      }

      map<string, string> locationMap = GetLocation(address, googleKey); 
      a.Location__Latitude__s = locationMap.get('lat') != null ? double.valueof(locationMap.get('lat')) : null;
      a.Location__Longitude__s = locationMap.get('lon') != null ? double.valueof(locationMap.get('lon')) : null;


      // Pierre Dufour - Always stamp the geocoding date even if it fails.
      //if ( locationMap.get('lat') != null)
      a.Last_Geocoding__c = Date.today();
      a.Last_Geocoding_Log__c = locationMap.get('log');

      update a;
    }


    public  map<string, string> GetLocation(string address, string googleKey)
    {
      map<string, string> retVal = new map<string, string>();
      double lat = null;
      double lon = null;
      
      if (address !=  null)
      {
        if (address != '')
        {
          address = EncodingUtil.urlEncode(address, 'UTF-8');
          string requestURL = 'https://maps.googleapis.com/maps/api/geocode/json?address='+address+'&sensor=false&key=' + googleKey; 
          system.debug(requestURL);
            // build callout
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            req.setEndpoint(requestURL);
            req.setMethod('GET');
            req.setTimeout(60000);

            {
              // callout
              HttpResponse res = h.send(req);
              
              // parse coordinates from response
              JSONParser parser = JSON.createParser(res.getBody());
              system.debug('DTT_Location.GetLocation ' + res.getBody());
              retVal.put('log', res.getBody() != null ? res.getBody().substring(0, Math.Min(res.getBody().length(), 32768-1) ): '');

              while (parser.nextToken() != null) {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&
                  (parser.getText() == 'location')){
                         parser.nextToken(); // object start
                         while (parser.nextToken() != JSONToken.END_OBJECT){
                           String txt = parser.getText();
                           system.debug(txt);
                           parser.nextToken();
                           if (txt == 'lat')
                           lat = parser.getDoubleValue();
                           else if (txt == 'lng')
                           lon = parser.getDoubleValue();
                         }
                       }
                     }

                     retVal.put('lat', string.valueof(lat));
                     retVal.put('lon', string.valueof(lon));
                   }

                 }
                   system.debug('DTT_Location.GetLocation ' + address);
                 system.debug('DTT_Location.GetLocation ' + retVal);
               }
               return retVal;
            }
          }