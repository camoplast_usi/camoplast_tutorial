public class DTT_Lead_Map_Ctrl
{
 private final Lead refLead;
 private boolean Mode = false;

    public DTT_Lead_Map_Ctrl(ApexPages.StandardController controller) 
   {
    refLead=[Select Id, Name, Street, City,State, PostalCode, Country,address
             from Lead where Id =: Controller.getRecord().Id];
     If ((refLead.street!=null)||(refLead.city!=null))
        Mode=true;
   }
     public void Reload()
    {

    }

    public void Shipping()
    {
        Mode = true;
    }

    public void Billing()
    {
        Mode = false;
    }
     public string SearchString
    {
        get
        {
            string retVal = Mode ? CleanAddress(GetAddress) : CleanAddress(GetAddress);   
            system.debug(retVal);
            return retVal;
        }
    }
    public string GetAddress
    {
        get     
        {
          string address =  string.format('{0} {1} {2} {3} {4}', new string[]{'', refLead.Street, refLead.City, refLead.State, refLead.Country});
          return address;
        }
    }
    public string CleanAddress(string address)
    {
        return address.replace('null', '').trim();
    }

    
    public boolean Visible
    {
        get
        {
            boolean retVal = false;
            
            if (refLead.Country != null)
                retVal = true;

            if (refLead.State != null)
                retVal = true;
            
            if (refLead.City != null)
                retVal = true;

            if (refLead.Street != null)
                retVal = true;

            return retVal;
        }
    }
    
   
}