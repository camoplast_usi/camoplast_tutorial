/*********************************************************************************
Class Name      : CS_Test_OrderItem
Description     : This class is used for Test Classes for Functionalities on OrderItem object
Created By      : Reju Palathingal
Created Date    : 2-Aug-14
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Reju Palathingal          2-Aug-14                   Initial Version

*********************************************************************************/
@isTest
public class CS_Test_OrderItem{

    
       static testmethod void test_updateOrderItem(){
           Pricebook2 pb=new Pricebook2();
                 pb.Name= 'Test';
                 pb.Construction_Discount_Class__c= 'C';
                 pb.MH_Discount_Class__c= 'B';
             insert pb;
             Pricebook2 pb1= new Pricebook2();
                 pb1.Name= 'Test2'; 
                 pb1.Construction_Discount_Class__c= 'B';
                 pb1.MH_Discount_Class__c= 'B';
             insert pb1;
             Product2 Prd= new Product2();
                 Prd.Name = '123';
                 Prd.ProductCode = '12344';
                 Prd.IsActive = True;
              insert Prd;
             PricebookEntry pbe =new PricebookEntry ();
                 
                 pbe.Product2Id = Prd.id;
                 pbe.Pricebook2Id = pb.id;
                 pbe.UnitPrice = 12334;
             insert pbe;
        Account acc= new Account();
                 acc.Name = 'Test Account';
                 acc.CurrencyIsoCode = 'EUR';            
                 acc.Con_Discount_Class__c = 'C';
                 acc.MH_Discount_Class__c =  'B';                 
             insert acc; 
             RecordType rec = [SELECT Id FROM RecordType where Name = 'North America'];
             datetime myDateTime = datetime.now();     
        Order ord = new Order();
                 ord.PoNumber= '123';
                 ord.EffectiveDate = myDateTime.date() ;            
                 ord.RecordType= rec;
                 ord.AccountId =  acc.ID;   
                 ord.Status = 'Draft';              
             insert ord;   
              
         OrderItem orditm = new OrderItem();
                 
                 orditm.OrderId = ord.Id;
                 orditm.PricebookEntryId= pbe.Id;
                 orditm.Quantity = 2.00;
                 
             insert orditm;
          
        system.assert (orditm.Id!=null);            
      }
  }