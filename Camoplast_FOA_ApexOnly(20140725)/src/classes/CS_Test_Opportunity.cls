/*********************************************************************************
Class Name      : CS_TestClass_Opportunity
Description     : This class is used for Test Classes for Functionalities on Opportunity object
Created By      : Maryam Abbas
Created Date    : 15-Jul-14
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Maryam Abbas              15-Jul-14               Initial Version
*********************************************************************************/
@isTest(SeeAllData=true)
public class CS_Test_Opportunity{
 /*********************************************************************************
 Method Name    : test_updateOpportunityPriceBook
 Description    : Test method to update price book based on account selection on opportunity
 Return Type    : void
 Parameter      : nil                 
 *********************************************************************************/
    
       static testmethod void test_updateOpportunityPriceBook(){
             
             Pricebook2 pb=new Pricebook2();
                 pb.Name= 'Test';
                 pb.Construction_Discount_Class__c= 'C';
                 pb.MH_Discount_Class__c= 'B';
             insert pb;
             Pricebook2 pb1= new Pricebook2();
                 pb1.Name= 'Test2'; 
                 pb1.Construction_Discount_Class__c= 'B';
                 pb1.MH_Discount_Class__c= 'B';
             insert pb1;
            /* Pricebook2 pbstd= new Pricebook2();
                 pbstd.Name= 'Test2'; 
                 pbstd.Construction_Discount_Class__c= 'B';
                 pbstd.MH_Discount_Class__c= 'B';
                 pbstd.IsStandard = true;
             insert pbstd;*/
            
             datetime myDateTime = datetime.now();
        test.starttest(); 
        RecordType rt = [select id,Name from RecordType where SobjectType='Opportunity' and Name ='EMEA Sales Opportunity' Limit 1]; 
     
             Account acc= new Account();
                 acc.Name = 'Test Account';
                 acc.CurrencyIsoCode = 'EUR';            
                 acc.Con_Discount_Class__c = 'C';
                 acc.MH_Discount_Class__c =  'B';     
                 acc.Warehouse__c = '2100';
                 acc.Price_List__c ='1';
                 acc.Price_Group__c='1';
                 acc.Incoterms__c = 'CFR';
                 acc.Terms__c = 'COD';
                 acc.Payment_Method__c = 'C';
                 acc.Complete_Delivery__c = true;
                 acc.Incoterms_Location__c = 'test';
                 
                
             insert acc;
              Account sacc= new Account();
                 sacc.Name = 'Test Account';
                 sacc.CurrencyIsoCode = 'EUR';            
                 sacc.Con_Discount_Class__c = 'C';
                 sacc.MH_Discount_Class__c =  'B';     
                 sacc.Warehouse__c = '2100';
                 sacc.Price_List__c ='1';
                 sacc.Price_Group__c='1';
                 sacc.Incoterms__c = 'CFR';
                 sacc.Terms__c = 'COD';
                 sacc.Payment_Method__c = 'C';
                 sacc.Complete_Delivery__c = true;
                 sacc.Incoterms_Location__c = 'test';
                 
                
             insert sacc;
             Contact c = New Contact();
             c.LastName = 'Test';
             c.Account = acc;
             insert c;
             Contact sc = New Contact();
             sc.LastName = 'Test';
             sc.Account = sacc;
             insert sc;
             AccountContactRole acr = new AccountContactRole();
             acr.accountid = acc.ID;
             acr.contactid = c.ID;
             acr.role = 'Orders Main Contact';
             insert acr;
             AccountContactRole sacr = new AccountContactRole();
             sacr.accountid = sacc.ID;
             sacr.contactid = sc.ID;
             sacr.role = 'Service';
             insert sacr;
             Contact cnew = [SELECT ID FROM Contact WHERE ID = :c.ID limit 1] ;  
             Terms_and_Conditions__C terms = new Terms_and_Conditions__c();
             terms.Terms_and_Conditions__c = 'New Term';
             terms.AccountId__c = acc.ID;
             insert terms;
             Account accnt = [SELECT Assigned_Price_Book__c FROM Account WHERE ID = :acc.ID limit 1];    
             accnt.Assigned_Price_Book__c= pb.ID;  
             Opportunity opp= new Opportunity();
                 opp.Name = 'Test Opty';
                 opp.CloseDate = myDateTime.date() ;            
                 opp.StageName = 'Qualification';
                 opp.AccountId =  acc.ID;
                 opp.Ship_To_Account__c =sacc.ID;
                 opp.recordTypeId=rt.id;                 
             insert opp;
             Opportunity opty = [SELECT Ship_To_Contact__c, Quote_Prepared_For__c, Pricebook2Id,Incoterms_Location__c, Warehouse__c, Price_List__c, Price_Group__c, Incoterms__c, Payment_Terms__c, Payment_Method__c, Complete_Delivery__c FROM Opportunity WHERE ID=:opp.ID limit 1];
       //      system.Assert(opty.Pricebook2Id== pb.ID);  
             Account acc1= new Account();
                 acc1.Name = 'Test Account1';
                    
             insert acc1;
             system.Assert(acc1.Id != null);
             Opportunity opp1= new Opportunity();
                 opp1.Name = 'Test Opty 2';
                 opp1.CloseDate = myDateTime.date();            
                 opp1.StageName = 'Qualification';
                 opp1.AccountId = acc1.ID;     
                 opp1.Ship_to_Account__c = acc1.ID;          
             insert opp1;
             Opportunity opty1 = [SELECT Pricebook2Id FROM Opportunity WHERE ID =:opp1.ID limit 1];
             //system.Assert(opty1.Pricebook2Id== pb.ID); 
            //  List<Pricebook2> cs_DefaultPrice = [Select Id FROM Pricebook2 WHERE Pricebook2.IsStandard = true];
             //  system.Assert(cs_DefaultPrice.get(0).Id!=null);
               Quote q = new Quote();
               q.Name = 'New Quote';
               q.OpportunityId = opp.ID;
               //q.AccountId = acc.ID;
                q.RecordTypeId = [select Id from RecordType where Name LIKE '%EMEA%' and SobjectType = 'Quote'].get(0).Id;
            
               insert q;
             
               Terms_and_Conditions__c TC = [SELECT Terms_and_Conditions__c,OpportunityId__c,QuoteId__c FROM Terms_and_Conditions__c WHERE OpportunityId__c =:opp.ID limit 1];
              Terms_and_Conditions__c TC1 = [SELECT Terms_and_Conditions__c,OpportunityId__c,QuoteId__c FROM Terms_and_Conditions__c WHERE QuoteId__c =:q.ID limit 1];
             
       
            // system.Assert(opty1.Pricebook2Id=='01sb0000001kBItAAM'); //fix this
            system.Assert(opty.Quote_Prepared_for__c == cnew.ID);
              system.Assert(opty.Ship_To_Contact__c == sc.ID);
            // system.Assert(opty.Warehouse__c=='2100');
             system.Assert(opty.Price_List__c=='1');
             system.Assert(opty.Price_Group__c=='1');
             system.Assert(opty.Incoterms__c=='CFR');
             system.Assert(opty.Payment_Terms__c=='COD');
             system.Assert(opty.Payment_Method__c=='C');
             system.Assert(opty.Complete_Delivery__c =true);
             system.Assert(opty.Incoterms_Location__c=='test');
            
            // system.Assert(q.Warehouse__c=='2100');
            system.Assert(TC.OpportunityId__c!=null);  
             system.Assert(TC1.QuoteId__c!=null);
             system.Assert(q.Price_List__c=='1');
             system.Assert(q.Price_Group__c=='1');
             system.Assert(q.Incoterms__c=='CFR');
             system.Assert(q.Payment_Terms__c=='COD');
             system.Assert(q.Payment_Method__c=='C');
             system.Assert(q.Complete_Delivery__c =true);
             system.Assert(q.Incoterms_Location__c=='test');
               
        test.stoptest();              
      }
}