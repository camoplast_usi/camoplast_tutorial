/*********************************************************************************
Class Name      : CS_Account_PostControl
Description     : This is class is the extension for CS_AccountStatusBanner
Created By      : Divya A N
Created Date    : 2014-08-28
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Divya                    28-08-2014            Initial Version
Jayadev Rath             09-09-2014            Added method headers
Jayadev Rath             10-09-2014            Updated Refresh() method to pull data from ERP System
*********************************************************************************/
public with sharing class CS_Account_PostControl {

    Private Account refAccount = null;
    Public Boolean refreshPage {get; set;}
    //='https://ps1w2.rt.informaticacloud.com/active-bpel/services/REST/000V28/Vishal_AccSoldToNAService';
    Public String sNASoldToEndpoint ='https://ps1w2.rt.informaticacloud.com/active-bpel/services/REST/000V28/GET-AccountSoldTo-SFDC_NA-Service';
    Public String sNAShipToEndpoint ='https://ps1w2.rt.informaticacloud.com/active-bpel/services/REST/000V28/GET-AccountShipTo-SFDC_NA-Service';
    Public map<string, string> mapHeader {
        get{
                map<string, string> retVal = new map<string, string>();
                retVal.put('Content-type', 'application/json');
                retVal.put('Authorization', 'Basic cGlkdWZvdXJAZGVsb2l0dGUuY2EuZGV2OndlbGNvbWUxMjM0');
                return retval;
        }
    }
    
    
    
    
    /*********************************************************************************
    Method Name    : Constructor
    Description    : Retrives the Account information from the standard Controller
    Return Type    : None
    Parameter      : None
    *********************************************************************************/
    Public CS_Account_PostControl(ApexPages.StandardController controller) {
        If (controller != null && controller.getId() != null)
            refAccount = [SELECT Id,Name, Posted__c,Record_Type_Name__c,Customer_ID__c, SAP_External_Id__c, Ship_To_Id__c,
                            Epicor_External_Id__c, Last_Sync_Datetime_with_ERP__c,Is_Synced_From_SFDC__c,lastmodifieddate
                            FROM Account WHERE Id = :controller.getId() LIMIT 1];
    }

    /*********************************************************************************
    Method Name    : Post
    Description    : Updates the Last Sync date on Account for updating the In Sync flag.
    Return Type    : PageReference
    Parameter      : None
    *********************************************************************************/
    Public PageReference Post() {
        refAccount.Last_Sync_Datetime_with_ERP__c  = system.now();

        //For the Mock Up using Sample Epicor/SAP Ids
        if(refAccount.Record_Type_Name__c == 'NA_Sold_To' ||  refAccount.Record_Type_Name__c== 'NA_Ship_To'){
            refAccount.Epicor_External_Id__c = 'SampleEpicorExternalId';
        }
        else if(refAccount.Record_Type_Name__c == 'EMEA_Ship_To'){
            refAccount.SAP_External_Id__c = 'SampleSAPExternalId';
        }
        //refAccount.Posted__c = true;

        Update refAccount;
        refreshPage = true;
        return null;
    }
    
    /*********************************************************************************
    Method Name    : Refresh
    Description    : Fetches the latest data from ERP system and updates the current Account record
    Return Type    : PageReference
    Parameter      : None
    *********************************************************************************/
    Public void Refresh() {
        //The sync will happen if the record type is EMEA Sold To or if the Record has been posted but modified afterwards
        //If(refAccount.Record_Type_Name__c == 'EMEA_Sold_To' || (refAccount.Posted__c  && (refAccount.Last_Sync_Datetime_with_ERP__c <= refAccount.lastModifiedDate))){
        //    refAccount.Last_Sync_Datetime_with_ERP__c  = system.now();
        //    Update refAccount;
        //}
        //refreshPage = true;
        
        // Fetch details from the EPICOR System:
        // WHY WILL THERE BE TWO DIFFERENT ENDPOINTS FOR SOLD TO AND SHIP TO?
        //NEED ACCOUNT MAPPING.
        If(refAccount!= null && refAccount.Record_Type_Name__c == 'NA_Sold_To' || refAccount.Record_Type_Name__c == 'NA_Ship_To') {
          try{
            Map<String,Object> mapJsonBody = new Map<String,Object>();
            
            // Prepare the JSONBody:
            mapJsonBody.put('customer_code', refAccount.Customer_ID__c);
            mapJsonBody.put('ship_to',refAccount.Ship_To_Id__c);    // Will be used at Informatica only for Ship To 
            mapJsonBody.put('short_name', refAccount.Name);
            mapJsonBody.put('address_type',(refAccount.Record_Type_Name__c=='NA_Sold_To' ? 0 : 1 ) );
            mapJsonBody.put('Session_id',UserInfo.getSessionId());
            mapJsonBody.put('ServerUrl',DTT_ICRT_Helper.ServerURL);
            
            
            String sJSONBody = JSON.serializePretty(mapJsonBody);
            // Make the callout to get data from EPIcor system.
            
            System.debug('@@ sJSONBody' + sJSONBody);
            DTT_RestClient restClient = new DTT_RestClient((refAccount.Record_Type_Name__c=='NA_Sold_To' ? sNASoldToEndpoint :sNAShipToEndpoint ),'POST',mapHeader, sJSONBody);
            System.debug(restClient.responseBody);
            System.debug('@@ Response Body' + restClient.responseBody);
            
            // Check if response is Empty or has any content:
            if(restClient.responseBody == null) {
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.CS_Connectivity_Issue));
                return;
            }
            
            // Parse the JSONbody and get data from Epicor:
            List<DTT_RestResponse_NA.AccountWrapper> lstAccountDetails = (List<DTT_RestResponse_NA.AccountWrapper>)JSON.deserialize(restClient.responseBody , List<DTT_RestResponse_NA.AccountWrapper>.class);
            For(DTT_RestResponse_NA.AccountWrapper responseAcc : lstAccountDetails) {
                refAccount.Customer_ID__c = responseAcc.customer_code;
                refAccount.BillingStreet = responseAcc.addr1 ;
                refAccount.BillingCity = responseAcc.addr2 ;
                refAccount.Phone = responseAcc.phone_1 ;
                refAccount.BillingPostalCode = responseAcc.postal_code ;
                
                // Set the current time after refresh.
                refAccount.Last_Sync_Datetime_with_ERP__c  = system.now();
                Update refAccount;
                Break;    // run for only first response & then break:
            }
          refreshPage = true;
          }
          catch(Exception e) {
              System.debug('@@ Exception Caught: '+ e.getMessage() );
              ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.CS_Connectivity_Issue));
              //refreshPage = false;
          }
        }
    }
}