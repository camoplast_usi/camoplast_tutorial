/*********************************************************************************
Class Name      : CS_Test_MockHttpResponseGenerator 
Description     : Class to test inventory call out (to Informatica)
Created By      : Jayadev Rath
Created Date    : 21-Aug-2014
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
Jayadev Rath                21-Aug-2014            Initial Version
----------------------------------------------------------------------------------
**********************************************************************************/   @isTest
global class CS_Test_MockHttpResponseGenerator implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
    
    // Create a fake response
    HttpResponse res = new HttpResponse();
    res.setHeader('Content-Type', 'application/json');
    res.setBody('[]');
    res.setStatusCode(200);
    return res;
    }
}