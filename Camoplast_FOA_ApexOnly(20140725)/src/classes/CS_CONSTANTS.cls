/*************************************************************************************
Classe Name : CS_CONSTANTS
Description : Class to hold all Flags, Static Variables, Constants to be used across different classes/triggers.
Created By  : Jayadev Rath
Created Date: 01-Aug-2014
Modification Log:
---------------------------------------------------------------------------------- 
Developer             Date             Description
---------------------------------------------------------------------------------- 
Jayadev Rath         01-Aug-2014       Initial Version
************************************************************************************/
Public Class CS_CONSTANTS {
    
    // Identifies if an Order is created from Quote [If true, then copy all Term&Condition records from Quote level, else copy from Account level.]
    Public Static Boolean bOrderCreatedFromQuote = False;
    
    public static final string sOrder_RecTypeDevName_EMEA_Sales = 'EMEA_Sales_Order';
    public static final string sOrder_RecTypeDevName_EMEA_Service = 'EMEA_Service_Order';
    public static final string sOrder_RecTypeDevName_NA = 'North_America';

}