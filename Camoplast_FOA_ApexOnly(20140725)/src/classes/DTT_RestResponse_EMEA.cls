public  class DTT_RestResponse_EMEA {

    public Class ProductWrapper{
        public string Plant {get; set;}
        public string Description {get; set;}
        public string Flag {get; set;}
        public string ValuationType {get; set;}
        public string BallStockQty {get; set;}
        public string DshStockQty {get; set;}
        public string Material {get; set;}
        public string Ldescription {get; set;}
        public string OsoStockQty {get; set;}
        public string ItStockQty {get; set;}
        public string SlocDescrition {get; set;}
        public string StpoStockQty {get; set;}
        public string VuuStockQty {get; set;}
        public string Classification {get; set;}
        public string UnitOfMeasure {get; set;}
        public string BasePrice {get; set;}
        public string AllStockQty {get; set;}
        public string Zcheck {get; set;}
        public string Discount {get; set;}
        public string PlantDescription {get; set;}
        public string NetPrice {get; set;}
        public string StorageLocation {get; set;}
        public string IntransitQty{get; set;}
        public string AvailableQty{get;set;}
        public string MaterialDescription{get;set;}
    }
    
    Public Class OrderItemWrapper{
        Public String Line_Item_Number {get;set;}
    }
    Public Class OrderWrapper {
        Public String Response_from_LogSFDC {get;set;}
        Public String Order_Number{get;set;}
        Public List<DTT_RestResponse_EMEA.OrderItemWrapper> Items{get;set;}
    }
}