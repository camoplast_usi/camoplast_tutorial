/*************************************************************************************
Trigger Name    : CS_ProductTrigger
Description     : Trigger for ‘Product' object. 
Created By      : Divya A N
Created Date    : 15-Sep-14
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Divya A N               15-Sep-14                  Initial Version
************************************************************************************/
trigger CS_ProductTrigger on Product2 (before insert,before update,after insert,after update) {
    
    if(Trigger.isAfter && Trigger.isInsert){
        CS_Trigger_Product.createPriceBookEntry(trigger.new);
    }
   
}