/*********************************************************************************
Class Name      : AccInfoToInformatica
Description     : Class to send an outbound call to Informatica sending all the Account details
Created By      : Jayadev Rath
Created Date    : 12 Aug 2014
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------
 
*********************************************************************************/

Public Class AccInfoToInformatica{

    // Added remote site setting
    Public String endPoint = 'https://app2.informaticacloud.com/saas/api/1/salesforceoutboundmessage/RCPvyAK8aWMb5mldI35V5YLGmSzk1uXD';
    Public String sUserName = 'sumsingh@deloitte.com.dev';
    Public String sPassword = 'welcome123';
    Public string sJobName = 'Sumit_SFDC_CSV_Account_Realtime';
    Public String sJobType = 'DSS';
    
    Public AccInfoToInformatica(){}
    Public AccInfoToInformatica(ApexPages.StandardController controller) {
        
        if(controller.getId() != null) 
            invokeInformatica(controller.getId());
    }
    
    Public pageReference invokeInformatica(Id AccountId){
        // Get the account details from DB:
        //Account acc = 
        // Which information should be Passed??
        String finalEndPointUrl =endPoint + 
                                '?username='+EncodingUtil.urlEncode(sUserName,'UTF-8') +
                                '&password='+EncodingUtil.urlEncode(sPassword,'UTF-8') +
                                '&jobName='+ EncodingUtil.urlEncode(sJobName,'UTF-8') +
                                '&jobType='+ EncodingUtil.urlEncode(sJobType,'UTF-8');
        
        
        HttpRequest req = new HttpRequest();
        req.setEndPoint(finalEndPointUrl);
        req.setMethod('POST');
        HttpResponse res     = new HttpResponse();
        system.debug('Request %%%$$ '+req);
        try {
            Http http = new HTTP();
            res = http.send(req);
        }
        catch(Exception e) {}
        system.debug('$$$### Result '+res);
        
        return new PageReference('/'+AccountId);
    }
}